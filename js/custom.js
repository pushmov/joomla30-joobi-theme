/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready(function(){
    jQuery('.overflow').perfectScrollbar();
    
    
    jQuery('div.dropdown').hover(function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
});