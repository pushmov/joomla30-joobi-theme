<?php
defined('JOOBI_SECURE') or die('J....');


class Basket_Basket_View_module extends WClasses {




/** this function will return an HTML display output two ( Display type NORMAL ) out for basket module
 * @param string $basket - basket session
 * @param string $display - contains the HTML display for price
 * @return HTML
*/
public function displayMiniBasket( $basket ) {

	$HTML = '';

	$returnIdURI = WView::getURI();
	$returnid = base64_encode( $returnIdURI) ; //returnid
	$link = $this->_checkoutLink( $returnid );


	//add a link to go to the cart
	$HTML = '<a href="'. $link .'" style="text-decoration:none;">';
	$HTML .= '<div class="cartMini">';

	if ( $this->showitems ) {
		//total number of Items
		if ( !empty($basket->Products) ) $itemNb = count( $basket->Products );
		if ( !empty($itemNb) ) {
			$htmlItems = '<div class="cartItemNb"><span class="cartItemNbNumber">'. $itemNb .'</span>' . ' ';
			$htmlItems .= '<span class="cartItemNbText">';
			$htmlItems .= ( ( $itemNb > 1 ) ? WText::translate('Items') : WText::translate('Item') ) ;
			$htmlItems .= ' </span></div>';
			$HTML .= $htmlItems;
		}//endif

	}//endif

	if ( $this->showtotal ) {

		$usedCurrency = WUser::get( 'curid' );
		$priceHTML = ( !empty($basket->totalprice) ? WTools::format( $basket->totalprice, 'money', $usedCurrency ) : '' );
		$formatedTotalPrice = '<span class="cartModTotal">' . $priceHTML . '</span>';
		// display total price
		$total = '<div class="cartTotalWrap"><span class="cartModTotalText">'. WText::translate('Total') .'</span> ';	// <span class="cartModTotalText">'. WText::translate('Total') .' :</span>
		$total .= '<span class="cartModTotalPrice">' . $formatedTotalPrice .'</span></div>';
		$HTML .= $total;

	}//endif


	if ( $this->showaddcart ) {

		$returnIdURI = WView::getURI();
		$returnid = base64_encode( $returnIdURI) ; //returnid

		$link = $this->_checkoutLink( $returnid );



		$HTML .= '<div class="cartModCheckout">';
		$objButtonO = WPage::newBluePrint( 'prefcatalog' );
		$objButtonO->type = 'buttonAddToCartInCatalogPage';
		$objButtonO->text = WText::translate( 'Check Out' );
		$objButtonO->link = $link;
		$HTML .= WPage::renderBluePrint( 'prefcatalog', $objButtonO );
//		$HTML .= '<a href="'. $link .'" style="text-decoration:none;"><span class="cartModCheckoutText">'. WText::translate( 'Check Out' ) .'</span></a>';
		$HTML .= '<div class="clr"></div></div>';

	}//endif

	$HTML .= '</div>';
	$HTML .= '</a>';

	return $HTML;

}//endfct


/** this function will return an HTML display output two ( Display type NORMAL ) out for basket module
 * @param string $basket - basket session
 * @param string $display - contains the HTML display for price
 * @return HTML
*/
public function displayOne( $basket ) {

	$HTML = '';

        $button = '<div class="btn-update">';
	$button .= '<button type="submit" name="task" value="updatecart" class="btn cartModUpdateBtn btn-small btn-info">'.WText::translate('Update').'</button>';
        $button .= '</div>';
//	$objButtonO = WPage::newBluePrint( 'prefcatalog' );
//	$objButtonO->type = 'buttonBasketUpdateCart';
//	$objButtonO->text = WText::translate( 'Update' );
////	$objButtonO->link = $link;
//	$button = WPage::renderBluePrint( 'prefcatalog', $objButtonO );

        $HTML .= '<div class="dropdown dropdown-mini">';
        $HTML .= '<a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-shopping-cart"></i> Cart<b class="caret"></b></a>';
//echo debug( 3353011, 'REFACTOR THIS fieldL  DIV');
	// create update button
	$total = '<div class="fieldL"></div>';
//	$total .= '<div class="fieldR">';
//	$total .= '<span class="SBLeft"><span class="SBRight"><span class="SBCenter">';
//	$total .= $button;
//	$total .= '</span></span></span>';
//	$total .= '</div>';

	$objButtonO = WPage::newBluePrint( 'button' );
	$objButtonO->text = $button;
	$objButtonO->type = 'standard';
        $objButtonO->extraClasses = 'btn-small btn-info';
	$objButtonO->wrapperDiv = 'fieldR';
	$updateBtn .= WPage::renderBluePrint( 'button', $objButtonO );
	$total .= '<div class="clr"></div>';
	$total .= $this->_displayTotal( $basket );
        
        //show message "Your shopping cart is empty" if basket is empty
        if(empty($basket->Products)) {
            
            $HTML .= '<div class="dropdown-menu">'
                    . '<div class="cart-empty">Your shopping cart is empty!</div></div>';
            
        } else {
            
            $controller = new stdClass;
            $controller->controller = 'basket';
            $controller->wid = $this->wid;
            $controller->sid = WModel::get( 'basket', 'sid');
            $controller->firstForm = true;

            $this->layout = WView::getHTML( 'basket_module_listing', $controller );

            $formObj = WView::form( $this->layout->firstFormName );
            $formObj->hiddenRemove( 'task' );
            //	if ( isset( $this->layout->formObj->_hidden['task'] ) ) unset($this->layout->formObj->_hidden['task']);

            $this->layout->addContent( $updateBtn );


            //dropdopwn hover wrapper
            $HTML .= '<div class="dropdown-menu">';

            $HTML .= '<div class="dropdown-items overflow wrapper"><div class="items-scroll">'.$this->layout->make().'</div></div>';

            //button view cart and checkout
            $HTML .= '<div class="clr"></div>';

            //button view cart and checkout wrapper
            $HTML .= '<div class="cartmini-total-foot">';
            $HTML .= '<div class="total-wrap clearfix">' . $total . '</div>';

            if ( $this->showaddcart ) {

                    $returnIdURI = WView::getURI();
                    $returnid = base64_encode($returnIdURI); //returnid

                    $link = $this->_checkoutLink( $returnid );

                    $HTML .= '<div class="cartModCheckout">';


                    //add cart button here
                    $objButtonView = WPage::newBluePrint( 'prefcatalog' );
                    $objButtonView->type = 'buttonViewCartInCatalogPage';
                    $objButtonView->extraClasses = 'btn-danger btn-small btn-view-cart';
                    $objButtonView->text = WText::translate( 'View cart' );
                    $objButtonView->link = $link;
                    $HTML .= WPage::renderBluePrint( 'prefcatalog', $objButtonView );

                    //checkout button here

                    $objButtonO = WPage::newBluePrint( 'prefcatalog' );
                    $objButtonO->type = 'buttonAddToCartInCatalogPage';
                    $objButtonO->extraClasses = 'btn-success btn-small btn-add-cart-mini';
                    $objButtonO->text = WText::translate( 'Checkout' );
                    $objButtonO->link = $link;
                    $HTML .= WPage::renderBluePrint( 'prefcatalog', $objButtonO );
    //		$HTML .= '<a href="'. $link .'" style="text-decoration:none;"><span class="cartModCheckoutText">'. WText::translate( 'Check Out' ) .'</span></a>';
                    $HTML .= '<div class="clr"></div></div>';

            }//endif

            //end dropdown hover wrapper
            $HTML .= '</div>';
            $HTML .= '</div>';
            $HTML .= '</div>';
            //echo debug(8, $HTML);
        }
        
	return $HTML;

}//endfct


/** this function will return an HTML display output two ( Display type SLIDER ) out for basket module
 * @param string $basket - basket session
 * @param string $display - contains the HTML display for price
 * @return HTML
*/
public function displayTwo( $basket ) {
	$HTML = '';

	//	$showitemtxt = isset( $this->showitemtxt ) ? $this->showitemtxt : 0;
	$itemtxt = isset( $this->itemtxt ) ? $this->itemtxt : '';

	// get number of items
	if ( !empty($this->showitems) ) {
		WGlobals::set( 'BasketShowItems', true, 'global' );
	}//endif


	$total = $this->_displayTotal( $basket );

	$controller = new stdClass;
	$controller->controller = 'basket';
	$controller->wid = $this->wid;
	$controller->sid = WModel::get( 'basket', 'sid');
	$controller->firstForm = true;
	$this->layout = WView::getHTML( 'basket_module_slider_form', $controller );	// listing of item  basket_module_listing
	if ( empty($this->layout) ) return $HTML;

//	unset( $this->layout->formObj->_hidden['task'] );
	$formObj = WView::form( $this->layout->firstFormName );
	$formObj->hiddenRemove( 'task' );


	$returnIdURI = WView::getURI();
	$returnid = base64_encode($returnIdURI); //returnid
	$link = $this->_checkoutLink( $returnid );

	$this->layout->addContent( $total );

	$HTML .= '<div class="clr"></div>';
	$HTML .= '<div class="cartModMain">'. $this->layout->make() .'</div>';
	$HTML .= '<div class="clr"></div>';

	if ( $this->showaddcart ) {

		// display the main view that contains the slider and basket products
		$HTML .= '<div class="cartModCheckout clearfix">';
		$objButtonO = WPage::newBluePrint( 'prefcatalog' );
		$objButtonO->type = 'buttonAddToCartInCatalogPage';
		$objButtonO->text = WText::translate( 'Check Out' );
		$objButtonO->link = $link;
		$HTML .= WPage::renderBluePrint( 'prefcatalog', $objButtonO );
//		$HTML .= '<a href="'. $link .'" style="text-decoration:none;"><span class="cartModCheckoutText">'. WText::translate( 'Check Out' ) .'</span></a>';
		$HTML .= '</div>';
		$HTML .= '<div class="clr"></div>';
	}//endif

	return $HTML;
}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $basket
 */
private function _displayTotal( $basket ) {


	$total = '';
	$showSubtotal = false;
	$usedCurrency = WUser::get( 'curid' );

	//Shwo the tax
	WPref::load( 'PCATALOG_NODE_USETAX' );
	if ( PCATALOG_NODE_USETAX && PCATALOG_NODE_SHOWTAX ) {

		//totaldiscount
		if ( !empty($basket->totaltax) ) {
			$showSubtotal = true;

			$totalTax = '<div class="cartModTotalWrap"><span class="cartModTotalText">'. WText::translate('Tax') .' :</span>';
			if ( PCATALOG_NODE_SHOWPRICETAX == -1 ) $totalTax .=  ' + ';

			$totalTax2Pay = $basket->totaltax;

//			if ( !empty($basket->shippingTax) && $basket->shippingTax > 0 ) $totalTax2Pay = $totalTax2Pay + $basket->shippingTax;

			$totalTax .= WTools::format( $totalTax2Pay, 'money', $usedCurrency ) .'</div>';

		}//endif

	}//endif

	if ( $showSubtotal ) {

		$total .= '<div class="cartModTotalWrap"><span class="cartModTotalText">'. WText::translate('Subtotal') .' :</span>';
		$total .= WTools::format( $basket->subtotal, 'money', $usedCurrency ) .'</div>';

		//we need to show the shipping and handling if exist

		// total shipping rate
		if ( PCATALOG_NODE_USETAX && !empty($basket->shippingRate) && $basket->hasShipping ) {
			if ( $basket->shippingRate > 0 ) $showSubtotal = true;
			$shippingRateValue = $basket->shippingRate;
//			if ( PCATALOG_NODE_SHOWPRICETAX == -1 && !empty($basket->shippingTax) && $basket->shippingTax > 0 ) $shippingRateValue = $shippingRateValue - $basket->shippingTax;

			//define the Carrier name and log
//			$shipHelperC = WClass::get( 'shipping.helper' );
//			$shippingName = $shipHelperC->getCarrierAndRateInfo( $basket->shiprateid );

			$total .= '<div class="cartModTotalWrap"><span class="cartModTotalText">'. WText::translate('Shipping') .' :</span>';
			$total .= ' + ' . WTools::format( $shippingRateValue, 'money', $usedCurrency ) .'</div>';

		}//endif

		//taxes
		$total .= $totalTax;


	}//endif

	if ( !empty( $basket->totaldiscount ) && $basket->totaldiscount > 0 ) {
		$total .= '<div class="cartModTotalWrap"><span class="cartModTotalText">'. WText::translate('Discount') .' :</span>';
		$total .= WTools::format( -1 * $basket->totaldiscount, 'money', $usedCurrency ) .'</div>';
	}//endif

//echo debug( 7898989, $basket );

	if ( isset($basket->totalprice) ) $formatedTotalPrice = '<span class="cartModTotal">' . WTools::format( $basket->totalprice, 'money', $usedCurrency ) . '</span>';
	else $formatedTotalPrice = '';

	// display total price
	$total .= '<div class="cartModTotalWrap"><span class="cartModTotalText">'. WText::translate('Total') .' :</span>';
	$total .= $formatedTotalPrice .'</div>';
	//$HTML .= $total;

	return $total;

}//endfct


/** this function will return the checkout link to be used in a module
 * @param string $returnedid
*/
	private function _checkoutLink( $returnid ) {

		$itemId = WPage::getSpecificItemId( 'basket' );

		$link = WPage::link( 'controller=basket&task=check&returnid=' . $returnid, '', $itemId );
		return $link;
	}//endfct


}//endclass