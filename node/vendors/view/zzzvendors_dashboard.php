<?php defined('JOOBI_SECURE') or die('J....'); ?>
<div id="vendors-dashboard">
	<div class="dashleft">
		<div class="dashProfile" style="float:left;">
			<?php echo $this->getContent( 'image' ); ?>
		</div>
		
		<div class="generalInfo" style="float:left;">
			<?php echo $this->getContent( 'generalinfo' ); ?>
		</div>
		
		<div style="clear:both;float:left;">
			<div style="margin-top: 20px;">
				<?php echo $this->getContent( 'contactinfo' ); ?>
			</div>
		</div>

		<div style="clear:both;float:left;">
			<div style="margin-top: 20px;">
				<?php echo $this->getContent( 'shippinginfo' ); ?>
			</div>
		</div>
	</div>
	
	<div class="dashright">
		<div class="dashmenu" style="clear:both;margin-top: 20px;">
				<?php echo $this->getContent( 'menubar' ); ?>
		</div>
		<div class="dashtabs" style="clear:both;margin-top: 20px;">
				<?php echo $this->getContent( 'tabs' ); ?>
		</div>
	</div>
</div>