<?php
defined('JOOBI_SECURE') or die('J....');



class WRender_Countdown_classObject {

	/*
	 * WPage::newBluePrint( 'icon' );
	 *
	 */
	public $location = '';

	public $icon = '';	// the name of the icon / image

	public $text = '';	// a text to put into the title of the img or tooltips

	public $size = '';	//define the size of an icon

	public $color = '';	//define the color of an icon

	public $animation = '';	// the anmication fro the icon


}//endclass


/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Countdown_class extends Theme_Render_class {


/**
 *
 * This function is to render a button
 * WPage::renderBluePrint( 'countdown', $iconO );
 * @param object $data
 */
  	public function render( $object ) {

  		static $onlyOnce = true;
  		static $idCount = 0;

  		if ( $onlyOnce ) {
        	    //include js file
        	    WPage::addJSFile( 'js/countdown.js' );
  		}//endif

  		$idCount++;
  		$divId = 'countDownIdWeb'. $idCount;
  		$date = date( 'Y-m-d H:i:s', $object->time );
  		$variableName = 'ctWeb' . $idCount;


  		unset( $object->type );
  		$translation = WGlobals::filter( $object, 'safejs' );

  		$translation_json = json_encode($translation);

  		$JScode = $variableName . " = new dateTimeDownWeb('". $this->_datediff( $date ) . "', '" . $divId . "','".($translation_json)."');";
  		$JScode .= $variableName . '.do_cd(' . $object->precision . ');';

  		WPage::addJSScript( $JScode );

  		return '<div id="' . $divId . '"></div>';

  	}//endfct


/**
 *
 * Date difference function. Will be using below
 * @param unknown_type $finalTime
 * @param unknown_type $nowtime
 */
        private function _datediff( $finalTime, $nowtime=0 ) {

        	if ( $nowtime == 0 ) {
        		$datefrom = date("Y-m-d H:i:s"); // current time -- NO NEED TO CHANGE
        	} else {
        	    $datefrom = $nowtime;
        	}//endif

//TODO use WApplication::stringToTime(
        	$datefrom = WApplication::stringToTime( $datefrom, 0 );
        	$dateto = WApplication::stringToTime( $finalTime, 0 );
//        	$datefrom = strtotime($datefrom, 0);
//        	$dateto = strtotime($finalTime, 0);

        	$difference = $dateto - $datefrom; // Difference in seconds

        	if ( $difference < 0 ) $difference = $difference * -1;

        	return $difference;

        }//endfct


}//endclass