<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Pagination_class extends Theme_Render_class {


/**
 *
 * This function is to render a button
 * @param object $data
 * 				-> text: text to render
 * 				-> link: falcutive link that we put around the button
 * 				-> linkOnClick
 * 				-> class or style ... still need to define how we do that one:
 * 						-> standard ( blue checkout button )
 * 						-> infoLink
 * 						-> viewAll
 * 				-> wrapperDiv
 * 				->float: left / right normal
 * 				-> size: small, medium ( standard), large ....  so we differentiate between link
 * 				-> color:
 * 				-> icon:	( add, next, before, )
 * 				-> iconLocation:	before, after ( if RTL we reverse ;P ) default after
 */
  	public function render( $data ) {

  		if ( empty($data->pageNumbersA) ) return '';

		$pageLinks = '';
  		//creaet the pagination
  		foreach( $data->pageNumbersA as $onePage ) {
  			$pageLinks .= $this->_oneLink( $onePage );
  		}//enforeach


  		$paginationHTMLMAIN = '<ul';
  		//if we have T3 we need to add another class name
  		if ( defined( 'T3_TEMPLATE' ) ) $paginationHTMLMAIN .= ' class="pagination"';
  		$paginationHTMLMAIN .= '>'.$pageLinks.'</ul>';


		if ( !empty($data->pageOf) ) $paginationHTML = '<p class="counter">'.$data->pageOf.'</p>';
		else $paginationHTML = '';


		if ( APIPage::isRTL() ) {
			$html = $paginationHTMLMAIN . $paginationHTML;
		} else {
			$html = $paginationHTML . $paginationHTMLMAIN;
		}//endif


		$return = '<div class="pagination">' . $html . '</div>';

		$return = '<div class="pagination-wrap">' . $return . '</div>';

		return $return;

  	}//endfct




/**
	* <p>Create the navigation links</p>
	* @param string $onePage->text
	* @param mixed $value
	* @param boolean $onePage->current
	* @param string $pageO->classOne
	* @param string $pageO->classTwo
	* @param string $off
	*/
//	private function _oneLink( $text, $value, $current=false , $class='', $class2='', $off='', $index=null ) {
  	private function _oneLink( $onePage ) {

  		if ( empty($onePage) ) return '';

//TODO this is not if admin test but rather if we have a form and javascripts enabled.
		//for now if admin will do it
			$addtionalClass = ( !empty($onePage->class) ? ' class="pagination-'.$onePage->class.'"' : '' );

			if ( !empty($onePage->current) ) {
//				$onePage->text = '<span class="currentPage">' . $onePage->text . '</span>';
//				return '<span class="pagiLeft currentPage"><span class="pagiRight"><span class="pagiCenter">'.$onePage->text.'</span></span></span>';
				$html = '<li'.$addtionalClass.'><span class="pagenav">'.$onePage->text.'</span></li>';
				return $html;

			} else {


				$return = '<li'.$addtionalClass.'><a';
				$return .= ' class="pagenav"';
				if ( !empty($onePage->linkOnClick) ) $return .= ' onclick="' . $onePage->linkOnClick . '"';
				$return .= ' href="#">'.$onePage->text.'</a></li>';
				return $return;


				$link = '<span class="pagiCenter"><a href="#" class="page" title="'.$onePage->text .
'" onclick="' . $onePage->linkOnClick . '">'.$onePage->text.'</a></span>';

				$test = '<span class="pagiCenter">' . $onePage->text . '</span>';

				if ( empty($onePage->off) ) $onePage->off = '';
				$return ='<span class="'.$onePage->classTwo. $onePage->off .'"><span class="'.$onePage->classOne.'">';
				$return .= ( trim( $onePage->off ) == 'disabled' ) ? $test : $link;
				$return .=	'</span></span>';

			}//endif

			return $return;


	}//endfct

}//endclass
