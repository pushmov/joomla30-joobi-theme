<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/


/**
 *
 * this class is the object to create a button with all the properties possible to use
 */
class WRender_Image_classObject {

	/*
	 * WPage::newBluePrint( 'image' );
	 */
	public $location = '';

	public $border = 0;

	public $text = '';

	public $width = 0;
	public $height = 0;

	public $id = null;
	public $name = null;

	public $align = null;
	public $style = null;
	public $class = null;

}//endclass


class WRender_Image_class extends Theme_Render_class {

	private static $_imageStyle = null;
	private static $_imageResponsive = null;


/**
 *
 *	WPage::renderBluePrint( 'image', $data );
 *
 * This function is to render an image
 * @param object $data
 */
  	public function render( $data ) {

  		if ( empty($data->location) ) {
  			$this->codeE( 'Image location not specified!' );
  			return '';
  		}//endif

  		if ( !isset( self::$_imageStyle ) ) {
  			self::$_imageStyle = $this->value( 'image.style' );
  			self::$_imageResponsive = $this->value( 'image.responsive' );
  		}//endif

  		if ( !empty( $data->class ) ) {
  			$newClass = $data->class . ' ';
  		} else {
  			$newClass = '';
  		}//endif

  		if ( self::$_imageResponsive ) $newClass .= 'img-responsive ';
  		if ( !empty(self::$_imageStyle) ) $newClass .= 'img-' . self::$_imageStyle;

  		$newClass = trim( $newClass );

  		$html = '<img src="' . $data->location . '" border="' . $data->border . '"';
  		if ( !empty( $data->text ) ) $html .= ' alt="' . WGlobals::filter( $data->text ) . '"';
  		if ( !empty($newClass) ) {
  			$html .= ' class="' . $newClass . '"';
//  		} else {
//  			$html .= '';
  		}//enddif

  		if ( !empty( $data->id ) ) {
  			$html .= ' id="' . $data->id . '"';
  		}//endif

  		if ( !empty( $data->name ) ) {
  			$html .= ' name="' . $data->name . '"';
  		}//endif

  		if ( !empty( $data->align ) ) {
  			$html .= ' align="' . $data->align . '"';
  		}//endif

  		if ( !empty($data->width) || !empty($data->height) ) {
  			$style = '';
  			if ( !empty($data->width) ) $style .= 'width:'.$data->width.'px;';
  			if ( !empty($data->height) ) $style .= 'height:'.$data->height.'px;';
  			if ( !empty($data->style) ) $style .= $data->style;
  			if ( !empty($style) ) $html .= ' style="' . $style . '"';

  		}//endif

  		$html .= '/>';

		return $html;

  	}//endfct

}//endclass