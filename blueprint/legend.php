<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Legend_class extends Theme_Render_class {

	private static $_instance = array( 'publish'=>array(), 'standard'=>array(), 'notice'=>array() );
	private static $_instanceFalseAddCSS = array( 'publish'=>array(), 'standard'=>array(), 'notice'=>array() );

	private $_columniconcolor = true;

/**
 *
 * This function is to render a button
 * @param object $data
 *
 * 			WPage::renderBluePrint( 'legend', $objButtonO );
 *
 * 				-> action: store / create

 */
  	public function render( $data ) {

  		if ( is_string( $data ) ) {
  			if ( 'createLegend' == $data ) {
		  		$legendRemove = $this->value( 'legend.remove' );
		  		if ( !empty($legendRemove) ) return '';

		  		$this->_columniconcolor = $this->value( 'table.columniconcolor' );
  				return $this->_createLegend();
//  			} elseif ( 'createListingIcon' == $data ) {
////  				$this->_columniconcolor = $this->value( 'table.columniconcolor' );
//
  			} else {
  				return $this->_renderImage( $data );
  			}//endif

  		} else {

  			if ( !empty( $data->createListingIcon ) ) {
		 		$this->_columniconcolor = true;
//		 		$myDiv = ( !empty($data->property) ? $data->property : 'mydiv' );
//		  		return $myDiv . ".setAttribute('class','fa " . WPage::renderBluePrint( 'icon', $data->action ) . " fa-lg'); ";	// $this->_getIcon( $data->action )
//		 		return $myDiv . ".addClass('fa " . WPage::renderBluePrint( 'icon', $data->action ) . " fa-lg');";	// $this->_getIcon( $data->action )
		 		return "fa " . WPage::renderBluePrint( 'icon', $data->action ) . " fa-lg";	// $this->_getIcon( $data->action )

  			} elseif ( !empty( $data->sortUpDown ) ) {

  				switch( $data->action ) {
  					case 'upGreen':
  						return '<i class="fa fa-sort-asc text-success fa-lg"></i>';
  					case 'upGray':
  						return '<i class="fa fa-sort-asc text-gray fa-lg"></i>';
  					case 'downGreen':
  						return '<i class="fa fa-sort-desc text-success fa-lg"></i>';
  					case 'downGray':
  						return '<i class="fa fa-sort-desc text-gray fa-lg"></i>';
  					case 'orderBy':
  						if ( $data->direction == 'desc' ) {
  							return '<i class="fa fa-sort-amount-desc fa-lg"></i>';
  						} else {
  							return '<i class="fa fa-sort-amount-asc fa-lg"></i>';
  						}//endif
  					case 'saveOrder':
  						return '<i class="fa fa-floppy-o fa-lg"></i>';

  					default:
  						return '';
  				}//endswitch
  			}//endif

  			if ( !isset($data->class) ) $data->class = '';
  			if ( !isset($data->group) ) $data->group = 'standard';
  			if ( !isset($data->order) ) $data->order = 99;
  			if ( !isset($data->ID) ) $data->ID = '';
  			return $this->_storeLegend( $data->image, $data->text, $data->group, $data->order, $data->class, $data->ID );
  		}//endif

  	}//endfct



/**
	* <p>Get the legend
	* This function holds all the images and text which needs to be printed in the legend</p>
	* @param string $image image file
	* @param string $text image name
	* @param string $group the grouping (  'publish = for publishing icons' , 'notice' for message without icons,   standard anything else )
	* @param int $order the order of the picture facultatif
	* @param string $class the CSS class to use if not specifed we take the default jpng-16-...
	*/
	private function _storeLegend( $image='', $text='', $group='standard',  $order=99, $class='', $ID='' ) {	// , $addCSS=true, $picturePath='toolbar/16/'

		if ( empty($text) ) {

			if ( !empty(self::$_instanceFalseAddCSS) ) {

				foreach( self::$_instanceFalseAddCSS as $key => $item ) {
					if ( !empty($item) ) {
						foreach( $item as $key2 => $item2 ) {
							if ( !empty($item2) ) {
								foreach( $item2 as $key3 => $item3 ) {
									if ( !isset( self::$_instance[$key][$key2][$key3] ) ) {
										self::$_instance[$key][$key2][$key3] = self::$_instanceFalseAddCSS[$key][$key2][$key3];
									}//endif
								}//endofreach
							}//endif
						}//endofreach
					}//endif
				}//endofreach
			}//endif

			return self::$_instance;
		}//endif

		$group = strtolower($group);
		$image = strtolower($image);

		//to cache the fact that we store the WPage::addCSS
//		if ( $addCSS && ((!isset(self::$_instance[$group][$order][$image] )) || ( $group == 'notice' )) ) {
//			if ( empty($image) ) {
//				$image = '/';
//			} else {
//				if ( $addCSS ) {
//					$this->_addCSSLegend( $image, $class, $picturePath );
//				}//endif
//			}//endif
//
//			$ObjectT= new stdClass;
//			$ObjectT->text = $text;
//			$ObjectT->path = $picturePath;
//			self::$_instance[$group][$order][$image] = $ObjectT;
//
//		} else

		if ( !isset(self::$_instanceFalseAddCSS[$group][$order][$image] ) ) {
			$ObjectT= new stdClass;
			$ObjectT->text = $text;
//			$ObjectT->path = $picturePath;
			self::$_instanceFalseAddCSS[$group][$order][$image] = $ObjectT;
		}//endif

		return $this->_renderImage( $image, $ID );
//		return self::$_instance;

	}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $image
 * @param unknown_type $ID
 */
	private function _renderImage( $image, $ID='' ) {
//debug( 56223,  $ID );
		$html = '<i';
//		$html = '<span';
		if (!empty($ID) ) $html .= ' id="' . $ID . '"';
		$html .= ' class="fa ' . WPage::renderBluePrint( 'icon', $image ) . ' fa-lg">';	// $this->_getIcon( $image )	;
//		$html .= '</span>';
		$html .= '</i>';
		return $html;
	}//endfct



/**
	* <p>Display the legend of the page
	* groups definition:
	* 1: PUBLISH state
	* 2: Yes / No
	* </p>
	*/
	 private function _createLegend() {

		$legends = $this->_storeLegend();

		if ( empty( $legends ) || !is_array($legends) ) {
			return '';
		}//endif

		$HTMLs = array();
		$icons = false;
		$notices = false;

		// sort the table to make sure we start at group 1
		foreach( $legends as $group => $orders ) {
			if ( !empty($orders) && is_array($orders) ) {
				ksort($orders);
				foreach( $orders as $order => $legend ) {
					foreach( $legend as $image => $text ) {
						if ( $image != '/' ) {
							$img = '<span class="legend-text"><i class="fa ' . WPage::renderBluePrint( 'icon', $image ) . ' fa-lg"></i>'.$text->text.'</span>';
							$HTMLs[] = $img;
							$icons = true;
						}else {
							$HTMLnotices[] = $text;
							$notices = true;
						}//endif
					}//endforeach
				}//endforeach
			}//endif
		}//endforeach

		$html = '';
		if ($notices) {
			$img = '<span class="legend-text"><i class="fa fa-lightbulb-o fa-lg"></i></span>';
			$html .= $img . WText::lib('Notice') . ' : ' . implode( ' | ' , $HTMLnotices);
//			$html .= '<br>';
		}//endif

		if ( $icons ) $html .= WText::lib('Icon Legend') . ' : ' . implode( ' | ' , $HTMLs );
		$div = new WDiv( $html );
		$div->classes = 'legend' ;
		return $div->make();	// $this->crlf.

	}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $image
 */
//	private function _getIcon( $image ) {
//
//		switch( $image ) {
//			case 'publish':
//				$html = 'power-off';
////				$html = 'check-circle';
//				if ( $this->_columniconcolor ) $html .= ' text-success';
//				break;
//			case 'unpublish':
//				$html = 'times-circle';
//				if ( $this->_columniconcolor ) $html .= ' text-danger';
//				break;
//			case 'yes':
//				$html = 'check ';
//				if ( $this->_columniconcolor ) $html .= ' text-success';
//				break;
//			case 'no':
//				$html = 'times';
//				if ( $this->_columniconcolor ) $html .= ' text-success';
//				break;
//			case 'edit':
//				$html = 'pencil-square-o';
//				if ( $this->_columniconcolor ) $html .= ' text-primary';
//				break;
//			case 'show':
//				$html = 'eye';
//				if ( $this->_columniconcolor ) $html .= ' text-info';
//				break;
//			case 'delete':
//				$html = 'trash-o';
//				if ( $this->_columniconcolor ) $html .= ' text-danger';
//				break;
//			case 'copy':
//				$html = 'eye';
//				if ( $this->_columniconcolor ) $html .= ' text-info';
//				break;
//			case 'lock':
//				$html = 'lock';
//				if ( $this->_columniconcolor ) $html .= ' text-success';
//				break;
//			case 'unlock':
//				$html = 'unlock';
//				if ( $this->_columniconcolor ) $html .= ' text-warning';
//				break;
//			case 'pending':
//				$html = 'unlock';
//				if ( $this->_columniconcolor ) $html .= ' text-warning';
//				break;
//			case 'archive':
//				$html = 'archive';
//				if ( $this->_columniconcolor ) $html .= ' text-info';
//				break;
//			case 'disabled':
//			case 'cancel':
//				$html = 'times-circle-o';
//				if ( $this->_columniconcolor ) $html .= ' text-danger';
//				break;
//			case 'refresh':
//				$html = 'refresh';
//				if ( $this->_columniconcolor ) $html .= ' text-primary';
//				break;
//			case 'preferences':
//				$html = 'cog';
//				if ( $this->_columniconcolor ) $html .= ' text-primary';
//				break;
//
//			default:
//				$html = 'times';
//				if ( $this->_columniconcolor ) $html .= ' text-primary';
//WMessage::log( 'Icon not defined:' . $image, 'error-legend-icon' );
////__START__
//				$this->codeE( 'Icon not defined:' . $image, 'error-legend-icon' );
////__END__
//				break;
//
//		}//endfct
//
//		return $html;
//	}//endfct

}//endclass
