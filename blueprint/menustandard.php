<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Menustandard_class extends Theme_Render_class {

	private $_roleHelper = null;

	private $_menuHTMLleftA = array();
	private $_menuHTMLrightA = array();

	private $_wid = 0;

	private $_navShowIcon = null;

	private $_outputLinkC = null;

/**
 *
 * This function is to render a menu type of cpanel
 * @param object $data
 */
  	public function render( $data ) {
  		static $count = 0;

  		$navUseLogo = $this->value( 'nav.uselogo' );
  		$navBrand = $this->value( 'nav.brand' );
  		$navLogoName = $this->value( 'nav.logoname' );
  		$this->_navShowIcon = $this->value( 'nav.showicon' );

  		$this->_wid = $data->wid;

  		//we need to process the tree to define the dept for each menu
  		$parentA = array( 'pkey' => 'mid', 'parent' => 'parent', 'name' => 'name' );
  		$childOrderParent=array();
		$data->elements = WOrderingTools::getOrderedList( $parentA, $data->elements, 2, false, $childOrderParent );
		$parentOrderedListA = array();
		foreach( $data->elements as $oneMenu ) {
			$parentOrderedListA[$oneMenu->parent][] = $oneMenu;
		}//endforeach
		$parentOrderedListA = array_reverse( $parentOrderedListA, true );

		$this->_outputLinkC = WClass::get( 'output.link' );

		$this->_roleHelper = WRole::get();
//		$this->_menuHTMLA = array();
		$menuLeft = '';
  		$menuRight = '';
		foreach( $parentOrderedListA as $parent => $menuSetA ) {
			$this->_createMenus( $menuSetA, $menuLeft, $menuRight );
			$this->_menuHTMLleftA[$parent] = $menuLeft;
			$this->_menuHTMLrightA[$parent] = $menuRight;
		}//endforeach


		// menu
//		$currentParent = 0;
		$menu = '';
		if ( !empty($this->_menuHTMLleftA[0]) ) {
			$menu .= '<ul class="nav navbar-nav">';
			$menu .= $this->_menuHTMLleftA[0];
			$menu .= '</ul>';
		}//endif
		if ( !empty($this->_menuHTMLrightA[0]) ) {
			$menu .= '<ul class="nav navbar-nav navbar-right">';
			$menu .= $this->_menuHTMLrightA[0];
			$menu .= '</ul>';
		}//endif

		$html = '<nav class="navbar navbar-default" role="navigation">';
		$html .= '<div class="container-fluid">';

		$count++;
		$id = 'zHozMenu' . $count;

		if ( $navUseLogo ) {
			if ( IS_ADMIN ) {
				$brand = '<i class="fa app-joobi-logo"></i>';	// fa-home
				WPage::addCSSFile( 'fonts/app/css/app.css' );
			} else {
				if ( 'app-joobi-logo' == $navLogoName ) {
					WPage::addCSSFile( 'fonts/app/css/app.css' );
				}//endif
				$brand = '<i class="fa ' . $navLogoName . '"></i>';
			}//endif
		} else {
			$brand = $navBrand;
		}//endif

		//Brand eg Joobi Icon for toggle on mobile device
		$html .= '<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#' . $id . '">
<span class="sr-only">' . WText::translate( 'Toggle navigation' ). '</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#">' . $brand . '</a>
</div>';

		// the brand can be the joobi logo or the Home menu
		$html .= '<div class="collapse navbar-collapse" id="' . $id . '">';

		$html .= $menu;

		$html .= '</div>';	// <!-- /.navbar-collapse -->
		$html .= '</div>';	// <!-- /.container-fluid -->
		$html .= '</nav>';

		return $html;

  	}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $menuSetA
 * @param unknown_type $direction
 */
  	private function _createMenus( $menuSetA, &$menuLeft, &$menuRight ) {

  		$menuLeft = '';
  		$menuRight = '';
		foreach( $menuSetA as $oneMenu ) {

			if ( !$this->_roleHelper->hasRole( $oneMenu->rolid ) ) continue;
			WTools::getParams( $oneMenu );
			if ( !empty($oneMenu->requirednode) ) {
				$nodeExist = WExtension::exist( $oneMenu->requirednode );
				if ( empty($nodeExist) ) continue;
			}//endif

			if ( !empty($oneMenu->bfloat) && 'right' == $oneMenu->bfloat ) {
				$menuRight .= $this->_makeOneLink( $oneMenu );
			} else {
				$menuLeft .= $this->_makeOneLink( $oneMenu );
			}//endif

		}//endforeach

		return;

  	}//endfct




/**
 *
 * Enter description here ...
 * @param unknown_type $oneMenu
 */
  	private function _makeOneLink( $oneMenu ) {

  		$menu = '';

  		if ( 102 == $oneMenu->type ) {

			$buttonObject = Output_Mlinks_class::loadButtonFile( $oneMenu );
			if ( null === $buttonObject ) {
				$obj = null;
				return $obj;
			}//endif

			$buttonObject->initialiseMenu( $this->_data );
			$status = $buttonObject->make();
			if ( false === $status ) return '';

			foreach( $buttonObject as $key => $val ) $oneMenu->$key = $val;

		}//endif


  		if ( $this->_navShowIcon && !empty( $oneMenu->faicon ) ) {
  			$oneMenu->name = '<i class="fa ' . $oneMenu->faicon . '"></i>' . $oneMenu->name;
  		}//endif

  		$target = '';
		$this->_outputLinkC->wid = $this->_wid;
		if ( !empty($oneMenu->action) ) {

			if ( 102 == $oneMenu->type ) {

				if ( !empty($oneMenu->link) ) {
					$link = $oneMenu->link;
				//we check if the link has index.php if it does we dont convert the link
				} elseif ( strpos( $oneMenu->action, 'index.php' ) !== false ) {
					//we dont change the link
					$link = $oneMenu->action;
				} else {
					if ( substr( $oneMenu->action, 0, 11 ) != 'controller=' ) $oneMenu->action = 'controller=' . $oneMenu->action;
					//no index.php we convert
					$link = $this->_outputLinkC->convertLink( $oneMenu->action, '', null );
				}//endif
			} elseif ( 55 == $oneMenu->type ) {
				//we dont change the link
				$link = $oneMenu->action;
				$target = ' target="_blank"';
			} else {
				if ( substr( $oneMenu->action, 0, 11 ) != 'controller=' ) $oneMenu->action = 'controller=' . $oneMenu->action;
				$link = $this->_outputLinkC->convertLink( $oneMenu->action, '', null );
			}//endif

		}else $link = '#';


  		if ( !empty($this->_menuHTMLleftA[$oneMenu->mid]) || !empty($this->_menuHTMLrightA[$oneMenu->mid]) ) {
  			if ( $oneMenu->indentTreeNumber < 1 ) {
	  			//it means this is a parent and there is childs
	  			$menu .= '<li class="dropdown"><a' . $target . ' href="' . $link . '" class="dropdown-toggle" data-toggle="dropdown">';
	  			$menu .= $oneMenu->name;
	  			$menu .= '<b class="caret"></b></a><ul class="dropdown-menu">';
	  			if ( !empty($this->_menuHTMLleftA[$oneMenu->mid]) ) $menu .= $this->_menuHTMLleftA[$oneMenu->mid];
	  			if ( !empty($this->_menuHTMLrightA[$oneMenu->mid]) ) $menu .= $this->_menuHTMLrightA[$oneMenu->mid];
	  			$menu .= '</ul></li>';

  			} else {
	  			$menu .= '<li class="dropdown-submenu"><a' . $target . ' href="' . $link . '">';
	  			if ( !empty($oneMenu->description) ) {
	  				$menu .= $oneMenu->name;
	  			} else {
	  				$menu .= $oneMenu->name;
	  			}//endif
	  			$menu .= '</a><ul class="dropdown-menu">';
	  			if ( !empty($this->_menuHTMLleftA[$oneMenu->mid]) ) $menu .= $this->_menuHTMLleftA[$oneMenu->mid];
	  			if ( !empty($this->_menuHTMLrightA[$oneMenu->mid]) ) $menu .= $this->_menuHTMLrightA[$oneMenu->mid];
	  			$menu .= '</ul></li>';
  			}//endif

  		} else {
  			$menu .= '<li>';
  			$noLink = ( empty($link) || '#'== $link ? ' class="noLink"' : '' );
  			$menu .= '<a' . $target . $noLink .  ' href="' . $link . '">';
  			if ( !empty($oneMenu->description) ) {
  				$menu .= '<span>' . $oneMenu->name . '</span>';
  				$menu .= '<span class="hint">' . $oneMenu->description . '</span>';
  			} else {
  				$menu .= $oneMenu->name;
  			}//endif
  			$menu .= '</a>';
  			$menu .= '</li>';
  		}//ednif

  		return $menu;

  	}//endfct



/**
 * THIS IS TO RENDER WITH A JOOMLA MENU
 * Enter description here ...
 * @param unknown_type $data
 */
// 	public function renderXXX( $data ) {
//
//  		$html = '';
//		$this->firstFormName = $data->firstFormName;
//		$this->namekey = $data->namekey;
//		$this->wid = $data->wid;
//		$this->subtype = $data->subtype;
//		$this->nestedView = $data->nestedView;
//		$this->menuSize = $data->menuSize;
//
//		$list = array();
//		//convert joobi menus into Joomla menu
//		foreach( $data->elements as $oneMenu ) {
//			$menu = new stdclass;
//			$menu->id = $oneMenu->mid;
//
//			$menu->level = '';
//			$menu->deeper = '';
//			$menu->shallower = '';
//			$menu->parent = '';
//			$menu->active = false;
//
//			$menu->flink = WPage::link( $oneMenu->action );
//			$menu->type = 'component';
//			$menu->title = $oneMenu->name;
//			$menu->anchor_css = '';
//			$menu->anchor_title = '';
//			$menu->menu_image = '';
//			$menu->browserNav = 0;
//
//			$list[] = $menu;
//
//		}//endforeach
//
//		$active_id = 0;
//		$path = array();
//		$class_sfx = '';
//		$showAll = 1;
//
//		//we need to prepare all the variables
//		$app = JFactory::getApplication();
//		$template = $app->getTemplate();
//		$module = JModuleHelper::getModule('breadcrumbs');
//		$params = new JRegistry;
//		$params->loadString($module->params);
//
//		$pathMenu = JPATH_THEMES . DS . $template . DS . 'html' . DS . 'mod_menu' . DS . 'default.php';
//
//		$systemFile = WGet::file();
//		if ( !$systemFile->exist( $pathMenu ) ) {
//		}//endif
//
//		$systemFile = WGet::file();
//		if ( $systemFile->exist( $pathMenu ) ) {
//			$count = count( $list );
//			include( $pathMenu );
//			$html = ob_get_clean();
//		}//endif
//
//		return $html;
//
//  	}//endfct

}//endclass
