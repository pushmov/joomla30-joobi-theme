<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/


/**
 *
 * this class is the object to create a menu with all the properties possible to use
 */
class WRender_Menu_classObject {

	public $subtype = null;
	public $elements = array();
	public $firstFormName = '';
	public $wid = 0;
	public $namekey = '';
	public $type = 0;
	public $nestedView = false;
	public $menuSize = 0;
	public $formID = '';
	public $formInfoO = null;
	public $typeForm = false;

}//endclass


class WRender_Menu_class extends Theme_Render_class {

/**
 *
 * This function is to render a button
 * @param object $data
 */
  	public function render( $data ) {

  		if ( empty( $data->elements ) ) return '';
//debug( 5666115, $data->subtype );
  		$html='';
  		switch( $data->subtype ) {
  			case 110:	//cpanel button menu
  				$html = WPage::renderBluePrint( 'menupanel', $data );
  				break;
  			case 115:	// standard horizontal menu to navigate from one view to the other
  				$html = WPage::renderBluePrint( 'menustandard', $data );
  				break;
  			default:	// menu for a view
  				$html = WPage::renderBluePrint( 'menuview', $data );
  				break;
  		}//endswitch

		return $html;

  	}//endfct

}//endclass

class WButtons_default extends WButtons_standard {
}//endclass
