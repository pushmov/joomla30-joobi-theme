<?php
defined('JOOBI_SECURE') or die('J....');

/**
* @version $Id:html.pane.php 502 2007-01-14 01:31:04Z c $
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/

/**
* <p>Class to render a container</p>
* @author Joobi Team
*
* WPage::renderBluePrint( 'frame', $data );
*
*/

class WRender_Frame_class extends Theme_Render_class {


	  	public function render( $data ) {
//debug( 56622345, $data->frame );

	  		$params = $data->params;
			//we had the javascript by reference to modify it from any element
			switch( $data->frame ) {
				case 11:	//default layout of the form
				case 'list':
					WLoadFile( 'blueprint.frame.list', JOOBI_DS_THEME_JOOBI );
					$frame = new WPane_list( $params );
					break;
				case 21:
				case 'tab':
					$formTabFade = $this->value( 'form.tabfade' );
					WLoadFile( 'blueprint.frame.tabs', JOOBI_DS_THEME_JOOBI );
					$frame = new WPane_tabs( $params );
					$frame->fade = $formTabFade;
					break;
				case 27:
					$formTabFade = $this->value( 'form.tabfade' );
					WLoadFile( 'blueprint.frame.tabvertical', JOOBI_DS_THEME_JOOBI );
					$frame = new WPane_tabvertical( $params );
					$frame->fade = $formTabFade;
					break;

//				case 77:
//debug( 56622345, 'NOT USED ANY MORE, can be removed now' );
//					$frame = new WPane_tabjsfree($params);
//					break;
				case 31:
				case 91:
				case 'div':
					WLoadFile( 'blueprint.frame.div', JOOBI_DS_THEME_JOOBI );
					$frame = new WPane_div( $params );
					break;
				case 51:
				case 'sliders':
					WLoadFile( 'blueprint.frame.sliders', JOOBI_DS_THEME_JOOBI );
					$frame = new WPane_sliders( $params );
					break;
				case 115:
				case 'nopane': 	// ?? what is that
					WLoadFile( 'blueprint.frame.nopane', JOOBI_DS_THEME_JOOBI );
					$frame = new WPane_nopane( $params );
					break;
				case 83:	// multiple columns
				case 'column':
					WLoadFile( 'blueprint.frame.column', JOOBI_DS_THEME_JOOBI );
					$frame = new WPane_column( $params );
					break;
				case 84:
				case 'row':
					WLoadFile( 'blueprint.frame.row', JOOBI_DS_THEME_JOOBI );
					$frame = new WPane_row( $params );
					break;
				default:
					WLoadFile( 'blueprint.frame.list', JOOBI_DS_THEME_JOOBI );
					$frame = new WPane_list( $params );
					break;

			}//endswitch

			return $frame;

	  	}//endfct


}//endclass


/**
* <p>class for creating a pane.</p>
* @author Joobi Team
*/
abstract class WPane extends WElement {

	var $useCookies = true;

/** <p>get an instance of the pane</p>
 * @param string $behavior type of tab
 * @param array $params parameters
	*/
	function &getInstance( $behavior='tabs', $params = array() ) {

		$classname = 'WPane_'.$behavior;
		$instance = new $classname($params);

		return $instance;
	}//endfct

}//endclass


/**
* <p>Class to create a tab like pane.</p>
* @author Joobi Team
*/
//class WPane_mootabs extends WPane {
//
//
///**
//	* Constructor
// * @param array $params parameters
//	*/
//	function __construct( $params=array() ) {
//		parent::__construct( $params );
//	}//endfct
//
//
///** <p>Start the pane</p>
//	 * @param array $params parameters
//*/
//	public function startPane( $params ) {
//
//		$this->_head = '<div name="maintab" id="main'.$params->id.'" >';
//
//		$this->_title[]='<ul>';
//
//		$js = 'joobiTabs=true;';
//		$js.= 'setTimeout("var myTabs1 = new mootabs(\'main'.$params->id.'\',{width: \'300px\',height: \'250px\',useAjax: false});",100);';// .$this->crlf;	//leave the int !!!
//		$js .= $this->crlf;
//
//		WPage::addJSLibrary( 'mootools' );
//		WPage::addJSScript($js,'mootabs');
//
//
//	}//endfct
//
//
//
///** <p>End the pane</p>
//	*/
//	public function endPane() {
//		$this->_title[]='</ul>';
//		$this->content = $this->_head.implode( "",  $this->_title).implode( "",  $this->_panel).'</div> ' . $this->crlf;
//		return $this->content;
//	}//endfct
//
//
///** <p>Start one tab</p>
// * @param array $params parameters
//	*/
//	public function startPage( $params ) {
//		$this->_panel[$params->id] = '<div name="tabjb" class="tab-page" id="'.$params->id.'" >' . $this->crlf;
//		$this->_title[]= '<li title="'.$params->id.'" >'.$params->text.'</li>';	// class="'.WLAYOUTSTYPE_ELEMENT.'"
//	}//endfct
//
///** <p>End the pane</p>
//	*/
//	function add( $content ) {
//		end($this->_panel);
//		$id=key($this->_panel);
//		return $this->_panel[$id] .= $content;
//	}//endfct
//
///** <p>End one tab</p>
//	*/
//	public function endPage() {
//		end($this->_panel);
//		$id=key($this->_panel);
//		return $this->_panel[$id] .= '</div>' . $this->crlf;
//	}//endfct
//
//
//}//endclass



