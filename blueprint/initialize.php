<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Initialize_class extends Theme_Render_class {


/**
 *
 * This function is to display the path of the browsed category
 * @param array of object $branchesA
 */
  	public function render( $data=null ) {

  		//can be used to retreive some parameters
  		if ( !empty($data) ) {
  			if ( $data == 'brand' ) {
  				WPage::addCSSFile( 'fonts/app/css/app.css' );
  				return '<i class="fa app-joobi-logo"></i>';
  			} elseif ( $data == 'font-awesome' ) return $this->_addFontAwesome( false );
  			elseif ( $data == 'font-awesome-animation' ) return $this->_addFontAwesome( true );
  			else return $this->value( $data );
  		}//endif



		WPage::addCSSFile( 'fonts/app/css/app.css' );

//debug( 33001121, 'initialization' );

		$themeCustomO = WGlobals::get( 'themeCustom', null, 'global' );
		if ( !empty($themeCustomO) ) {
			$this->overwriteThemePreferences( $themeCustomO );
		}//endif


		if ( defined( 'T3_TEMPLATE' ) ) {	//T3 framework
			//no need it already load Bootstrap
			//font awesome is loaded already
                    
                        WPage::addCSSFile( 'css/dropdown-hover.css' );
                        WPage::addCSSFile( 'css/perfect-scrollbar.css' );
                        
                        WPage::addJSFile( 'js/perfect-scrollbar.js' );
                        WPage::addJSFile( 'js/custom.js' );

		} else {

			//Yoo theme already load the CSS
			//we know is yoo theme because it has yoo_ in the theme name
			//we dont need to load the bootstrap but we need to load font awesome

//	JSN_TPLFRAMEWORK_ID

//GANTRY_OVERRIDES_PATH
// gantry does not have the fancy icon so we need to load them ourselves


			//load Joomla bootstrap
			if ( JOOBI_FRAMEWORK == 'joomla30' ) {
//debug( 3300111 );
				$document = JFactory::getDocument();
				JHtml::_( 'bootstrap.framework' );
				JHtml::_( 'bootstrap.loadCss', false, $document->direction );
			}//endif


			//for sub menu
			WPage::addJSLibrary( 'jquery' );

			//for now I comment this line for the carrousel to work let see what happen

			WPage::addJSFile( 'js/bootstrap.js' );
			WPage::addJSFile( 'js/menu.js' );

//debug( 6662234, 'WE NEED TO ADD OUR OWN BOOTSTRAP HERE SO WE NEED TO FIND THE PROPER CSS TO ADD FOR THE MENU' );
			WPage::addJSFile( 'js/extrascript.js' );

			$this->_addFontAwesome();


		}//endif


		//add our own bootstrap file
		$skin = $this->value( 'skin' );
		$noBootstrap = $this->value( 'nobootstrap' );


		if ( empty($noBootstrap) ) {
			if ( !empty( $skin ) ) {

				$pluginInstalled = WGlobals::get( 'pluginThemeSystem', false, 'global' );
				if ( empty( $pluginInstalled ) ) {
					$this->userW( 'In order to use a customized skin for the theme, the Joomla plugin "Bootstrap Skins for Theme" needs to be publish!' );
					if ( !defined( 'T3_TEMPLATE' ) ) WPage::addCSSFile( 'css/bootstrap.css' );
				} else {
					$explodeSkinA = explode( '.', $skin );
					$path = WPage::fileLocation( 'css', $explodeSkinA[0] . '/css/' . $explodeSkinA[1] . '.css', 'skin' );
					WGlobals::set( 'themeCustomSkin',  $path, 'global' );
				}//endif
			} else {
				if ( !defined( 'T3_TEMPLATE' ) ) WPage::addCSSFile( 'css/bootstrap.css' );
			}//endif
		}//endif


  		return true;

  	}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $animation
 */
  	private function _addFontAwesome( $animation=false ) {
  		static $ft = true;
  		static $ftanm = true;

  		$font_awesome = $this->value( 'font.awesome' );

		if ( ('auto' == $font_awesome || 1 == $font_awesome) && $ft ) {
			//add font-awesome for alert message
			WPage::addCSSFile( 'fonts/font-awesome/css/font-awesome.css', 'theme', '//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' );
			$ft = false;
		}//endif

		if ( $animation && $ftanm ) {
			WPage::addCSSFile( 'fonts/font-awesome/css/font-awesome-animation.css', 'theme' );
			$ftanm = false;
		}//endif

  	}//endfct

}//endclass
