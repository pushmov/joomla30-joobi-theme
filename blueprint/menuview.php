<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Menuview_class extends Theme_Render_class {

	private $_data = null;

	private $_roleHelper = null;

	private $_colorDefault = '';
//	private $_predefinedButtonA = array( 'edit', 'add', 'delete', 'copy' );


/**
 *
 * This function is to render a menu type of cpanel
 * @param object $data
 */
  	public function render( $data ) {

  		$html = '';
		$this->_data = $data;

		$toolbarGroup = $this->value( 'toolbar.group' );
		$this->_toolbarColor = $this->value( 'toolbar.color' );
		$this->_toolbarIcon = $this->value( 'toolbar.icon' );
		$this->_toolbarPosition = $this->value( 'toolbar.position' );
		$this->_toolbarIconSize = $this->value( 'toolbar.iconsize' );
		$this->_toolbarIconColored = $this->value( 'toolbar.iconcolored' );
		$this->_colorDefault = $this->value( 'toolbar.colordefault' );
//		if ( 'default' == $this->_colorDefault ) $this->_colorDefault = '';	// we dont pass it if it already the default

		//reverse the order
//		$parentOrderedListA = array_reverse( $data->elements );

		$this->_roleHelper = WRole::get();

		$countBtn = count($data->elements);
		if ( $toolbarGroup && $countBtn > 1 ) $html = '<div class="btn-group">';

		//we need to prepare all the variables
		foreach( $data->elements as $oneMenu ) {

  		if ( !$this->_roleHelper->hasRole( $oneMenu->rolid ) ) continue;

  		if ( $oneMenu->type == 90 ) {
  			//divider
  			if ( $toolbarGroup ) $html .= '</div><div class="btn-group">';
  			continue;

  		}//endif

  		$html .= $this->_makeOneButton( $oneMenu );

		}//endforeach
		if ( $toolbarGroup && $countBtn > 1 ) $html .= '</div>';

		//do the wrapper
		$html = '<div class="btn-toolbar" role="toolbar">' . $html . '</div>';
		return $html;

  	}//endfct



/**
 *
 * Enter description here ...
 * @param unknown_type $oneMenu
 */
  	private function _makeOneButton( $oneMenu ) {
  		static $menuFileA = array();

		WTools::getParams( $oneMenu );

		if ( !empty($oneMenu->requirednode) ) {
			$nodeExist = WExtension::exist( $oneMenu->requirednode );
			if ( empty($nodeExist) ) return '';
		}//endif

  		$objButtonO = WPage::newBluePrint( 'button' );
		if ( !empty( $oneMenu->themepref ) ) {
			$explodeA = explode( '.', $oneMenu->themepref );
			$objButtonO = WPage::newBluePrint( 'prefcatalog' );
			$objButtonO->type = $explodeA[1];
		} else {
//			$objButtonO = WPage::newBluePrint( 'button' );
			$objButtonO->type = 'button';	// 	button
		}//endif


  		if ( $oneMenu->type > 1 && $oneMenu->type < 18 && ( empty($oneMenu->faicon) || empty($oneMenu->color) ) ) {
    		$this->_predefinedButton( $oneMenu );
  		}//endif

		//processing of the custom buttons
		$buttonObject = Output_Mlinks_class::loadButtonFile( $oneMenu );
		if ( null === $buttonObject ) {
			$obj = null;
			return $obj;
		}//endif

		$buttonObject->initialiseMenu( $this->_data );
		$status = $buttonObject->make();
		if ( false === $status ) return null;

		if ( !empty($status) && true !== $status ) return $status;

		foreach( $buttonObject->buttonO as $key => $val ) if ( !isset($oneMenu->$key) || $oneMenu->$key !=$val ) $oneMenu->$key = $val;

  		$objButtonO->text = $oneMenu->name;
//  		$objButtonO->type = 'infoLink';	// 	button
  		$objButtonO->float = 'right';

  		if ( $this->_toolbarColor ) {
    		if ( !empty( $oneMenu->color ) ) $objButtonO->color = $oneMenu->color;
  		}//endif

  		if ( $this->_colorDefault ) $objButtonO->colorDefault = $this->_colorDefault;

  		if ( $this->_toolbarIconColored ) $objButtonO->coloredIcon = true;

  		if ( $this->_toolbarIconSize ) $objButtonO->iconSize = $this->_toolbarIconSize;


//debug( 6772217, $oneMenu->faicon );
  		if ( $this->_toolbarIcon && !empty( $oneMenu->faicon ) ) {
    		$objButtonO->icon = $oneMenu->faicon;
  		} else {
  			//get the icon based on the type
  		}//endif

  		//deal withg popup
  		if ( !empty($oneMenu->popheight) || !empty($oneMenu->popheight) ) {
  			$oneMenu->isPopUp = true;
			$objButtonO->popUpWidth = ( !empty($oneMenu->popwidth) ? $oneMenu->popwidth : '80%' );
			$objButtonO->popUpHeight = ( !empty($oneMenu->popheight) ? $oneMenu->popheight : '80%' );
			$objButtonO->popUpIs = true;
  		}//endif

		if ( !empty($buttonObject->link) ) {
			$objButtonO->link = $buttonObject->link;	//  '#' != $buttonObject->link
			// this is unfortunately a hack because we realized this potenital conflict problem too late
			// the problem is when we use for isntance precatalog and we have an in between type....
			// the precatalog type and the button type are not the same
			$objButtonO->typeReplacement = $objButtonO->type;
			$objButtonO->type = 'link';
		} elseif ( !empty($objButtonO->link) ) {

			//dont replace the link
		} elseif ( !empty($oneMenu->isPopUp) ) {
			$objButtonO->link = WPage::linkPopUp( $oneMenu->action );

//			$popWidth = ( !empty($oneMenu->popwidth) ? $oneMenu->popwidth : '80%' );
//			$popHeight = ( !empty($oneMenu->popheight) ? $oneMenu->popheight : '80%' );
//			$objButtonO->extraTags = WPage::createPopUpRelTag( $popWidth, $popHeight );
			//those are hacks to be able to integrate Joomla and Bootstrap
//			$objButtonO->extraClasses = 'modal';	// this is only for Joomla 3.0 I believe anyway
//			$objButtonO->extraStyle = 'display:inline-block;';

			$objButtonO->popUpIs = true;
			$objButtonO->popUpWidth = ( !empty($oneMenu->popwidth) ? $oneMenu->popwidth : '80%' );
			$objButtonO->popUpHeight = ( !empty($oneMenu->popheight) ? $oneMenu->popheight : '80%' );


		} elseif ( $oneMenu->type < 50 || !empty( $buttonObject->buttonO->buttonJS ) ) {
			$objButtonO->link = '#';	// $oneMenu->action
		} else {
			$objButtonO->link = WPage::link( $oneMenu->action );
		}//endif


		if ( !empty( $this->_toolbarIconPosition ) ) {
			$objButtonO->iconPosition = $this->_toolbarIconPosition;
		}//endif


		if ( !empty( $buttonObject->buttonO->buttonJS ) ) {
			$objButtonO->linkOnClick = $buttonObject->buttonO->buttonJS;
			$objButtonO->loading = true;	// if we have some action we add the loading of the button
		}//endif

//debug( 666611109, $objButtonO );
		if ( !empty( $oneMenu->themepref) ) {
			$html = WPage::renderBluePrint( 'prefcatalog', $objButtonO );
		} else {
			$html = WPage::renderBluePrint( 'button', $objButtonO );
		}//endif

  		return $html;


  	}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $button
 */
  	private function _predefinedButton( &$button ) {

  		$button->faicon = 'fa-';
  		switch( $button->type ) {
    		case 2:	//edit
    			$button->faicon .= 'pencil-square-o';
    			$button->color = 'primary';
    			break;
    		case 3:	//new
    			$button->faicon .= 'plus';
    			$button->color = 'success';
    			break;
    		case 4:	//save
    			$button->faicon .= 'check';
    			$button->color = 'success';
    			break;
    		case 5:	//cancel
    			$button->faicon .= 'times';
    			$button->color = 'danger';
    			break;
    		case 6:	//apply
    			$button->faicon .= 'floppy-o';
    			$button->color = 'info';
    			break;
    		case 7:	//delete
    		case 8:	//trash
    			$button->faicon .= 'trash-o';
    			$button->color = 'danger';
    			break;
    		case 9:	//back
    			$button->faicon .= 'step-backward';
    			$button->color = 'info';
    			break;
    		case 10:	//copy
    			$button->faicon .= 'files-o';
    			$button->color = 'info';
    			break;
    		case 11:	//Pulbish
    			$button->faicon .= 'plus-square-o';
    			$button->color = 'success';
    			break;
    		case 12:	//Unpulbish
    			$button->faicon .= 'minus-square-o';
    			$button->color = 'danger';
    			break;
    		case 13:	//dashboard
    			$button->faicon .= 'cogs';
    			$button->color = 'info';
    			break;
    		case 14:	//Previous
    			$button->faicon .= 'chevron-left';
    			$button->color = 'info';
    			break;
    		case 15:	//next
    			$button->faicon .= 'chevron-right';
    			$button->color = 'info';
    			break;
    		case 16:	//help
    			$button->faicon .= 'life-ring';
    			$button->color = 'primary';
    			break;
    		case 17:	//wizard
    			$button->faicon .= 'magic';
    			$button->color = 'primary';
    			break;
  		}//ednswitch

  	}//endfct

}//endclass
