<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/

/**
 *
 * this class is the object to create a button with all the properties possible to use
 */
class WRender_Button_classObject {

	/*
	 *
	 * WPage::newBluePrint( 'button' );
	 *
	 *
	 * class or style ... still need to define how we do that one:
	 * 						-> standard ( blue checkout button )
	 * 						-> infoLink
	 * 						-> viewAll
	 */
	public $style = 'button';

	public $text = '';	//text to show on the button or link

	public $useTitle = true;	// define if we need to add title to the button or link
	public $title = '';	// the title on the link

	public $link = '#';	//falcutive link that we put around the button
	public $linkOnClick = '';	// the js command on teh link
	public $float = '';	// left / right normal
	public $size = '';	// size: extra small, small, medium ( standard), large ....  so we differentiate between link

	public $color = '';	// define the color of the button
	public $colorDefault = 'default';	// define the default color of the button

	public $iconShow = true;
	public $icon = '';	// ( add, next, before, )
	public $iconPosition = null; // right, left
	public $iconSize = ''; // icon size
	public $coloredIcon = false;	// define if the icon should have color

	public $iconLocation = '';	// before, after ( if RTL we reverse ;P ) default after
	public $tooltip = '';	//a text which should be rendered as a tooltip

	/* define if we need to open the link into a popup */
	public $popUpIs = false;
	public $popUpWidth = '80%';
	public $popUpHeight = '80%';

	public $target = '';		// the target for the link when we use link
	public $extraTags = null; 	// possible extra tag to add to the link
	public $extraClasses = '';	// possible extra class name to add to the link
	public $extraStyle = ''; 	// possible extra style on the button or link

	/*  Only for STYLY button */
	public $buttonType = 'button';

	public $loading = false;	//define if the Loading shoudl appear when we click on the button

}//endclass



class WRender_Button_class extends Theme_Render_class {

	private static $btnColor = null;
	private static $btnDefaultColor = null;
	private static $btnIcon = null;
	private static $btnIconPosition = null;


/**
 *
 * This function is to render a button
 * @param object $data
 *
 * 			WPage::renderBluePrint( 'button', $objButtonO );
 *
 * 				-> class or style ... still need to define how we do that one:
 * 						-> standard ( blue checkout button )
 * 						-> infoLink
 * 						-> viewAll
 * 				-> text: text to render
 * 				-> link: falcutive link that we put around the button
 * 				-> linkOnClick
 * 				-> wrapperDiv
 * 				->float: left / right normal
 * 				-> size: small, medium ( standard), large ....  so we differentiate between link
 * 				-> color:
 * 				-> icon:	( add, next, before, )
 * 				-> iconLocation:	before, after ( if RTL we reverse ;P ) default after
 *
 *				->popUpIs
 *
 * 				->buttonType = type of button
 * 				->tooltip = a text which should be rendered as a tooltip
 * 				->value: true => on
 */
  	public function render( $data ) {

  		if ( empty($data->type) ) $data->type = 'infoLink';
  		if ( 'standard' == $data->type ) {
			return $data->text;
		}//endif

		//chcek on preferences
		if ( !isset( self::$btnColor ) ) {
			self::$btnColor = $this->value( 'button.color' );
			self::$btnDefaultColor = $this->value( 'button.defaultcolor' );
			if ( empty(self::$btnDefaultColor) ) self::$btnDefaultColor = 'default';
			self::$btnIcon = $this->value( 'button.icon' );
			self::$btnIconPosition = $this->value( 'button.iconposition' );
		}//endif


		//if we should not show any color we overwrite the color with empty
		if ( empty(self::$btnColor) ) $data->color = '';

		if ( !isset( $data->colorDefault ) ) $data->colorDefault = self::$btnDefaultColor;
		if ( !isset( $data->iconShow ) ) $data->iconShow = self::$btnIcon;
		if ( !isset( $data->iconPosition ) ) $data->iconPosition = self::$btnIconPosition;


		if ( empty($data->link) ) $data->type = 'button';

		if ( 'button' == $data->type ) {
			if ( empty( $data->buttonType ) ) $data->buttonType = 'button';
			$html = '<button type="' . $data->buttonType . '"';
		} else {
			//	link
			$html = '<a href="' . $data->link . '"';
			if ( !empty($data->target) ) $html .= ' target="' . $data->target . '"';
		}//endif

		if ( !empty($data->popUpIs) && !empty($data->link) ) {
			$data->extraTags = WPage::createPopUpRelTag( $data->popUpWidth, $data->popUpHeight );
//			$data->extraClasses .= ' modal';
//			$data->extraStyle = 'display:inline-block;';
		}//endif


			//orignal but removed when dont the button for toolbar for lisitng and form
			if ( !empty( $data->color ) && empty($data->coloredIcon) ) {
				$html .= ' class="btn btn-' . $data->color;
			} else {
				$html .= ' class="btn btn-' . $data->colorDefault;
//				$html .= ' class="btn btn-default';
			}//endif

			if ( !empty($data->extraClasses) ) {
				$html .= ' ' . $data->extraClasses;
			}//endif

			//define the size
			if ( !empty( $data->size ) ) {
				switch( $data->size ) {
					case 'large':
					case 'xlarge':
					case 'xxlarge':	// just for backward compatibiltiy purpose
					case '2xlarge':
					case '3xlarge':
					case '4xlarge':
						$html .= ' btn-lg';
						break;
					case 'small':
						$html .= ' btn-sm';
						break;
					case 'xsmall':
						$html .= ' btn-xs';
						break;
					default:
						break;
				}//endswitch

			}//endif

			$html .= '"';	// closing the class

			if ( !empty($data->linkOnClick) ) {
				$html .= ' onclick="' . $data->linkOnClick . '"';
			}//endif

			if ( !empty($data->extraStyle) ) {
				$html .= ' style="' . $data->extraStyle . '"';
			}//endif

			if ( !empty($data->extraTags) ) {
				$html .= $data->extraTags;
			}//endif

			//loading...
			if ( !empty($data->loading) ) {
				$loagingMessage = WText::lib( 'Loading' );
				$html .= ' data-loading-text="' . $loagingMessage . '..."';
//debug( 56112234, 'MAKE SURE THE LOADING BUTTON IS TRANSLATED' );
			}//endif

			if ( !empty($data->useTitle) && empty($data->title) ) {
				//we need to folter because the button can't have HTML in it only text
				//the title is used for accessibility purpose on the link an dbutton
				$data->title = WGlobals::filter( $data->text, 'phrase' );
			}//endif

			if ( !empty($data->title) ) $html .= ' title="' . $data->title . '"';
			if ( !empty($data->id) ) $html .= ' id="' . $data->id . '"';

			$html .= '>';

			$iconHTML = '';
			if ( !empty($data->icon) ) {
//				$html .= '<span class="icon-' . $data->icon . '"></span>';
				$iconHTML = '<i class="fa ' . $data->icon;	// . ' fa-lg';
				if ( !empty($data->iconSize) ) {
		 			switch( $data->iconSize ) {
		 				case 'medium':
		  					break;
		  				case 'large':
		  					$iconHTML .= ' fa-lg';
		  					break;
		  				case 'xlarge':
		  					$iconHTML .= ' fa-2x';
		  					break;
		  				case 'xxlarge':
		  				case '2xlarge':
		  					$iconHTML .= ' fa-3x';
		  					break;
		  				case '3xlarge':
		  					$iconHTML .= ' fa-4x';
		  					break;
		  				case '4xlarge':
		  					$iconHTML .= ' fa-5x';
		  					break;
		  				default:
		  					$iconHTML .= ' ' . $data->iconSize;
		  					break;
		  			}//endswitch
				}//endif

				if ( !empty($data->coloredIcon) ) {
					if ( empty($data->color) ) $data->color = 'primary';
					$iconHTML .= ' text-' . $data->color;
				}//endif
				$iconHTML .= '"></i>';

			}//endif

			if ( !empty($data->iconPosition) && 'right' == $data->iconPosition ) {
				$html .= $data->text;
				$html .= $iconHTML;
			} else {
				$html .= $iconHTML;
				$html .= $data->text;
			}//endif


		if ( 'button' == $data->type ) {
			$html .= '</button>';
		} else {
			$html .= '</a>';
		}//endif

		return $html;

  	}//endfct

}//endclass
