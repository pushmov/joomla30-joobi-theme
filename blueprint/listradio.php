<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Listradio_class extends Theme_Render_class {


/**
	 * Generates just the option tags for an HTML select list
	 * @param	array	An array of objects
	 * @param	string	The name of the object variable for the option value
	 * @param	string	The name of the object variable for the option text
	 * @param	mixed	The key that is selected (accepts an array or a string)
	 * @param	bolean	$data->disable or not the option
	 * @returns	string	HTML for the select list
	 */
	public function render( $data ) {	//the trnaslate option can be removed
		static $count = 1;

		if ( empty($data->listA) ) return '';

		if ( empty($data->tagID ) ) $data->tagID = $data->tagName;
		$optionGroup = false;
		if ( $data->colnb > 0 ) {
			//count the columns and add <BR>
			$columnCount = true;
		} else {
			$columnCount = false;
		}//endif

//debug( 6767832, 'DO WE STILL HAVE THOSE TAGS WHY WE USE THIS????/' );
//		if ( empty($this->classes) ) $this->classes = 'joobiform';
		if ( isset($this->classes) ) $data->tagAttributes .= ' class="'.$this->classes.'"';
		if ( isset($this->style) ) $data->tagAttributes .= ' style="'.$this->style.'"';
		if ( isset($this->align) ) $data->tagAttributes .= ' align="'.$this->align.'"';

		$typeSlect = 'checked="checked"';
		if ( $data->radioType ) {
			// checkbox type
				$typeHTML = 'checkbox';
				$nameHTML = $data->tagName.'[]';
		} else {
			// radio type
				$typeHTML = 'radio';
				$nameHTML = $data->tagName;
		}//endif

		$icol = 1;

		$html = '';

		if ( $data->radioStyle=='checkBox' && $columnCount ) {
			$html .= '<div class="col-sm-10">';
			$html .= '<div class="checkBoxAlign">';
			$isCheckBox = true;
		} else {
			$isCheckBox = false;
		}//endif


		if ( !empty($data->requiredCheked) ) {
			$myText = WGlobals::get( 'requireCheckedText', '', 'global' );
			$onClick = 'onclick="' . WView::checkTerms( count($data->listA), $myText ) .'"';
		} else {
			$onClick = '';
		}//endif


		foreach( $data->listA as $key => $value ) {

			if ( $data->arrayType ) {
				$listKey = $key;
				$listValue = $value;
			} else {
				$valKey = $data->propertyKey;
				$listKey = $value->$valKey;
				$valVal = $data->propertyText;
				$listValue = $value->$valVal;
			}//endif

			if ( substr( $listValue, 0, 2 ) == '--' ) {
				//if we need a OPTION GROUP

				if ( $optionGroup ) {
					$html .= '</fieldset>' . $this->crlf ;
				}
				$listValue = trim( substr( $listValue, 2, strlen($listValue)-2 ) );
//debug( 788832356, 'REFACTOR THIS !!!! ' );
				$html .= '<fieldset id="' . $data->tagID . '" class="'.$classes.'"><legend>'.$listValue.'</legend>';
				$optionGroup = true;

			} else {

//debug( 677622324, $value );
				// normal tag
				$extra = '';
				$selected = false;
				if ( is_array( $data->selected ) ) {
					if ( in_array( $listKey, array_values($data->selected) ) ) {
						$selected = true;
					}//endif
				} else {
					if ( $listKey == $data->selected ) {
						$selected = true;
					}//endif
				}//endif

				if ( $selected ) $extra .= $typeSlect;
				if ( $data->disable ) $extra .= ' disabled';

				if ( !empty($value->color) ) {
					switch( $value->color ) {
						case 'green':
							$color2Use = 'success';
							break;
						case 'red':
							$color2Use = 'danger';
							break;
						case 'yellow':
							$color2Use = 'info';
							break;
						case 'orange':
							$color2Use = 'warning';
							break;
						default:
							$color2Use = 'primary';
							break;
					}//endswitch
				} else {
					$color2Use = 'primary';
				}//endif

				if ( !$isCheckBox ) {
					$color2Use = 'default';
					if ( $selected ) $classLabel = 'btn active btn-' . $color2Use;
					else $classLabel = 'btn btn-default';
				}//endif

				$html .= '<label';
				if ( !empty($classLabel) ) $html .= ' class="' . $classLabel . '"';
				$html .= ' for="' . $data->tagID . $key. '">';

				$html .= '<input type="'.$typeHTML.'" name="' . $nameHTML . '" id="' . $data->tagID . $key.'" value="' . $listKey . '" '.$data->tagAttributes.' ' . $extra . $onClick . '/>';
				$html .= $listValue;

				$html .= '</label>';

				$html .= WGet::$rLine;

			}//endif

			if ( $data->radioStyle=='checkBox' || !$data->radioType ) {
				if ( $columnCount ) {

					if ( $data->colnb != $icol && intval($icol/$data->colnb) === $icol/$data->colnb ) {

						if ( $isCheckBox ) {	// 	$data->radioStyle=='checkBox'
							$html .= '</div><div class="checkBoxAlign">';//'<br />';	// <br />	// <div class="checkBoxAlign">
						} else {
							$html .= '</div><div class="radioAlign">';//'<br />';	// <br />
						}//endif
					}//endif
					$icol++;
				}//endif
			} else {
				if ( $columnCount ) {
					if ( intval($icol/$data->colnb) === $icol/$data->colnb ) $html .= '<br />';	// <br />
					$icol++;
				}//endif
			}//endif

		}//endforeach

		if ( $isCheckBox ) $html .= '</div></div>';

		$html .= WGet::$rLine;

		$html = ( $optionGroup ) ? '</fieldset>' . $html : $html;

		switch( $data->radioStyle ) {
			case 'checkBox':
				//no need to add anything else
				break;
			case 'checkBoxMultipleSelect':
				if ( $data->colnb < 4 ) $data->colnb = 4;
				$heightBigBox = $data->colnb * 26;
				$html = '<div class="radioAlign">' . $html . '</div>';
				$html = '<div class="checkBoxMultipleSelect" style="height:' . $heightBigBox . 'px;">' . $html . '</div>';
				break;
			case 'radioButton':
			default:
				$extraDisable = ( $data->disable ? ' disabled' : '' );
				$html = '<div class="btn-group btn-toggle' . $extraDisable . '" data-toggle="buttons">' . $html . '</div>';

				break;
		}//endif

		return $html;

	}//endfct


}//endclass
