<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/


/**
 *
 * WPage::newBluePrint( 'tooltips' );
 *
 */
class WRender_Tooltips_classObject {

	public $tooltips = '';	//The tooltip to show
	public $text = '';	// the text on which the tooltip need to be applied

	public $title = '';	// the title of the tooltip

	public $id = '';

	public $class = '';	// an extra class to apply to the tooltip label tag

}//endclass


class WRender_Tooltips_class extends Theme_Render_class {

	private static $_method = null;
	private static $_placement = '';
	private static $_html = true;
	private static $_trigger = '';
	private static $_location = '';

/**
 *
 * Bootstrap tooltips
 *
	$toolTipsO = WPage::newBluePrint( 'tooltips' );
	$toolTipsO->tooltips = $tempContent[$listing->lid]->overlay;	//the description of the tooltip
	$toolTipsO->title = $htmlData;	// the title of the tooltip
	$toolTipsO->text = $htmlData;	// the text to add the tooltip to
	$toolTipsO->id = $listing->lid;
	$htmlData = WPage::renderBluePrint( 'tooltips', $toolTipsO );
 *
 * @param unknown_type $data
 */
  	public function render( $data ) {

  		if ( empty($data->tooltips) ) return '';

  		//load the prefrences only once
  		if ( !isset(self::$_method) ) {
			self::$_method = $this->value( 'tooltip.method' );
			self::$_placement = $this->value( 'tooltip.placement' );
			self::$_html = $this->value( 'tooltip.html' );
			self::$_trigger = $this->value( 'tooltip.trigger' );
			if ( self::$_trigger == 'hover focus' && self::$_method == 'tooltip' ) {
				self::$_trigger = '';
			} elseif ( self::$_trigger == 'click' && self::$_method == 'popover' ) {
				self::$_trigger = '';
			}//endif
			self::$_location = $this->value( 'tooltip.location' );

			if ( JOOBI_FRAMEWORK == 'joomla30' ) {
				JHtml::_('bootstrap.tooltip');
			}//endif


  		}//endif

//debug( 67223, $data->tooltips );
		$data->tooltips = htmlspecialchars( $data->tooltips );
		if ( !empty($data->title) ) $data->title = htmlspecialchars( $data->title );


  		$html = '<label class="hasTooltip';	// rel="tooltip"
  		if ( !empty($data->class) ) $html .= ' ' . $data->class;
  		$html .= '"';
  		$html .= ' data-toggle="' . self::$_method . '"';	// data-container="body"
  		if ( !empty(self::$_placement) ) $html .= ' data-placement="' . self::$_placement . '"';
  		 if ( !empty(self::$_html) ) {
  			$html .= ' data-html="true"';
  			// if we dont allow HTML we need to remvoe HTML formatting
//  			$emailHelperC = WClass::get( 'email.conversion' );
//  			if ( !empty($emailHelperC) ) $data->tooltips = $emailHelperC->HTMLtoText( $data->tooltips, false, false );
  		}//endif
  		if ( !empty(self::$_trigger) ) $html .= ' data-trigger="' . self::$_trigger . '"';


  		if ( 'popover' == self::$_method ) {
  			// popover
debug( 677334, 'we need to include the class to render the popover' );
  			if (!empty($data->title) ) $html .= ' title="' . $data->title . '"';
  			$html .= ' content="' . $data->tooltips . '"';
  		} else {
  			// tooltip
  			if ( !empty($data->id) ) $html .= ' for="' . $data->id . '"';
  			$html .= ' title="' . $data->tooltips . '"';
  		}//endif

  		$html .= '>';
  		$html .= $data->text;
  		$html .= '</label>';

  		return $html;

  	}//endfct

}//endclass
