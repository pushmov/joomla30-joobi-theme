<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Listing_class extends Theme_Render_class {

/**
 *
 * This function is to render a button
 * @param object $data
 */
  	public function render( $data ) {

  		$obj = new stdClass;
  		$obj->tableClass = 'table';	// table-striped
  		$tableStriped = $this->value( 'table.striped' );
  		if ( $tableStriped ) $obj->tableClass .= ' table-striped';
  		$tableHover = $this->value( 'table.hover' );
  		if ( $tableHover ) $obj->tableClass .= ' table-hover';
  		$tableBorder = $this->value( 'table.border' );
  		if ( $tableBorder ) $obj->tableClass .= ' table-bordered';
  		$tableCondensed = $this->value( 'table.condensed' );
  		if ( $tableCondensed ) $obj->tableClass .= ' table-condensed';


  		$obj->tableCustomClass = false;	//allow custom class name
  		$obj->tableStyle = '';

  		$obj->transform = new WRender_Listing_functionality_class;
  		$obj->transform->showButtonColor = $this->value( 'table.buttoncolor' );
  		$obj->transform->showButtonText = $this->value( 'table.buttontext' );
  		$obj->transform->showButtonIcon = $this->value( 'table.buttonicon' );
  		$obj->transform->showButtonIconPosition = $this->value( 'table.buttonposition' );
  		$obj->transform->showButtonIconColored = $this->value( 'table.buttoniconcolored' );

		return $obj;

  	}//endfct




}//endclass


class WRender_Listing_functionality_class extends WClasses {

	public $showButtonColor = false;
	public $showButtonText = false;
	public $showButtonIcon = true;
	public $showButtonIconPosition = '';
	public $showButtonIconColored = false;


	private $htmlObj = null;

	private $_AdvSearchStatus = false;

	private $_magicHeader = false;	//a flag used to know if we need a table header in the listing

	private $_jsRun = null;


/**
 *
 * Enter description here ...
 * @param unknown_type $obj
 */
	public function tableStyle( $obj ) {

		WLoadFile( 'html-table', JOOBI_LIB_HTML_CLASS );
		$table = new WTableau( $obj );

		return $table;

	}//endfct



/**
 *
 * wrap the table
 * @param string $html
 */
	public function wrapTable( $html ) {
		return '<div class="table-responsive">' . $html . '</div>';
	}//endfct



/**
	* <p>Create the header of the page</p>
	*/
	public function createHead( $htmlObj ) {

		$this->htmlObj = $htmlObj;

		$viewID = $this->htmlObj->yid;
		$this->_AdvSearchStatus = WGlobals::getUserState( "wiev-$viewID-adv_srch" , 'viewIDadv', '', 'string' );

		if ( WGet::isDebug() ) {
			$uniqueKey = 'SearchBox_' . WGlobals::filter( $this->htmlObj->formName, 'jsnamekey' );
		} else {
			$uniqueKey = 'WZY_' . WGlobals::count('f');
		}//endif

		$this->_jsRun = WPage::actionJavaScript( $this->htmlObj->_defaultTask, $this->htmlObj->formName, array(), false, $uniqueKey );

		$searchWord = WText::lib('Search');

		$this->_createListingHead( $searchWord );


		if ( $this->_magicHeader ) {

			$filterHTML = '';
			if ( !empty( $this->htmlObj->headSeachFooter ) ) {
//				$search = new WDiv( $this->htmlObj->headSeachFooter );
//				$search->id= 'filter-search';
				$filterHTML .= $this->htmlObj->headSeachFooter;
			}//endif


			$filterPAginationDIV = '';
			$alreadyAddedPagination = false;
			if ( $this->htmlObj->pagination < 11 ) {

				//show the page number if necessary
				if ( !empty($this->htmlObj->pagiHTML)
				&& $this->htmlObj->pageNavO->pages_total > $this->htmlObj->pageNavO->getDisplayedPages() ) {
					$textMe = '<div class="pull-left pageNb">' . WText::lib('Page').' # </div>';
					$textMe .= '<input type="text" class="form-control" value="" id="wz_pagenb" name="pagenb'.$this->htmlObj->yid.'" style="text-align:center;" size="2">';
					$filterPAginationDIV .= '<div class="pagi-element pull-left">' . $textMe . '</div>';
				}//endif
				$filterPAginationDIV .= $this->htmlObj->pagiHTML;
				$alreadyAddedPagination = true;
			}//endif


			//this is the dropdown to choice how many to display
			if ( !empty( $this->htmlObj->headNumberFooter ) || !empty($this->htmlObj->pagiHTML) ) {
//				$number = new WDiv( $this->htmlObj->headNumberFooter );
//				$number->id= 'filter-totals';
				$filterPAginationDIV .= '<div class="pagi-element pull-left">' . $this->htmlObj->headNumberFooter . '</div>';
			}//endif

			//this is the pagination itxelf
			if ( !$alreadyAddedPagination && !empty( $this->htmlObj->headNumberFooter ) && $this->htmlObj->pagination > 4 && $this->htmlObj->pagination < 11 && !empty($this->htmlObj->pagiHTML) ) {
				$filterPAginationDIV .= $this->htmlObj->pagiHTML;
			}//endif

			if ( !empty($filterPAginationDIV) ) {
				$filterHTML .= '<div class="filter-pagi pull-left">' . $filterPAginationDIV . '</div>';
			}//endif


			//this is the picklist
			if ( !empty( $this->htmlObj->headListFooter ) ) {

				$filterHTML .= '<div class="filter-picklist btn-group pull-right">';
				foreach( $this->htmlObj->headListFooter as $onePicklist ) {
					$filterHTML .= '<div class="filter-one-picklist pull-left">' . $onePicklist . '</div>';
				}//endforeach
				$filterHTML .= '</div>';

			}//endif



			if ( !empty($filterHTML) ) {

				$this->htmlObj->filtersHTML .= '<div id="infoFilter" class="clearfix">' . $filterHTML . '</div>';

			}//endif

		}//endif

	}//endfct


/**
	* <p>Create the footer of the page</p>
	*/
	public function createBottom( $htmlObj ) {

		$html = '<div class="bottomPagi clearfix">';
		$html .= '<div class="center-block">';
		$html .= $this->htmlObj->pagiHTML;
		$html .= '</div>';
		$html .= '</div>';
		return $html;
	}//endfct


/**
	* <p>Create the headers</p>
	*/
	private function _createListingHead( $searchWord ) {

		if ( $this->htmlObj->dropdown ) $this->_magicHeader = true;

		//pagination
		if ( $this->htmlObj->pagination ) {

			if ( !isset($this->htmlObj->pageNavO) ) {
				$this->htmlObj->pageNavO = WView::pagination( $this->htmlObj->yid, $this->htmlObj->totalItems, $this->htmlObj->limitStart, $this->htmlObj->limitMax, $this->htmlObj->sid, $this->htmlObj->name, $this->htmlObj->_defaultTask );
			}//endif

			if ( isset($this->htmlObj->formName) && $this->htmlObj->pageNavO->total > 5 ) {
				$this->htmlObj->headNumberFooter = '<div class="pull-left pagi-display">' . WText::lib('Display') . '</div>';
				$this->htmlObj->headNumberFooter .= '<div class="pull-left">' . $this->htmlObj->pageNavO->displayNumber( $this->htmlObj->formName, $this->htmlObj->task, $this->htmlObj->pagiIncrement ) . '</div>';
			}//endif

			$this->_magicHeader = true;

			if ( $this->htmlObj->pageNavO->total > $this->htmlObj->limitMax )
				$this->htmlObj->pagiHTML = $this->htmlObj->pageNavO->getListFooter();

		}//endif

		//we have some search
		if ( !empty($this->htmlObj->search) || !empty($this->htmlObj->advSearch) ) {

			$this->_magicHeader = true;

			$basicSearchBox = '';

			$this->htmlObj->headSeachFooter = '';

			//create the search box
			if ( !$this->_AdvSearchStatus ) {

					$toolTips = WText::lib( 'Search in the columns:' ) . '<br>';
					$toolTipsA = array();
					foreach( $this->htmlObj->searchedColumnA as $oneClo ) {
						$toolTipsA[$oneClo->name] = true;
					}//endforeach
					$toolTips .= implode( ', ', array_keys($toolTipsA) );

					$basicSearchBox .= '<div class="searchbox btn-group pull-left">';

//					$design = WText::lib( 'Search information' );
//					$basicSearchBox .= '<label class="element-invisible" for="filter_search">' . $design . '</label>';
					$basicSearchBox .= '<input id="wz_search" class="hasTooltip" type="text" title=""';
					if ( empty($this->htmlObj->searchWord) ) {
						$basicSearchBox .= ' placeholder="' . $searchWord . '..."';
					} else {
						$basicSearchBox .= ' value="' . $this->htmlObj->searchWord .'"';
					}//endif

					$basicSearchBox .= ' name="search'.$this->htmlObj->yid.'" data-original-title="' . $toolTips . '">';

					$basicSearchBox .= '</div>';

					$onClickReset = 'document.getElementById(\'wz_search\').value=\'\';';

			} else {

				//call the advance sarch
				$outputAdvSearchC = WClass::get( 'output.advsearch' );
				$advanceSearchHTML = $outputAdvSearchC->createAdvanceSearch( $this->htmlObj->advSeachableA, $this->htmlObj->elements );

				$this->htmlObj->headSeachFooter .= '<div class="panel panel-info filter-advsearch"><div class="panel-heading"><h4 class="panel-title">' . WText::translate( 'Advance Search') . '</h4></div><div class="panel-body">' . $advanceSearchHTML . '</div></div>';
//				$this->htmlObj->headSeachFooter .= '<div class="filter-advsearch clearfix">' . $advanceSearchHTML . '</div>';

				$onClickReset = '';
				foreach( Output_Doc_Document::$advSearchHTMLElementIdsA as $oneID ) {
					$onClickReset .= 'document.getElementById(\'' . $oneID . '\').value=\'\';';
				}//endforeach

			}//endif




			$this->htmlObj->headSeachFooter .= '<div class="filter-search btn-group pull-left">';
			$this->htmlObj->headSeachFooter .= $basicSearchBox;
			$this->htmlObj->headSeachFooter .= $this->_searchButtonGo( $searchWord );
			$this->htmlObj->headSeachFooter .= $this->_searchButtonReset( $onClickReset );
			if ( !empty($this->htmlObj->advSearch) ) {
				//we temporary disable advance search
				$this->htmlObj->headSeachFooter .= $this->_advSearchRendering();
			}//endif
			$this->htmlObj->headSeachFooter .= '</div>';

		}//endif


	}//endfct


/**
 *
 * Enter description here ...
 */
	private function _searchButtonGo( $searchWord ) {

		$ButtonO = WPage::newBluePrint( 'button' );
		$ButtonO->type = 'button';
		$ButtonO->buttonType = 'submit';
		if ( $this->showButtonText ) $ButtonO->text = WText::lib('Go');
		if ( $this->showButtonColor ) $ButtonO->color = 'success';
		if ( $this->showButtonIconColored ) {
			$ButtonO->coloredIcon = true;
			$ButtonO->color = 'success';
		}//endif
		$ButtonO->valueOn = true;
		$ButtonO->tooltip = $searchWord;
		if ( $this->showButtonIcon ) $ButtonO->icon = 'fa-search';
		$ButtonO->linkOnClick = 'return ' . $this->_jsRun;

		return WPage::renderBluePrint( 'button', $ButtonO );

	}//endfct


/**
 *
 * Enter description here ...
 */
	private function _searchButtonReset( $clearMe ) {

		$resetText = WText::lib( 'Reset' );
		$ButtonO = WPage::newBluePrint( 'button' );
		$ButtonO->type = 'button';
		$ButtonO->buttonType = 'button';
		if ( $this->showButtonText ) $ButtonO->text = $resetText;
		$ButtonO->valueOn = true;
		if ( $this->showButtonColor ) $ButtonO->color = 'danger';
		if ( $this->showButtonIconColored ) {
			$ButtonO->coloredIcon = true;
			$ButtonO->color = 'danger';
		}//endif
		$ButtonO->tooltip = $resetText;
		if ( $this->showButtonIcon ) $ButtonO->icon = 'fa-times';
		$ButtonO->linkOnClick = $clearMe . 'return ' . $this->_jsRun;
		$this->htmlObj->headSeachFooter .= WPage::renderBluePrint( 'button', $ButtonO );

	}//endfct


/**
 *
 */
	private function _advSearchRendering() {

		if ( empty($this->htmlObj->formObj) ) return false;

		$paramsArray=array();
		$paramsArray['controller'] = 'output';
		$paramsArray['disable']=true;
		$paramsArray['ajxUrl'] = 'controller=output&task=advsearch';
		$this->_href = false;
//		WPage::addJSLibrary( 'mootools' );

//		$namekey = 'advSearchID';
		$myJS = 'return ' . WPage::actionJavaScript( 'advsearch', $this->htmlObj->formObj->id, $paramsArray, 'this', $this->htmlObj->formName . '_advSearchID', false );

//		$buttonText = ( $this->_AdvSearchStatus ? WText::lib( 'Back to Standard Search' ) : WText::lib( 'Advance Search' ) );

		if ( $this->_AdvSearchStatus ) {
			$buttonText = WText::lib( 'Back to Standard Search' );
		} else {
			$buttonText = WText::lib( 'Advance Search in the columns:' ) . '<br>';
			$toolTipsA = array();
			foreach( $this->htmlObj->advSeachableA as $oneClo ) {
				$toolTipsA[$oneClo->name] = true;
			}//endforeach
			$buttonText .= implode( ', ', array_keys($toolTipsA) );
		}//endif


		//add a hidden value for the view ID so that we can show the advacne search for that view
		$this->htmlObj->formObj->hidden( 'viewID', $this->htmlObj->yid );

		$ButtonO = WPage::newBluePrint( 'button' );
		$ButtonO->type = 'button';
		$ButtonO->buttonType = 'button';
		if ( $this->showButtonText ) $ButtonO->text = WText::lib( 'Advance' );
		$ButtonO->valueOn = true;
		if ( $this->showButtonColor ) $ButtonO->color = 'info';
		if ( $this->showButtonIconColored ) {
			$ButtonO->coloredIcon = true;
			$ButtonO->color = 'info';
		}//endif
		$ButtonO->tooltip = $buttonText;
		if ( $this->showButtonIcon ) $ButtonO->icon = 'fa-search-plus';
		$ButtonO->linkOnClick = $myJS;
		return WPage::renderBluePrint( 'button', $ButtonO );


	}//endfct


}//endfclass