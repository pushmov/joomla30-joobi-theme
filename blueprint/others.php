<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Responsive</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Others_class extends Theme_Render_class {

/**
 *
 *	WPage::renderBluePrint( 'others', $data );
 *
 * This function is to render any kind of object not render with the other files
 * @param object $data
 */
  	public function render( $data ) {

  		if ( empty($data) || empty($data->type) ) {
  			//
  			$this->codeE( 'WRender_Others_class data incomplete!' );
  			return false;
  		}//endif

  		switch( $data->type ) {
  			case 'rss':
				$html = '<a href="' . $data->link . '" target="_blank">';
				$html .= '<i class="fa fa-rss fa-2x"></i>';
				$html .= '</a>';
  			break;

  			case 'editView':
  				$html = $this->_editView( $data );
  				break;

  			case 'translationView':
  				$html = $this->_translationView( $data );
  				break;

  			case 'editModule':
  				$html = $this->_editModule( $data );
  				break;

  			default:
  				$html = '';
  				break;
  		}//endswitch

		return $html;

  	}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $data
 */
	private function _editView( $data ) {


		$image = '<i class="fa fa-edit fa-lg text-danger"></i>';	// rel="tooltip" title="' . $data->text . '"

		if ( empty($data->studio) ) $link = WPage::linkPopUp( 'controller=main-'. $data->controller . '&task=edit&eid='.$data->eid );
		else $link = WPage::linkPopUp( 'controller=view-'. $data->controller . 's&task=edit&eid='.$data->eid );

//		$link = WPage::linkPopUp( 'controller=main-'. $data->controller . '&task=edit&eid='.$data->eid );
		$linkHTML = WPage::createPopUpLink( $link, $image, '80%', '90%', '', '', $data->text );

		$content = '';
		if ( !empty($data->look) ) $data->controller = $data->look;
		switch( $data->controller ) {
			case 'form':
//			case 'menu':
				$content = $linkHTML;
				break;
			case 'form-layout':
				$content = '<div class="editElement editFormLayout">' . $linkHTML . '</div>';
				break;
			case 'listing':
				$content = '<div class="editElement editListing">' . $linkHTML . '</div>';
				break;
			case 'menu':
				$content = '<div class="editElement editMenu">' . $linkHTML . '</div>';
				break;
			case 'view':
				$content = '<div class="editElement editView">' . $linkHTML . '</div>';
				break;
			default:
				$content = '<div class="editElement">' . $linkHTML . '</div>';
				break;
		}//endswitch
//$content = $linkHTML;
//debug( 677221, $content );
		return $content;

	}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $data
 */
	private function _translationView( $data ) {

		$image = '<i class="fa fa-language fa-lg text-info"></i>';	//rel="tooltip" title="' . $data->textIcon . '"

		$lgid = WUser::get( 'lgid' );
		$link = WPage::linkPopUp( 'controller=main-translate&task=edit&type='. $data->controller .'&eid='.$data->eid . '&text=' . $data->text . '&lgid=' . $lgid, false );
		$linkHTML = WPage::createPopUpLink( $link, $image, '80%', 200, 'translation', '', $data->text );

		$content = '';
//		if ( !empty($look) ) $data->controller = $look;
		switch( $data->controller ) {
			case 'form':
//			case 'menu':
				$content = $linkHTML;
//debug( 7888445, $linkHTML );
				break;
			case 'form-layout':
				$content = '<div class="editElement translateFormLayout">' . $linkHTML . '</div>';
				break;
			case 'listing':
				$content = '<div class="editElement translateListing">' . $linkHTML . '</div>';
				break;
			case 'menu':
				$content = '<div class="editElement translateMenu">' . $linkHTML . '</div>';
				break;
			case 'view':
				$content = '<div class="editElement translateView">' . $linkHTML . '</div>';
				break;
			default:
				$content = '<div class="editElement">' . $linkHTML . '</div>';
				break;
		}//endswitch

		return $content;

	}//endfct



/**
	* <p>Create a little icon to edit a module</p>
	* @param int $moduleID
	* @return string HTML code
	*/
	private function _editModule( $data ) {

		//We don't do it if we are in a popup

		$image = '<i class="fa fa-edit fa-2x text-success"></i>';	// rel="tooltip" title="' . WText::translate( 'Module Preferences' ) . '"

		//com_modules only for modules
		$link = WPage::linkPopUp( 'controller=main-widgets-preference&task=edit&id=' . $data->moduleID . '&goty=com_modules' );

		$linkHTML = WPage::createPopUpLink( $link, $image, '80%', '80%', '', '', WText::translate( 'Edit Module Preferences') );

		$content = '<div class="editElement translateView">' . $linkHTML . '</div>';

		return $content;

	}//endfct


}//endclass