<?php
defined('JOOBI_SECURE') or die('J....');

/**
 *
 * this class is the object to create a button with all the properties possible to use
 */
class WRender_Badge_classObject {

	/*  Type of badge to render
	 *
	 * WPage::newBluePrint( 'badge' );
	 *
	 * -> default: a normal div
	 * -> vertical: a normal div
	 * -> horizontal: a normal div
	 * -> carrousel: a normal div
	 * -> slider: a normal div
	 * -> accordion-vertical: a normal div
	 * -> accordion-horizontal: a normal div
	 * -> spinner: a normal div
	 * -> scroller: a normal div
	 *
	 *
	 *  */
	public $type = 'default';

	/*
	 * The infromation about the items to render
	 */
	public $itemsA = array();

	/*
	 * The params to define how to render
	 */
	public $params = null;

	public $tagID = null;

	public $class = '';	// optional class name

	public $nodeName = '';	// the name of the node

	public $hasImage = false; 	// define that there is images to resize


}//endclass


/**
 * Tag_Parent_class class
 * Hold all the parent function to be used by each tag system
 */
class WRender_Badge_class extends Theme_Render_class {

	private static $countID = 0;
	private static $rowNb = 0;
	private static $columnNb = 0;

	public $nodeName = null;
	public $hasImage = null;

/**
	 * Create a Badge
	 * @param object-array $productA
	 * @param obejct $tagParams
	 * @return string HTML
	 */
	public function render( $data ) {

		$this->tagID = $data->tagID;

		$this->nodeName = $data->nodeName;
		$this->hasImage = $data->hasImage;

		if ( empty( $data->params->display) ) {
			$data->params->display = 'vertical';
			$data->type = 'vertical';
		}//endif

		$html = '';
		switch( $data->type ) {
			case 'carrousel':
				if ( empty($data->params->layout) || $data->params->layout=='default' ) $data->params->layout = 'badgebig';
				$html = $this->_displayCarrousel( $data->itemsA, $data->params, 'carrousel' );
				//we need to add this extra div for carrousle to work with all templates
				$html = '<div class="carrouselPanel">' . $html . '</div>';
				break;
			case 'slider':
				if ( empty($data->params->layout) || $data->params->layout=='default' ) $data->params->layout = 'badgemini';
				$html = $this->_displayCarrousel( $data->itemsA, $data->params, 'slider' );
				break;
			case 'accordion-vertical':	//vertical accordion
				if ( empty($data->params->layout) || $data->params->layout=='default' ) $data->params->layout = 'badgebig';
				$html = $this->_displayAccordionVertical( $data->itemsA, $data->params );
				break;
			case 'accordion-horizontal':	//vertical accordion
				if ( empty($data->params->layout) || $data->params->layout=='default' ) $data->params->layout = 'badgebig';
				$html = $this->_displayAccordionHorizontal( $data->itemsA, $data->params );
				break;

//			case 'spinner':
//				// make a spineer like this
//				// http://demo.hotjoomlatemplates.com/index.php?template=test
//				break;
//			case 'scroller':
//				if ( empty($data->params->layout) || $data->params->layout=='default' ) $data->params->layout = 'badgemini';
//				$html = $this->_displayScroller( $data->itemsA, $data->params );
//				break;
			case 'vertical':
			case 'horizontal':
			default:
//debug( 898989891, 'THIS IS FOR POPUP AND SHOULD BE CALLED SOMEWHERE ESLE' );
//				WPage::addJSLibrary( 'joobibox' );


				if ( empty($data->params->layout) || $data->params->layout=='default' ) $data->params->layout = 'badge';
				if ( empty($data->params->display) ) $data->params->display = 'vertical';

				switch( $data->params->display ) {
					case 'vertical':
//						$data->class = 'separatorVertical';
//						break;
					case 'horizontal':
//						$data->class = 'separatorHorizontal';
//						break;
					default:
						$data->class = 'separatorStandard';
						break;
				}//endswitch

				if ( !empty( $data->params->organizeTree ) ) {
					$html = $this->_renderSubCategoryBadge( $data->itemsA, $data->params, $data->class );
				} else {
					$html = $this->_renderStandardBadge( $data->itemsA, $data->params, $data->class );
				}//endif

				break;

		}//endswitch

		return $html;

	}//endfct


/**
 *
 * Render the Standard Badge layout ( all size )
 * @param array $productA
 * @param object $tagParams
 * @param string $className
 */
	private function _renderStandardBadge( $productA, $tagParams, $className='' ) {

		if ( !empty($tagParams->classSuffix) ) $className .= ' ' . $tagParams->classSuffix;

//		$html = '<div class="'. $className .'">';
		$html = '<div class="container-fluid"><div class="row">';


		$totalItems = count($productA);
		$count = 1;

		foreach( $productA as $key => $product ) {

			$product->nb = $count;

			if ( empty($tagParams->showNoImage) ) Output_Theme_class::setImageSize( $product, $tagParams, $this->tagID );	// $this->hasImage &&

			$html .= Output_Theme_class::callTheme( $product, $tagParams );

			if ( !empty($tagParams->layoutNbColumn) ) {
				if ( $count % $tagParams->layoutNbColumn == 0 && $totalItems != $count ) {
					$html .= '<div class="clearfix"></div>';
				}//endif
			}//endif

			$count++;
		}//endforeach

//		$html .= '<div class="clr"></div>';
		$html .= '</div></div>';
//debug( 788223, $html );
		return $html;

	}//endfct



/**
 *
 * Create the HTML for the slider
 * @param array of object $productsA
 * @param object $tagParams
 * @param string $typeDisplay type of display, carrousel or slider
 */
	private function _displayCarrousel( $productsA, $tagParams, $typeDisplay='carrousel' ) {
		static $alreadyLoaded = array();
		static $keyIndex=1;

//echo debug( 899211, $tagParams );

		//this file is necessary when we use the carrousel to fix problem with carrousel disapearing
		//no need to add it when T3 is since they already add that file
		if ( !defined( 'T3_TEMPLATE' ) ) WPage::addJSFile( 'js/extrascript.js' );

		if ( !empty($tagParams->layoutNbColumn) || !empty($tagParams->layoutNbRow) ) {
			//we first need to render the inside
			if ( empty($tagParams->layoutNbColumn) || $tagParams->layoutNbColumn < 1 ) $tagParams->layoutNbColumn = 1;
			if ( empty($tagParams->layoutNbRow) || $tagParams->layoutNbRow < 1 ) $tagParams->layoutNbRow = 1;
			$toalItems = $tagParams->layoutNbColumn * $tagParams->layoutNbRow;

			//create subArrayOfItems
			$slidesofItemsA = array();
			$count = 0;
			$index = 0;
			foreach( $productsA as $oneProduct ) {
				$count++;
				$slidesofItemsA[$index][] = $oneProduct;
				if ( $count >= $toalItems ) {
					$index++;
					$count = 0;
				}//endif
			}//endforeach

			//create each slides
			$HTMLSlidesA = array();
			foreach( $slidesofItemsA as $oneSlides ) {
				$HTMLSlidesA[] = $this->_renderStandardBadge( $oneSlides, $tagParams );
			}//endforeach
		}//endif


//		$key = $this->nodeName.'-37';	// defautl valuein case
		if ( !empty( $tagParams->idLabel ) ) {
			$key = $tagParams->idLabel;
			$keyIndex++;
		} else {
			$key = $this->nodeName . '-' . $keyIndex;
			$keyIndex++;
		}//endif

		$alreadyLoaded[$key] = true;	// crossfade:true;
		$typeDisplayAgain = ( $typeDisplay == 'carrousel' ) ? 'Carrousel' : 'Slider' ;

		$key .= $typeDisplayAgain;


		$carrouselSpeed = PCATALOG_NODE_CARROUSELSPEED;

		$html = '';
		$active = ' active';
		$indicatorsHTML = '';
		$indice = 0;

		if ( !empty($HTMLSlidesA) ) {
			$productsA = $HTMLSlidesA;
			$showSlidesB = true;
		} else {
			$showSlidesB = false;
		}//endif

		$count = 0;
		foreach( $productsA as $product ) {

			$html .= '<div class="item' . $active . '">';	//class="button"

			if ( $showSlidesB ) {
				$html .= $product;
			} else {
				$count++;
				$product->nb = $count;

				if ( $this->hasImage && empty($tagParams->showNoImage) ) Output_Theme_class::setImageSize( $product, $tagParams, $this->tagID );
				$html .= Output_Theme_class::callTheme( $product, $tagParams );

			}//endif
			$html .= '</div>';
//debug( 6777713, $key . $count );
			//creating the <!-- Indicators -->
			$indicatorsHTML .= '<li data-target="#' . $key . '" data-slide-to="' . (string)$indice . '"';
			if ( !empty($active) ) $indicatorsHTML .= ' class="active"';
			$indicatorsHTML .= '></li>';

			$indice++;
			$active = '';

		}//endforeach

		//creating control
		if ( empty($tagParams->showTitle) ) {

			$controlHTML = '<a class="left carousel-control" href="#' . $key . '" data-slide="prev">
<span class="fa fa-chevron-left"></span>
</a>
<a class="right carousel-control" href="#' . $key . '" data-slide="next">
<span class="fa fa-chevron-right"></span>
</a>';
		} else {
			$catalogCarrouselControlColor = $this->value( 'catalog.carrouselcontrolcolor' );
			$controlHTML = '<div class="controls pull-right hidden-xs">
<a class="fa fa-chevron-left btn btn-' . $catalogCarrouselControlColor . '" data-slide="prev" href="#' . $key . '"></a>
<a class="right fa fa-chevron-right btn btn-' . $catalogCarrouselControlColor . '" data-slide="next" href="#' . $key . '"></a>
</div>';
		}//endif

		$indicatorsHTML = '<ol class="carousel-indicators">' . $indicatorsHTML . '</ol>';

		$finalHTML = '<div class="carousel slide" data-ride="carousel" id="' . $key . '">';
//		$finalHTML .= $indicatorsHTML;
		$finalHTML .= '<div class="carousel-inner">';
		$finalHTML .= $html;
		$finalHTML .= '</div>';
		if ( empty($tagParams->showTitle) && !empty( $tagParams->carrouselArrow ) ) $finalHTML .= $controlHTML;
		$finalHTML .= '</div>';


		$data = WPage::newBluePrint( 'panel' );
		$data->id = $key . 'Panel';
		if ( !empty($tagParams->faicon) ) $data->faicon = $tagParams->faicon;
		if ( !empty($tagParams->color) ) $data->color = $tagParams->color;
		if ( !empty($tagParams->showTitle) ) $data->header = $tagParams->title;
		if ( !empty($tagParams->showTitle) ) $data->headerRightA[] = $controlHTML;

		$data->body = $finalHTML;

		return WPage::renderBluePrint( 'panel', $data );

	}//endfct



/**
 *
 * Create the HTML for the Accordion
 * @param array of object $productsA
 * @param object $tagParams
 * @param string $typeDisplay type of display, carrousel or slider
 */
	private function _displayAccordionVertical( $productsA, $tagParams ) {
//		static $loadedJS = false;

		if ( !empty($productsA) ) {

			if ( empty($tagParams->id) ) {
				if ( !empty($tagParams->idLabel) ) $tagParams->id = $tagParams->idLabel;
				else $tagParams->id = 'acordon' . time();
			}//endif


			//we use boostrap panel
			WLoadFile( 'blueprint.frame', JOOBI_DS_THEME_JOOBI );
			WLoadFile( 'blueprint.frame.sliders', JOOBI_DS_THEME_JOOBI );

			$tagParams->animate = true;
			$tagParams->delay = 2500;
//debug( 89993334, $tagParams );

			$frame = new WPane_sliders( $tagParams );
			$frame->startPane( $tagParams );

			$count = 0;
			foreach( $productsA as $product ) {

				$count++;
				$product->nb = $count;
				$frame->startPage( $tagParams );

				if ( $this->hasImage && empty($tagParams->showNoImage) ) Output_Theme_class::setImageSize( $product, $tagParams, $this->tagID );
				$html = Output_Theme_class::callTheme( $product, $tagParams );

				$frame->content = $html;

				$paramO = new stdClass;
				$paramO->text = $product->name;
				if ( empty($paramO->parent) ) $paramO->parent = $tagParams->id;

				$frame->endPage( $paramO );

			}//endforeach

			$html = $frame->endPane( $tagParams );

		} else {
			$html = '';
		}//endif
		if ( empty($tagParams->showTitle) ) {
			return $html;
		} else {
			$data = WPage::newBluePrint( 'panel' );
			$data->id = 'accordionVertical';
			if ( !empty($tagParams->faicon) ) $data->faicon = $tagParams->faicon;
			if ( !empty($tagParams->color) ) $data->color = $tagParams->color;
			if ( !empty($tagParams->showTitle) ) $data->header = $tagParams->title;

			$data->body = $html;

			return WPage::renderBluePrint( 'panel', $data );
		}//endif

	}//endfct


/**
 *
 * Create the HTML for the Accordion
 * @param array of object $productsA
 * @param object $tagParams
 * @param string $typeDisplay type of display, carrousel or slider
 */
	private function _displayAccordionHorizontal( $productsA, $tagParams ) {
		static $loadedJS=null;

		//this is when we have only 1 product for the Accordion
//		if ( count( $productsA ) < 2 ) {
//			$tagParams->display = 'normalSeparator';
//			return $this->_createBadge( $productsA, $tagParams );
//		}//endif

		if ( !$loadedJS ) {
			$browser = WPage::browser();
			if ( $browser->name == 'msie' ) {
				WPage::addCSSScript( 'div#itemAccordeonWrapper div.itemAccordeonTitleWrapper{text-indent:15px;left:17px;width:265px;height:36px!important;top:-8px!important;}');
			}//endif

			WPage::addJSLibrary( 'jquery' );

			WPage::addCSSFile( 'css/accordion-horizontal.css' );

			WPage::addJSFile( 'js/accordion-horizontal.js' );
			$loadedJS = true;
		}//endif

		$key = $this->nodeName;
		if ( isset($alreadyLoaded[$key]) ) {
			$key = $this->nodeName.'-'.$keyIndex;
			$keyIndex++;
		}//endif

		$height = 230;
		$width = 600;
		$subHeight = $height-67;
		$subWidth = $width-83;
		$numberPanes = count($productsA);
		$alreadyLoaded[$key] = true;	// crossfade:true;

//debug( 88894445, ' changer those JS variable with proper valu ethat is where I stopped check on the URL if something is not workin ');
		$JScode  = 'var $jq=jQuery.noConflict();';
		$JScode .= 'var itemAccordeonmodule_counter = "'.$numberPanes.'";';
		$JScode .= 'var itemAccordeonurl = "'. JOOBI_URL_THEME_JOOBI .'images/accordion/";';
		$JScode .= 'var itemAccordeonspeed = "900";';
		$JScode .= 'var itemAccordeontransition = "3500";';
		$JScode .= 'var itemAccordeoncycle = "yes";';
		$JScode .= 'var itemAccordeondef_slide = "1";';

		$JScode .= '$jq(function(){
var itemWidth = jQuery(\'#itemAccordeonWrapperWidth\').width();
var itemSlideWidth = itemWidth - ( ( itemAccordeondef_slide - 1) * 40);
var itemSusWidth = itemWidth - 141;
var num= itemAccordeondef_slide;
if (num == "1"){
itemAccordeonopen(1);
} else {
if (document.getElementById("itemAccordeon"+num)) {
eval("itemAccordeonopen("+num+");");
}
}
$jq("div.accordeonMain").addClass("accordeonMain-js");
$jq("div#itemAccordeonWrapper").addClass("itemAccordeonWrapper-js");
$jq("div#itemAccordeonWrapper").css("width", itemWidth+\'px\');
$jq("div.accordeonMain").css("width", itemSlideWidth+\'px\');
$jq("div.itemAccordeonContent").css("width", itemSusWidth+\'px\');
window.setTimeout("itemAccordeonrotate_slides()",itemAccordeontransition);
});';

		WPage::addJSFile( 'main/js/jquery_easing.js', 'inc' );
		WPage::addJSScript( $JScode, 'default', false );

		$height = $height . 'px';
		$width = $width . 'px';

		$html = '<div style="display:none">';
		foreach( $productsA as $product ) $html .= '<img alt="icon" src="' . JOOBI_URL_THEME_JOOBI .'images/accordion/slide.png">';
		$html .= '</div>';

		$extraCSS = ( empty($tagParams->showTitle) ? 'margin-bottom:20px;' : '' );
		$html .= '<div id="itemAccordeonWrapperWidth" style="width:100%;"></div><div id="itemAccordeonWrapper" onclick="itemAccordeondisable()" onblur="itemAccordeonenable()" style="border-top:none; border-left:none; border-bottom:none; border-right:none; padding:0px; margin:0px; height:'.$height.'; width:'.$width.';' . $extraCSS . '">';

		$slideWidth = $width - ( ( $numberPanes - 1) * 40);

		$i=0;
		foreach( $productsA as $product ) {

			$i++;
			$product->nb = $i;

			$html .= '<div id="itemAccordeon'.$i.'" onclick="itemAccordeonopen('.$i.')" class="accordeonMain" style="padding:0px; margin:0px; height:'. $height .'px;">';
			$html .= '<div id="accordeonLinkWrap'.$i.'" style="width:40px; height:'.$height.';">';
			$html .= '<div style="margin:0px; height:'.$subHeight.'px;" class="itemAccordeonBody">';
			$html .= '<div style="margin:0px" class="itemAccordeonTitleWrapper">';
			$html .= str_replace( ' ', '&nbsp;', $product->name );
			$html .= '</div></div>';
//			$html .= '<div class="itemAccordeonslide_number" style="margin:0px">';
//			$html .= $i;
//			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="itemAccordeonContent" style="margin:0px; width:'.$subWidth.'px;">';

			if ( $this->hasImage && empty($tagParams->showNoImage) ) Output_Theme_class::setImageSize( $product, $tagParams, $this->tagID );
			$html .= Output_Theme_class::callTheme( $product, $tagParams );
			$html .= '</div></div>';

		}//endforeach

		$html .= '</div>';


		if ( empty($tagParams->showTitle) ) {
			return $html;
		} else {
			$data = WPage::newBluePrint( 'panel' );
			$data->id = 'accordionHorizontal';
			if ( !empty($tagParams->faicon) ) $data->faicon = $tagParams->faicon;
			if ( !empty($tagParams->color) ) $data->color = $tagParams->color;
			if ( !empty($tagParams->showTitle) ) $data->header = $tagParams->title;

			$data->body = $html;

			return WPage::renderBluePrint( 'panel', $data );
		}//endif

	}//endfct



/**
 *
 * Render the Badge layout ( all size ) for sub-category and tree like layout UL - LI
 * @param array $productA
 * @param object $tagParams
 * @param string $className
 */
	private function _renderSubCategoryBadge( $productA, $tagParams, $className ) {

		if ( empty($tagParams->subCatPopOver) ) {
			$useDIV = true;	// div
		} else {
			if ( $tagParams->subCatPopOver == 7 ) {	// ul li
				$useDIV = false;
			} else {
				$useDIV = true;	// div
			}//endif
		}//endif


		//record the main parent
		$mainParent = key($productA);
		$initialLevel = $productA[$mainParent][0]->depth;

		$firstLevelA = $productA[$mainParent];
		unset( $productA[$mainParent] );

		//we need to reverse the array to start from the end which are the lowest child
		$reversedA = array_reverse( $productA, true );

		$childHMTLA = array();
		$tagParamsChild = clone $tagParams;
		$tagParamsChild->containerClass = 'categoryChild';
		$tagParamsChild->borderShow = false;
		$tagParamsChild->borderColor = 'none';
		$nb = 0;
		foreach( $reversedA as $manySubProduct ) {

			$htmlSubCategory = ( $useDIV ? '<div>' : '<ul class="subCat">' );		//class="' . $tagParams->containerClass . '"

			//this is for the child
			foreach( $manySubProduct as $oneProduct ) {

				//the relative depth
				$oneProduct->level = $oneProduct->depth - $initialLevel;
				$nb++;
				$oneProduct->nb = $nb;

				if ( empty($tagParams->showNoImage) ) Output_Theme_class::setImageSize( $oneProduct, $tagParamsChild, $this->tagID, $oneProduct->depth - $initialLevel + 1 );

				if ( !$useDIV ) $htmlSubCategory .= '<li>';

				$htmlSubCategory .= Output_Theme_class::callTheme( $oneProduct, $tagParamsChild );

				//we have a child we need to had it
				if ( isset($childHMTLA[$oneProduct->catid]) ) {
					$htmlSubCategory .= $childHMTLA[$oneProduct->catid];
				}//endif

				if ( !$useDIV ) $htmlSubCategory .= '</li>';

			}//endforeach

			$htmlSubCategory .= ( $useDIV ? '</div>' : '</ul>' );
			$childHMTLA[$oneProduct->parent] = $htmlSubCategory;	//</div></div>
		}//endforeach

		if ( !empty($tagParams->classSuffix) ) $className .= $tagParams->classSuffix;
		$extraClass = ( !empty($tagParams->display) &&  'menu' == $tagParams->display ) ? ' menu' : '';

		$html = '<div class="container-fluid"><div class="row">';
		$totalItems = count($initialLevel);
		$count = 1;
		$styleApplied = ( !empty($tagParams->containerStyle) ? $tagParams->containerStyle : '' );	// parentMenu
		$styleApplied = '';

		unset( $tagParams->containerStyle );
		foreach( $firstLevelA as $key => $product ) {

			if ( empty($tagParams->showNoImage) ) Output_Theme_class::setImageSize( $product, $tagParams, $this->tagID );	// $this->hasImage &&

			//we have a child we need to had it
			if ( isset($childHMTLA[$product->catid]) ) {
				$htmlChild = $childHMTLA[$product->catid];
			} else {
				$htmlChild = '';
			}//endif

			$styleAppliedClass = ( !empty($htmlChild) ) ? $styleApplied.' class="parentMenu"' : $styleApplied;

			//we put the child here so we can use it int eh badge to do an popover for instance
			if ( !empty($htmlChild) ) {

				if ( !empty( $tagParams->subCatPopOver ) && $tagParams->subCatPopOver == 1 ) {

					$product->htmlChild = $htmlChild;
					$product->htmlID = 'item_' . $product->namekey;
					$subCatId = $product->htmlID . '_sub';
					$htmlChild = '';

					//add some js to make the popover
					$js = "jQuery(function() {
var moveLeft = 20;
var moveDown = 10;
jQuery('div#" . $product->htmlID . "').hover(function(e) {
jQuery('div#" . $subCatId . "').show();
}, function() {
 jQuery('div#" . $subCatId . "').hide();
});
jQuery('div#" . $product->htmlID . "').mousemove(function(e) {
jQuery('div#" . $subCatId . "').css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
});
});
";

					WPage::addJSLibrary( 'jquery' );
					WPage::addJSScript( $js );

				} else {
					$product->htmlID = '';	//no need ID for child for the moment
				}//endif

			} else {
				$product->htmlID = '';	//no need ID for child for the moment
			}//endif

			if ( !empty( $tagParams->subCatPopOver ) ) {
				if ( $tagParams->subCatPopOver == 1 ) {
					$product->subCategoryStyle = 'popOver';
				} elseif ( $tagParams->subCatPopOver == 7 ) {
					$product->subCategoryStyle = 'ul-li';
				}//endif
			}//endif

			if ( !isset($product->subCategoryStyle) ) $product->subCategoryStyle = '';
			if ( empty($product->htmlChild) && !empty($htmlChild) ) $product->htmlChild = $htmlChild;

			$parentCat = Output_Theme_class::callTheme( $product, $tagParams );
			//put the child inside the parent
			if ( substr( $parentCat, -17 ) == "</div>\r\n\t</div>\r\n" ) {
				$html .= substr( $parentCat, 0, ( strlen($parentCat) -17 ) ) . $htmlChild . "</div></div>";
			} elseif ( substr( $parentCat, -17 ) == "</div>\r\n\t</div>\r\n" || substr( $parentCat, -16 ) == "</div>\r\n</div>\r\n" || substr( $parentCat, -12 ) == "</div></div>" ) {
				$html .= substr( $parentCat, 0, ( strlen($parentCat) -16 ) ) . $htmlChild . "</div></div>";
			} else {
				$html .= $parentCat . $htmlChild;
			}//endif

			$count++;

		}//endforeach

		$html .= '</div><div class="clearfix"></div>';	// <div class="clearfix"></div>
		$html .= '</div>';

		return $html;

	}//endfct


}//endclass
