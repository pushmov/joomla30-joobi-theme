<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/


/**
 *
 * this class is the object to create a button with all the properties possible to use
 */
class WRender_Progressbar_classObject {

 	public $targetTotal = 0; 	  	//this serves as the percentage of the progress bar
 	public $target = 0;		//this will be the target value of the progress bar

 	public $labelStyle = ''; 		//the style of the label
 	public $labelMsg = '';      	//the text that will appear in the progress bar
 	public $completeStyle = ''; 	//the style of the complete message in case you need to have different style
 	public $completeMsg = '';      //the text that will appear once the progress bar is complete
 	public $zeroStyle = ''; 		//the style of the not yet started message in case you need to have different style
 	public $zeroMsg = '';     		//the text that will appear once the progress bar is not yet started
 	public $percentage = 0;        //this will be the percentage of the progress bar
 	public $height = '';     		//this will be the height of the progress bar, please provide a unit either px or %
	public $width = ''; 			//this will be the width of the progress bar, please provide a unit either px or %

	public $color = 'success';
	public $striped = true;
	public $animated = true;


	public $showStatus = true;
	public $statusText = 'Status';
	public $statusValue = 'Ready';

	public $showError = false;
	public $errorValue = '';
	public $warningValue = '';

	public $showDuration = false;
	public $durationText = 'Estimated duration';
	public $durationValue = 0;

	public $completedText = 'Completed';

	public $showTime = false;
	public $timeText = 'Estimated completion time';
	public $timeValue = 0;

	public $showDetails = false;
	public $detailsText = 'Details';
	public $detailsValue = 0;


}//endclass


class WRender_Progressbar_class extends Theme_Render_class {

/**
 * This function will create the Progress Bar
 * @return $progressbarHTML string 	this will be the html of the progress bar
 * author Dev
 */
	public function render( $data ) {

		$renderType = ( !empty( $data->type ) ? $data->type : 'bar' );

		switch( $renderType ) {
			case 'error':
				return $this->_renderError( $data );
				break;
			case 'text':
				return  $this->_renderEstimate( $data );
				break;
			case 'bar':
			default:
				return $this->_renderBar( $data );
				break;
		}//endswitch

	}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $data
 */
	private function _renderBar( $data ) {

		if ( empty($data->percentage) ) {

			if ( !empty($data->targetTotal) ) {
				if ( ! is_numeric($data->targetTotal) || ! is_numeric($data->target) ) return '';
				$data->percentage = ( $data->target / $data->targetTotal ) * 100;
			}//endif

		}//endif

		$data->percentage = round( $data->percentage, 1 );

		if ( empty($data->labelMsg) ) $data->labelMsg = $data->percentage . '%';
		$style = ( !empty($data->labelStyle) ? ' style="' . $data->labelStyle . '"' : '' );
		$messageHTML = '<span' . $style . '>' . $data->labelMsg . '</span>';

		$extra = ' progress-bar-' . $data->color;
		if ( $data->striped ) $extra .= ' progress-bar-striped';
		if ( $data->animated ) $extra .= ' active';

		$progressbarHTML = '<div id="ProgressBar" class="progress">';
		$progressbarHTML .= '<div class="progress-bar ' . $extra .'" role="progressbar" aria-valuenow="' . $data->percentage . '" aria-valuemin="0" aria-valuemax="100" style="min-width: 3em; width: ' . $data->percentage . '%">';
		$progressbarHTML .= $messageHTML;
		$progressbarHTML .= '<span class="sr-only">' . $data->percentage . '% ' . WText::translate( 'Complete' ) . ' (' . $data->color . ')</span>';
		$progressbarHTML .= '</div></div>';

		return $progressbarHTML;

	}//endfct


/**
 *
 * Enter description here ...
 * @param unknown_type $data
 */
	private function _renderError( $data ) {

		$html = '';
 		//here we show the error message
		if ( $data->showError ) {	// $errorValue
			$html .= '<div id="WAjxPaneWarning" style="display:none;" class="alert alert-warning" role="alert">' . $data->warningValue . '</div>';
			$html .= '<div id="WAjxPaneError" style="display:none;" class="alert alert-danger" role="alert">' . $data->errorValue . '</div>';
		}//endif

		return $html;

	}//endfct

/**
 *
 * Enter description here ...
 * @param unknown_type $data
 */
	private function _renderEstimate( $data ) {


		$html = '<div id="WAjxText" class="row">';

		if ( $data->showStatus ) {
			$html .= '<div class="form-group">';
			$html .= '<label class="col-sm-3 control-label">' . $data->statusText . ': </label>';
			$html .= '<div class="col-sm-9">' . $data->statusValue . '</div>';
			$html .= '</div>';
		}//endif


		if ( $data->showDuration ) {
			$html .= '<div class="form-group">';
			$html .= '<label id="WAjxDurationText" class="col-sm-3 control-label">' . $data->durationText . ': </label>';
			if ( 100 == $data->percentage ) {
				$html .= '<div class="col-sm-9">' . $data->durationText . '</div>';
			} else {
				$html .= '<div class="col-sm-9">' . $data->durationValue . '</div>';
			}//endif
			$html .= '</div>';
		}//endif

		if ( $data->showTime ) {
			$html .= '<div class="form-group">';
			$html .= '<label class="col-sm-3 control-label">' . $data->timeText . ': </label>';
			$html .= '<div class="col-sm-9">' . $data->timeValue . '</div>';
			$html .= '</div>';
		}//endif

		if ( $data->showDetails ) {
			$html .= '<div class="form-group">';
			$html .= '<label class="col-sm-3 control-label">' . $data->detailsText . ': </label>';
			$html .= '<div class="col-sm-9">' . $data->detailsValue . '</div>';
			$html .= '</div>';
		}//endif

		$html .= '</div>';

		return $html;

	}//endfct


}//endclass