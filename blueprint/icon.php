<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/


/**
 *
 * this class is the object to create a button with all the properties possible to use
 */
class WRender_Icon_classObject {

	/*
	 * WPage::newBluePrint( 'icon' );
	 *
	 */
	public $location = '';

	public $icon = '';	// the name of the icon / image

	public $text = '';	// a text to put into the title of the img or tooltips

	public $size = '';	//define the size of an icon

	public $color = '';	//define the color of an icon

	public $animation = '';	// the anmication fro the icon


}//endclass


class WRender_Icon_class extends Theme_Render_class {

	private static $_columniconcolor = null;
	private static $_starcolor = null;
	private static $_starsize = null;

	private static $_count = 0;

	private $_data = null;


/**
 *
 *	WPage::renderBluePrint( 'icon', $iconO );
 *
 * This function is to render an icon
 * @param object $this->_data
 */
  	public function render( $data ) {

  		if ( empty($data) ) return '';

  		$this->_data = $data;

  		if ( !isset( self::$_columniconcolor ) ) {
  			self::$_columniconcolor = $this->value( 'table.columniconcolor' );
  			self::$_starcolor = $this->value( 'catalog.starcolor' );
  			self::$_starsize = $this->value( 'catalog.starsize' );
  		}//endif


  		if ( is_string($this->_data) ) {
  			return $this->_getIcon( $this->_data );
  		}//endif


  		if ( empty($this->_data->icon) ) {
  			$this->codeE( 'Icon not specified!' );
  			return '';
  		}//endif

  		//	rel="tooltip" title="Key active" id="blah"
  		$html = '<i class="fa ' . $this->_getIcon( $this->_data->icon );

  		if ( !empty($data->animation) ) {
  			$html .= ' ' . $data->animation;
  		}//endif

  		if ( !empty($this->_data->size) ) {
  			switch( $this->_data->size ) {
  				case 'medium':
  					break;
  				case 'large':
  					$html .= ' fa-lg';
  					break;
  				case 'xlarge':
  					$html .= ' fa-2x';
  					break;
  				case 'xxlarge':
  				case '2xlarge':
  					$html .= ' fa-3x';
  					break;
  				case '3xlarge':
  					$html .= ' fa-4x';
  					break;
  				case '4xlarge':
  					$html .= ' fa-5x';
  					break;
  				default:
  					break;
  			}//endswitch
  		}//endif
  		$html .= '"';

  		if ( !empty($this->_data->text) ) {
  			$html .= ' rel="tooltip" title="' . $this->_data->text . '"';
  			if ( empty( $this->_data->id ) ) {
  				self::$_count++;
  				$this->_data->id = 'gmi' . self::$_count;
  			}//endif
  		}//endif

  		if ( !empty( $this->_data->id ) ) {
  			$html .= ' id="' . $this->_data->id . '"';
  		}//endif

  		$html .= '></i>';

		return $html;

  	}//endfct



/**
 *
 * Enter description here ...
 * @param unknown_type $image
 */
	private function _getIcon( $icon ) {

		switch( $icon ) {
			case 'publish':
				$html = 'fa-power-off';
//				$html = 'fa-check-circle';
				if ( self::$_columniconcolor ) $html .= ' text-success';
				break;
			case 'unpublish':
				$html = 'fa-times-circle';
				if ( self::$_columniconcolor ) $html .= ' text-danger';
				break;
			case 'yes':
				$html = 'fa-check ';
				if ( self::$_columniconcolor ) $html .= ' text-success';
				break;
			case 'no':
				$html = 'fa-times';
				if ( self::$_columniconcolor ) $html .= ' text-success';
				break;
			case 'edit':
				$html = 'fa-pencil-square-o';
				if ( self::$_columniconcolor ) $html .= ' text-primary';
				break;
			case 'show':
				$html = 'fa-eye';
				if ( self::$_columniconcolor ) $html .= ' text-info';
				break;
			case 'delete':
				$html = 'fa-trash-o';
				if ( self::$_columniconcolor ) $html .= ' text-danger';
				break;
			case 'copy':
				$html = 'fa-files-o';
				if ( self::$_columniconcolor ) $html .= ' text-info';
				break;
			case 'lock':
				$html = 'fa-lock';
				if ( self::$_columniconcolor ) $html .= ' text-success';
				break;
			case 'unlock':
				$html = 'fa-unlock';
				if ( self::$_columniconcolor ) $html .= ' text-warning';
				break;
			case 'enabled':
				$html = 'fa-unlock';
				if ( self::$_columniconcolor ) $html .= ' text-success';
				break;
			case 'disabled':
				$html = 'fa-lock';
				if ( self::$_columniconcolor ) $html .= ' text-danger';
				break;
			case 'pending':
				$html = 'fa-unlock';
				if ( self::$_columniconcolor ) $html .= ' text-warning';
				break;
			case 'archive':
				$html = 'fa-archive';
				if ( self::$_columniconcolor ) $html .= ' text-warning';
				break;
			case 'unarchive':
				$html = 'fa-folder-open';
				if ( self::$_columniconcolor ) $html .= ' text-info';
				break;
			case 'disabled':
			case 'cancel':
				$html = 'fa-times';	// -circle-o
				if ( self::$_columniconcolor ) $html .= ' text-danger';
				break;
			case 'refresh':
				$html = 'fa-refresh';
				if ( self::$_columniconcolor ) $html .= ' text-primary';
				break;
			case 'preferences':
				$html = 'fa-cog';
				if ( self::$_columniconcolor ) $html .= ' text-primary';
				break;
			case 'fleche':
				$html = 'fa-chevron-right';
				if ( self::$_columniconcolor ) $html .= ' text-primary';
				break;
			case 'down':
				$html = 'fa-arrow-down';
				if ( self::$_columniconcolor ) $html .= ' text-warning';
				break;
			case 'loading':
				$html = 'fa-spinner fa-spin';
				if ( self::$_columniconcolor ) $html .= ' text-primary';
				break;
			case 'attachment':
				$html = 'fa-paperclip';
				if ( self::$_columniconcolor ) $html .= ' text-primary';
				break;
			case 'profile':
				$html = 'fa-user';
				if ( self::$_columniconcolor ) $html .= ' text-danger';
				break;
			case 'lock':
				$html = 'fa-lock';
				if ( self::$_columniconcolor ) $html .= ' text-success';
				break;
			case 'unlock':
				$html = 'fa-unlock';
				if ( self::$_columniconcolor ) $html .= ' text-danger';
				break;
			case 'preview':
				$html = 'fa-eye';
				if ( self::$_columniconcolor ) $html .= ' text-info';
				break;
			case 'followup':
				$html = 'fa-exclamation-circle';
				if ( self::$_columniconcolor ) $html .= ' text-warning';
				break;
			case 'sendmessage':
				$html = 'fa-envelope';
				if ( self::$_columniconcolor ) $html .= ' text-success';
				break;
			case 'dontsendmessage':
				$html = 'fa-times-circle';
				if ( self::$_columniconcolor ) $html .= ' text-danger';
				break;
			case 'push-pin':
				$html = 'fa-thumb-tack';
				if ( self::$_columniconcolor ) $html .= ' text-warning';
				break;
			case 'star-blank':
				$html = 'fa-star-o';
				if ( 'primary' == self::$_starcolor ) $html .= ' text-info';
				else $html .= ' text-' . self::$_starcolor;
				$this->_data->size = self::$_starsize;
				break;
			case 'star-half':
				$html = 'fa-star-half-o';
				$html .= ' text-' . self::$_starcolor;
  				$this->_data->size = self::$_starsize;
				break;
			case 'star':
				$html = 'fa-star';
				$html .= ' text-' . self::$_starcolor;
				$this->_data->size = self::$_starsize;
				break;

			default:
				$html = $icon;
				if ( !empty($this->_data->color) ) {
					$html .= ' text-' . $this->_data->color;
				} else {
					if ( self::$_columniconcolor ) $html .= ' text-primary';
				}//endif

//WMessage::log( 'Icon not defined:' . $image, 'error-legend-icon' );
////__START__
//				$this->codeE( 'Icon not defined:' . $image, 'error-legend-icon' );
////__END__
				break;

		}//endfct

		return $html;

	}//endfct

}//endclass