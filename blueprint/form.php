<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Form_class extends Theme_Render_class {

	public static $formDirection = null;
	public static $formTipStyle = null;

/**
 *
 * Enter description here ...
 */
	public function render() {
		if ( !isset(self::$formDirection) ) {
			self::$formDirection = $this->value( 'form.direction' );
			self::$formTipStyle = $this->value( 'form.tooltip' );

		}//endif
		return self::$formDirection;
	}//endfct


}//endclass


class WForms_default extends WForms_standard {


/**
 *
 * Enter description here ...
 */
	function preCreate() {

		if ( 'form-horizontal' == WView::$directionForm[$this->yid] ) {
			if ( isset( $this->element->classes ) ) {
				$this->element->classes = 'form-control ' . $this->element->classes;
			} else {
				if ( empty( $this->element->spantit ) ) {
					$this->element->classes = 'form-control';
//				} else {

				}//endif

			}//endif
		}//endif

		return true;

	}//endfct


/**
 *
 * Enter description here ...
 */
	function preShow() {

		if ( 'form-horizontal' == WView::$directionForm[$this->yid] ) {
			if ( isset( $this->element->classes ) ) {
				$this->element->classes = $this->element->classes;
			} else {
				$this->element->classes = '';
			}//endif
		}//endif

		return true;

	}//endfct


/**
 * Add content wrapper to the field for the form element, for instance to make rounded corners
 */
	protected function wrapperCreate() {

		if ( 'form-horizontal' == WView::$directionForm[$this->yid] ) {

//debug( 56653242, 'need to define the wrap for all eelment' );
		switch( $this->element->type ) {

			case 'output.select':
				$this->content = '<div class="col-sm-10 pickList">' . $this->content . '</div>';
				break;

			case 'main.trans':
			case 'main.transarea':
//				$this->content = '<div class="col-sm-10 input-group">' . $this->content . '</div>';
				$this->content = '<div class="col-sm-10"><div class="input-append">' . $this->content . '</div></div>';
				break;

//			case 'output.publish':
//			case 'output.yesno':
//			case 'output.layout':
//			case 'output.textonly':
//			case 'output.checkbox':
//			case 'main.captcha':
//			case 'main.colorpicker':
//			case 'main.phpedit':
//			case 'main.rating':
//			case 'main.showhidelink':
//			case 'main.map':
//				$this->content = '<div class="col-sm-10">' . $this->content . '</div>';
//				break;
//
//			case 'output.select':
////				//use the style and the type, most of picklist style is define in the picklist not int he element , only for custom fields
////				if ( $this->picklistTypeSingle && in_array( $this->pickListStyle, array( 2,6 ) ) ) {
////					// 2, radio button, 3, checkbox
////					if ( !in_array( $this->pickListStyle, array( 2,3,4,5,8 ) ) ) $this->content = '<span class="fieldCurveL"><span class="fieldCurveR"><span class="fieldCurveC">' . $this->content . '</span></span></span>';
////				} else {
////					$className = (  $this->formTypeShow ? 'show' : 'edit' );
////					$html = '<div class="multiSelectBox '.$className.'">' . $this->content . '<div>';
////					$this->content = '<div class="fieldBoxTL"><div class="fieldBoxTR"><div class="fieldBoxTC"></div></div></div><div class="fieldBoxML"><div class="fieldBoxMR"><div class="fieldBoxMC">' . $html . '</div></div></div><div class="fieldBoxBL"><div class="fieldBoxBR"><div class="fieldBoxBC"></div></div></div>';
////				}//endif
//				break;
//
//			case 'output.textarea':
////				if ( empty($this->element->editor) || $this->element->editor == 'textarea' || $this->element->editor == 'default' ) {
////					$this->content = '<div class="fieldBoxTL"><div class="fieldBoxTR"><div class="fieldBoxTC"></div></div></div><div class="fieldBoxML"><div class="fieldBoxMR"><div class="fieldBoxMC">' . $this->content . '</div></div></div><div class="fieldBoxBL"><div class="fieldBoxBR"><div class="fieldBoxBC"></div></div></div>';
////				}//endif
//				break;
//
//			case 'output.multiselect':
////				$this->content = '<div class="fieldBoxTL"><div class="fieldBoxTR"><div class="fieldBoxTC"></div></div></div><div class="fieldBoxML"><div class="fieldBoxMR"><div class="fieldBoxMC">' . $this->content . '</div></div></div><div class="fieldBoxBL"><div class="fieldBoxBR"><div class="fieldBoxBC"></div></div></div>';
//				break;
//
//			case 'output.media':
//				$this->content = '<div class="mediaWrap">' . $this->content . '</div>';
//				break;
//
//			case 'main.submit':
//				$objButtonO = WPage::newBluePrint( 'button' );
//				$objButtonO->text = $this->content;
//				$objButtonO->type = 'standard';
//				if ( !empty($this->element->classes) ) $objButtonO->wrapperDiv = $this->element->classes;
//				$this->content = WPage::renderBluePrint( 'button', $objButtonO );
//
//				break;
//
//			case 'output.media':
//				$this->content = '<div class="mediaWrap">' . $this->content . '</div>';
//				break;

			default:
				if ( empty( $this->element->spantit ) ) {
					$this->content = '<div class="col-sm-10">' . $this->content . '</div>';
				} else {
					$this->content = '<div>' . $this->content . '</div>';
				}//endif

				break;

		}//endswitch


		}//endif

	}//endfct


/**
 *
 * Add content wrapper to the field for the show
 */
	protected function wrapperShow() {

		switch( $this->element->type ) {

			default:
				if ( empty( $this->element->spantit ) ) {
					$this->content = '<div class="col-sm-10">' . $this->content . '</div>';
				} else {
					$this->content = '<div>' . $this->content . '</div>';
				}//endif

				break;

		}//ednswitch


//debug( 6611, 'DO WE NEED A WRAPPER HERE?' );

	}//endfct


}//endclass