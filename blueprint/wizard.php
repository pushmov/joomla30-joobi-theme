<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Wizard_class extends Theme_Render_class {

/**
 *
 * Enter description here ...
 * @param unknown_type $wizardO
 */
  	public function render( $wizardO ) {

  		if ( !IS_ADMIN && !WPref::load( 'PLIBRARY_NODE_WIZARDFE' ) ) return '';
  		if ( IS_ADMIN && !WPref::load( 'PLIBRARY_NODE_WIZARD' )) return '';

		if ( !empty($wizardO->wname) ) {

			$wizard_color = $this->value( 'wizard.color' );

			$description = nl2br( $wizardO->wdescription );

			//Load the tag system to convert the wizard description...
			//jtag
			if ( strpos( $description, '{widget:' ) ) {
				$tag = WClass::get('output.process');
				$tag->replaceTags( $description, WUser::get('data') );
			}//endif

			$html = '<fieldset>';
			$html .= '';
			$html .= '<legend><i class="fa fa-magic';
			if ( $wizard_color ) $html .= ' text-warning';
			$html .= '"></i>' . $wizardO->wname . '</legend>';
			$html .= $description;
			$html .= '</fieldset>';


			if ( !empty( $wizardO->showWizard ) ) $style = 'style="display:block;"';
			else $style = 'style="display:none;"';

			return '<div id="viewWizard" class="clearfix" '.$style.'>' . $html . '</div>';

		}//endif

		return '';

  	}//endfct

}//endclass