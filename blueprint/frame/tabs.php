<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Class to create a tab like pane.</p>
* @author Joobi Team
*/
class WPane_tabs extends WPane {

	public $fade = true; 	// use fade on tab

	private $_navTabHTMLA = array();

	private $_tabPaneHTMLA = array();

	private static $_paneIcon = null;

	private $_params = null;

/** <p>Start the pane</p>
	 * @param array $params parameters
*/
	public function startPane( $params ) {

		$this->_params = $params;
		if ( !isset( self::$_paneIcon ) ) {
		  	self::$_paneIcon = WPage::renderBluePrint( 'initialize', 'pane.icon' );
		}//endif

		$this->_navTabHTMLA = array();
		$this->_tabPaneHTMLA = array();

	}//endfct


/** <p>End the pane</p>
	*/
	public function endPane() {

		static $count = 0;
		$count++;
		$id = ( !empty( $this->_params->idText ) ? $this->_params->idText : 'EnapBat' . $count );

		$this->content = '<ul id="' . $id . '" class="nav nav-tabs">';
		$this->content .= implode( '', $this->_navTabHTMLA );
		$this->content .= '</ul>';

		$this->content .= '<div class="tab-content">';
		$this->content .= implode( '', $this->_tabPaneHTMLA );
		$this->content .= '</div>' . $this->crlf;

		return $this->content;

	}//endfct


/** <p>Start one tab</p>
 * @param array $params parameters
	*/
	public function startPage( $params ) {
		$this->content = '';
	}//endfct

/** <p>End one tab</p>
	*/
	public function endPage( $params ) {

		if ( empty($this->content) ) {
			return '';
		}//endif
//debug( 566611, $params );

		static $active = true;

                //check if we use cookie
                //this is trick because this function calls twice
//                static $_addedJsAlrady = 0;
               // if ( isset($this->useCookies) ) {
//                if ( $_addedJsAlrady === 0 ) {
                    //this is trick because this function calls twice
//                    $_addedJsAlrady++;
                    $js  = '';
                    //here i need to check what tab was opened and make it active
                    $js .= 'window.WApps.helpers.makeTabActive("' . $params->idText . '");' . WGet::$rLine;
                    //                    $js .= 'setTimeout(window.WApps.helpers.makeTabActive(), 0);';
                    //save new opened tab to cookie
                    //                    $js .= 'setTimeout(window.WApps.helpers.setToCookieActiveTab(),0);';
                    $js .= 'window.WApps.helpers.setToCookieActiveTab("' . $params->idText . '");' . WGet::$rLine;
                    WPage::addJSScript( $js,'default', false );

//                }//endif
//debug( 50033112, 'Define the active tab???' );

		if ( $active ) {
			$activeClassNav = ' class="active"';
			$activeClassPane = ' active';
			if ( $this->fade ) $activeClassPane = ' fade in' . $activeClassPane;
		} else {
			$activeClassNav = '';
			if ( $this->fade ) $activeClassPane = ' fade';
			else $activeClassPane = '';
		}//endif

		if ( $active ) $active = false;

		$navTabHTML = '<li' . $activeClassNav . '><a href="#' . $params->id . '" data-toggle="tab">';
		if ( self::$_paneIcon && !empty($params->faicon) ) $navTabHTML .= '<i class="fa ' . $params->faicon . '"></i>';
//		$navTabHTML .= $params->text;
		$navTabHTML .= '<h3 class="panel-title">' . $params->text . '</h3>';	// for screen reader
		$navTabHTML .= '</a></li>';

		$tabPaneHTML = '<div class="tab-pane' . $activeClassPane . '" id="' . $params->id . '">' . $this->content . '</div>';

		//reset the content now that we have taken it into the tab
		$this->content = '';

		$this->_navTabHTMLA[] = $navTabHTML;
		$this->_tabPaneHTMLA[] = $tabPaneHTML;

		return '';
	}//endfct


}//endclass
