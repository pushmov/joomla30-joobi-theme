<?php
defined('JOOBI_SECURE') or die('J....');


/**
* <p>Class to create a UL LI list</p>
* @author Joobi Team
*/
class WPane_list extends WPane {

	private $htmlContent = ''; //enetire content
	private $htmlCell = ''; //just one cell

	private $_params = null;

/**
	* Constructor
 * @param array $params parameters
	*/
	function __construct( $params = array() ) {
		parent::__construct($params);
		$this->startPane( $params );
	}//endfct


/** <p>Start the pane</p>
* @param array $params parameters
*/
	public function startPane( $params ) {
		$this->htmlContent = '';
	}//endfct


/** <p>End the pane</p>
	*/
	public function endPane() {
		$this->htmlContent .= '';
	}//endfct


/** <p>Start one tab</p>
 * @param array $params parameters
	*/
	public function startPage() {

		if ( !empty($this->_params->spantit) ) {
			$this->htmlContent .= '<div>';
		} else {
			$this->htmlContent .= '<div class="form-group">';
		}//endif

	}//endfct


/** <p>End one tab</p>
	*/
	private function endPage() {
		$this->htmlContent .= '</div>';
	}//endfct


/** <p>End the pane</p>
 * Enter description here ...
 * @param string $content
 * @param string $position
 * Possible values of $position are:
 * left, right L, R
 * fieldset	F
 * edit: E  for the edit button
 * standard: S for a standard div
 */
	function cell( $content, $position='' ) {
		$this->htmlCell .= $content;
	}//endfct


/** <p>End the pane</p>
	*/
	public function line( $params=null ) {
		if ( empty($this->_params) ) $this->_params = $params;
		$this->startPage();
		$this->htmlContent .= $this->htmlCell;//clear at the end of the line
		$this->endPage();
		$this->htmlCell = '';
	}//endfct

/** <p>End the pane</p>
	*/
	public function add( $content ) {
		$this->htmlContent .= $content;
	}//endfct


/** <p>End one tab</p>
	*/
	public function body() {
		$this->endPane();
		$this->content = $this->htmlContent;
		$this->htmlContent = '';
	}//endfct


/**
	* <p>Print one row for an Edit Table (table with 2 colums)</p>
	*/
	public function miseEnPageTwo( &$params, $value ) {
		$this->_params = $params;
		$name = $params->name;
		$title='';

//__START__
		if ( defined('PLIBRARY_NODE_PLEX') && PLIBRARY_NODE_PLEX ) {
			$testingTraceC = WClass::get( 'testing.trace', null,'class',false );
			if ( $testingTraceC && $testingTraceC->check( 'direct-edit' ) ) {
				$directEditClass = WClass::get('view.directedit');
				$this->cell( $directEditClass->editView( 'form', $params->fid ) , 'edit' );
			}//endif
		}//endif
//__END__


		//direct edit for users
		if ( WPref::load( 'PMAIN_NODE_DIRECT_EDIT' ) ) {
			$outputDirectEditC = WClass::get( 'output.directedit' );
			$editButton = $outputDirectEditC->editView( 'form', $params->yid, $params->fid );
			if ( !empty($editButton) ) $this->cell( $editButton , 'edit' );
		} elseif ( WPref::load( 'PMAIN_NODE_DIRECT_TRANSLATE' ) ) {
			$outputDirectEditC = WClass::get( 'output.directedit' );
			$editButton = $outputDirectEditC->translateView( 'form', $params->yid, $params->fid, $name );
			if ( !empty($editButton) ) $this->cell( $editButton , 'edit' );
		}//endif

		$tip = $params->description;
		$required = $params->required;
		$notitle = 0;
		if ( isset($params->notitle) ) $notitle = $params->notitle;
		if ( isset($params->flip) ) $flip=$params->flip;
		if ( isset($params->lbreak) ) $lbreak=$params->lbreak;

		$tipsType = ( !empty( $params->tipstyle ) ? $params->tipstyle : false );
		if ( empty($tipsType) && isset(WRender_Form_class::$formTipStyle) ) $tipsType = WRender_Form_class::$formTipStyle;

		//we set the title first
		if ( empty($notitle) ) {	//in case of links we want the title and value to merge

//			$req='<b class="required" title="' . WText::lib('Required Field!') . '" >*</b>';
			$req = '<span class="star"> *</span>';

			if ( $tip && ! $tipsType ) {	//translation already done by the mosToolTip function

//debug( 33018701, WView::$directionForm[$params->yid] );

				$toolTipsO = WPage::newBluePrint( 'tooltips' );
				$toolTipsO->tooltips = $tip;
				$toolTipsO->title = $name;
				$toolTipsO->text = $name;
				$toolTipsO->id = $params->idLabel;
				$toolTipsO->bubble = true;
				if ( 'form-horizontal' == WView::$directionForm[$params->yid] ) {
					$toolTipsO->class = 'col-sm-2 control-label';
				} elseif( 'form-inline' == WView::$directionForm[$params->yid] ) {
					$toolTipsO->class = 'sr-only';
				}//endif

				$s = WPage::renderBluePrint( 'tooltips', $toolTipsO );

//				$s =  WView::tooltip( $tip, $name, $name,'tip_'.$params->idLabel, true ) ;
				//if required add an asterisk and bold it red
				if ($required==1 && $params->editItem ) $title.=$s.$req ;
				else $title.= $s;
			} else {

				//if required add an asterisk and bold it red
				if ( $required==1 && $params->editItem ) $titleNude = $name . $req;
				else $titleNude = $name;

				if ( 'form-horizontal' == WView::$directionForm[$params->yid] ) {
					$Labelclass = ' class="col-sm-2 control-label"';
				} elseif( 'form-inline' == WView::$directionForm[$params->yid] ) {
					$Labelclass = ' class="sr-only"';
				} else {
					$Labelclass = '';
				}//endif

				$title .= '<label' . $Labelclass . ' for="' . $params->idLabel . '">' . $titleNude . '</label>';

			}//endif

		} else {
			//since we remove just the title we still need the form indentation
			$title = '<div class="col-sm-2">&nbsp;</div>';
		}//endif

		//create new style of tooltips if exsit
		if ( $tipsType ) {
			// this is a hack the tips shoudl be added to the at a differnt place
			if ( substr( $value, 0, 23 ) == '<div class="col-sm-10">' ) {
				switch( $tipsType ) {
					case 'abovefield':
						$value = '<div class="col-sm-10"><div class="text-muted tipsAbove">' . $tip . '</div>' . substr( $value, 23 );
						break;
					case 'belowfield':
						$value = substr( $value, 0, -6 ) . '<div class="text-muted tipsBelow">' . $tip . '</div></div>';
						break;
					default:
						break;
				}//endswitch
			}//endif
		}//endif
//debug( 5666114, $value );

		$this->td_c = 'key' . $params->spantit;
		//to flip and put line breaks where needed
		if ( !empty($flip) ) {
			//first column
			//if there is a colspan on the value column we don't create the title cell
			//and we set the colspan to the value of the colspan
			//it works the samethe other way around

			if (!$params->spantit){
				if ( !$params->spanval ) {
					//set the value cell
//					$this->td_va = 'top';
					$this->cell( $value, 'right' );

					//set the title cell
//					$this->td_c = '';
					$this->cell( $title, 'left' );
				} else {
					//set the title cell
//					$this->td_colspan = 2;
//					$this->td_va = 'top';
					$this->cell( $title, 'left' );
				}//endif
			}else {
				//set the value cell
//				$this->td_colspan = 2;
//				$this->td_va = 'top';
				$this->cell( $value, 'right' );
			}//endif

		} else {
			//first column
			//if there is a colspan on the value column we don't create the title cell
			//and we set the colspan to the value of the colspan
			if ( !$params->spantit ) {
				if ( !$params->spanval ) {
					//set the title cell
//					$this->td_va = 'top';
					$this->cell( $title, 'left' );

					//set the value cell
//					$this->td_c = '';
					$this->cell( $value, 'right' );
				} else {
					//set the title cell
//					$this->td_colspan = 2;
//					$this->td_va = 'top';
					$this->cell($title, 'left' );
				}//endif
			}else {
				//set the value cell
//				$this->td_colspan = 2;
//				$this->td_va = 'top';
				$this->cell( $value, 'right' );
			}//endif
		}//endif

		//third column for description line
		if ( isset($params->extracol) ) $this->cell( $tip, 'third' );

		 return $this->line();//.$this->crlf;

	}//endfct

}//endclass