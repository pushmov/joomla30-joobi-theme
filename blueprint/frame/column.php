<?php
defined('JOOBI_SECURE') or die('J....');


/**
* <p>Class to create a tab like pane.</p>
* @author Joobi Team
*/
class WPane_column extends WPane {

	private $_htmlContentA = array();

	private $_isRTL = null;


/**
	* Constructor
 * @param array $params parameters
	*/
//	function __construct( $params = array() ) {
//		parent::__construct($params);
//	}//endfct


/** <p>Start the pane</p>
	 * @param array $params parameters
*/
	public function startPane( $params ) {
		$this->_isRTL = WPage::isRTL();
		$this->_htmlContentA = array();
	}//endfct




/** <p>End the pane</p>
	*/
	public function endPane() {

		if ( empty($this->_htmlContentA) ) {
			$this->content = '';
			return '';
		}//endif

		//count columns
		$total = count( $this->_htmlContentA );

		if ( $total > 12 ) $total = 12;
		

		//calculate the width of the columns
		$totalUsed = 0;
		$totalExitCol = 0;
		if ( !empty($this->_columnWidthA) ) {
			foreach( $this->_columnWidthA as $key => $col ) {
				if ( !empty($col) ) {
					$new = floor( 12 * $col / 100 );
					$this->_columnWidthA[$key] = $new;
					$totalUsed += $new; 
					$totalExitCol++;
				}//endif
			}//endforeach
		}//endif
		
		$totalLEft = $total - $totalExitCol;
		if ( $totalLEft > 0 ) $indiceRef = floor( (12-$totalUsed) / ( $totalLEft ) );
		else $indiceRef = 0;
		
		$html = '<div class="container-fluid"><div class="row">';
		foreach( $this->_htmlContentA as $key => $oneColumn ) {
			
			if ( !empty( $this->_columnWidthA[$key] ) ) $indice = $this->_columnWidthA[$key];
			else $indice = $indiceRef;

			//pushRight
			$pushRight = ( $this->_isRTL ? ' col-md-push-' . $indice : '' );
			
			$html .= '<div class="col-md-' . $indice . $pushRight . '">' . $oneColumn . '</div>';
		}//endforeaach
		$html .= '</div></div>';

		$this->content = $html;

	}//endfct


/** <p>Start one tab</p>
 * @param array $params parameters
	*/
	public function startPage( $params ) {
	}//endfct

/** <p>End the pane</p>
	*/
	function add( $content ) {

		$this->_htmlContentA[] = $content;

	}//endfct

/** <p>End one tab</p>
	*/
	public function endPage() {
	}//endfct

}//endclass
