<?php
defined('JOOBI_SECURE') or die('J....');


class WPane_div extends WPane {

	function miseEnPageTwo( &$params, $value ) {
		$this->content = $value;
		return $value;
	}//endfct

/** <p>Start the pane</p>
	 * @param array $params parameters
*/
	public function startPane( $params ) {
		return $this->content;
	}//endfct


/** <p>End the pane</p>
	*/
	public function endPane() {
		return $this->content . $this->crlf;
	}//endfct


/** <p>Start one tab</p>
 * @param array $params parameters
	*/
	public function startPage( $params ) {
		return $this->content;
	}//endfct


/** <p>Start one tab</p>
 * @param array $params parameters
	*/
	function add( $content ) {
		$this->content .= $content;
		return $this->content;
	}//endfct

	public function line() {
//		$this->endPane();
//		$this->content = $this->htmlContent;
//		$this->htmlContent = '';
	}//endfct

/** <p>End one tab</p>
	*/
	public function body() {
		$this->endPane();
	}//endfct

/** <p>End one tab</p>
	*/
	public function endPage() {
		return $this->content . $this->crlf;
	}//endfct


}//endclass
