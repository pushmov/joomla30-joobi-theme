<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Class to create a tab like pane.</p>
* @author Joobi Team
*/
class WPane_tabvertical extends WPane {

	public $fade = true; 	// use fade on tab

	private $_navTabHTMLA = array();

	private $_tabPaneHTMLA = array();

	private static $_paneIcon = null;

	private $_leftTabs = true;
	private $_sideways = false;

	private $_params = null;

/** <p>Start the pane</p>
	 * @param array $params parameters
*/
	public function startPane( $params ) {

		$this->_params = $params;
		if ( !isset( self::$_paneIcon ) ) {
		  	self::$_paneIcon = WPage::renderBluePrint( 'initialize', 'pane.icon' );
		}//endif

		$this->_navTabHTMLA = array();
		$this->_tabPaneHTMLA = array();

	}//endfct


/** <p>End the pane</p>
	*/
	public function endPane() {

		WPage::addCSSFile( 'css/vertical-tabs.css' );

		$sideways = ( $this->_sideways ? ' sideways' : '' );

		if ( $this->_leftTabs ) {

			$this->content = '<div class="col-xs-3">';
			$this->content .= '<ul class="nav nav-tabs tabs-left' . $sideways . '">';
			$this->content .= implode( '', $this->_navTabHTMLA );
			$this->content .= '</ul>';
			$this->content .= '</div>';

			$this->content .= '<div class="col-xs-9">';
			$this->content .= '<div class="tab-content">';
			$this->content .= implode( '', $this->_tabPaneHTMLA );
			$this->content .= '</div>';
			$this->content .= '</div>';

		} else {

			$this->content = '<div class="col-xs-3">';
			$this->content .= '<div class="tab-content">';
			$this->content .= implode( '', $this->_tabPaneHTMLA );
			$this->content .= '</div>';
			$this->content .= '</div>';

			$this->content .= '<div class="col-xs-9">';
			$this->content .= '<ul class="nav nav-tabs tabs-right' . $sideways . '">';
			$this->content .= implode( '', $this->_navTabHTMLA );
			$this->content .= '</ul>';
			$this->content .= '</div>';

		}//endif


		return $this->content;

	}//endfct


/** <p>Start one tab</p>
 * @param array $params parameters
	*/
	public function startPage( $params ) {
		$this->content = '';
	}//endfct


/** <p>End one tab</p>
	*/
	public function endPage( $params ) {

		if ( empty($this->content) ) {
			return '';
		}//endif
//debug( 566611, $params );
		$this->fade = false;

		static $count = 0;
		$count++;

		static $active = true;
                //check if we use cookie
                //this is trick because this function calls twice
//                static $_addedJsAlrady = 0;
               // if ( isset($this->useCookies) ) {
//                if ( $_addedJsAlrady === 0 ) {
                    //this is trick because this function calls twice
//                    $_addedJsAlrady++;
//                    $js  = '';
                    //here i need to check what tab was opened and make it active
//                    $js .= 'window.WApps.helpers.makeTabActive("' . $params->idText . '");';
//                    $js .= 'setTimeout(window.WApps.helpers.makeTabActive(), 0);';
                    //save new opened tab to cookie
//                    $js .= 'setTimeout(window.WApps.helpers.setToCookieActiveTab(),0);';
//                    $js .= 'window.WApps.helpers.setToCookieActiveTab("' . $params->idText . '");';
//                    WPage::addJSScript( $js,'default', false );

//                }
//debug( 50033112, 'Define the active tab???' );

		if ( $active ) {
			$activeClassNav = ' class="active"';
			$activeClassPane = ' active';
//			if ( $this->fade ) $activeClassPane = ' fade in' . $activeClassPane;
		} else {
			$activeClassNav = '';
//			if ( $this->fade ) $activeClassPane = ' fade';
//			else $activeClassPane = '';
			$activeClassPane = '';
		}//endif

		if ( $active ) $active = false;

		$myID = $params->id . '_' . $count;

		$navTabHTML = '<li' . $activeClassNav . '><a href="#' . $myID . '" data-toggle="tab">';
		if ( self::$_paneIcon && !empty($params->faicon) ) $navTabHTML .= '<i class="fa ' . $params->faicon . '"></i>';
//		$navTabHTML .= $params->text;
//		$navTabHTML .= '<h3 class="panel-title">' . $params->text . '</h3>';	// for screen reader
		$navTabHTML .= $params->text;	// for screen reader
		$navTabHTML .= '</a></li>';

		$tabPaneHTML = '<div class="tab-pane' . $activeClassPane . '" id="' . $myID . '">' . $this->content . '</div>';

		//reset the content now that we have taken it into the tab
		$this->content = '';

		$this->_navTabHTMLA[] = $navTabHTML;
		$this->_tabPaneHTMLA[] = $tabPaneHTML;

		return '';

	}//endfct


}//endclass
