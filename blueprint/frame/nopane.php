<?php
defined('JOOBI_SECURE') or die('J....');


/**
* <p>Class to create a slider like pane.</p>
* @author Joobi Team
*/
class WPane_nopane extends WPane {

	/**
	* <p>Stub function</p>
	*/
	function miseEnPageTwo( &$params, $value ) {
		$this->content = $value;
		return $value;
	}//endfct

	function body() {
	}//endfct

	function cell( $value, $notUsed=null ) {
		$this->content .= $value;
		return $value;
	}//endfct

	function line() {
	}//endfct


/** <p>Start the pane</p>
	* @param array $params parameters
	*/
	public function startPane( $params ) {

	}//endfct


/** <p>End the pane</p>
	*/
	public function endPane() {
	}//endfct


/** <p>Start a slider</p>
	* @param array $params parameters
	*/
	public function startPage( $params ) {

	}//endfct


/** <p>End a slider</p>
	*/
	public function endPage() {

	}//endfct

}//endclass

