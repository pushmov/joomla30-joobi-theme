<?php
defined('JOOBI_SECURE') or die('J....');


/**
* <p>Class to create a tab like pane.</p>
* @author Joobi Team
*/
class WPane_row extends WPane {

	private $_htmlContentA = array();
//	private $_isRTL = null;

/**
	* Constructor
 * @param array $params parameters
	*/
//	function __construct( $params = array() ) {
//		parent::__construct($params);
//	}//endfct


/** <p>Start the pane</p>
	 * @param array $params parameters
*/
	public function startPane( $params ) {
//		$this->_isRTL = WPage::isRTL();
		$this->_htmlContentA = array();
	}//endfct



/** <p>End the pane</p>
	*/
	public function endPane() {

		if ( empty($this->_htmlContentA) ) {
			$this->content = '';
			return '';
		}//endif

		//pull
//		$pull = ( $this->_isRTL ? ' pull-right' : ' pull-left' );

		//count columns
		$html = '<div class="clearfix">';	// ' . $pull . '
		foreach( $this->_htmlContentA as $oneColumn ) {
			$html .= $oneColumn;
		}//endforeaach
		$html .= '</div>';

		$this->content = $html;
//		return $this->content;

	}//endfct


/** <p>Start one tab</p>
 * @param array $params parameters
	*/
	public function startPage( $params ) {
	}//endfct

/** <p>End the pane</p>
	*/
	function add($content) {
		$this->_htmlContentA[] = $content;
	}//endfct

/** <p>End one tab</p>
	*/
	public function endPage() {
	}//endfct

}//endclass
