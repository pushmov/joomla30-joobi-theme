<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Alert_class extends Theme_Render_class {

/**
 *
 * This function is to render a button
 * @param object $data
 */
  	public function render( $newArraySort ) {


  			$extraSound = '';
			$html = '<div id="WMessage">';

			foreach( $newArraySort as $type => $allMessages ) {
				if ( empty($allMessages) ) continue; //skip if the message is empty

					if ( $type == 'beep' ) {
						if ( PLIBRARY_NODE_ENABLESOUND ) {
							$browser = WPage::browser( 'namekey' );
							$extension = ( $browser=='safari' || $browser=='msie' ) ? 'mp3' : 'ogg';
							$URLBeep = PLIBRARY_NODE_CDNSERVER . '/joobi/user/media/sounds/' . $allMessages[0]->message . '.'. $extension;
							$extraSound .= '<audio autoplay="true" src="' . $URLBeep . '" preload="auto" autobuffer></audio>';
						}//endif
						continue;
					}//endif

				switch ( $type ) {
					case 'success':
						$alertClas ='success';
						$faicon = 'fa-thumbs-up';
					break;
					case 'error':
						$alertClas ='danger';
						$faicon = 'fa-times';
					break;
					case 'warning':
						$alertClas ='warning';
						$faicon = 'fa-exclamation-triangle';
					break;
					case 'notice':
					case 'text':
					default:
						$alertClas ='info';
						$faicon = 'fa-exclamation-circle';
					break;
				}//endswitch

				$childMsgs = '';

				$alertCollapse = $this->value( 'alert.collapse' );
				$alertDismiss = $this->value( 'alert.dismiss' );

				$count=0;
				foreach( $allMessages as $oneM ) {

					$count++;
					$msgid = 'msg_' . rand(0,99) . '_' . $count;
					$onClick = ' onclick="joobi.flip(\''.$msgid.'\', 1);"';


					//in case the message is not a string may be an array or obejct
//					if ( !is_string($oneM->message) ) $oneM->message = print_r( $oneM->message, true );
					if ( !is_string($oneM->message) ) $oneM->message = '<pre>' . var_dump( $oneM->message, true ) . '</pre>';

					//if we have variables to replace
					if ( !empty($oneM->variable) ) {
						$oneM->message = str_replace( array_keys($oneM->variable), array_values($oneM->variable), $oneM->message );
					}//endif

					//repalce the link into the message to look good with proper link color
					$oneM->message = str_replace( '<a ', '<a class="alert-link" ', $oneM->message );

					$HMessage = ( JOOBI_FRAMEWORK != 'netcom' ) ? $oneM->message : 'XML-RPC SERVER MESSAGE: ' . $oneM->message;

					$childMsgs .= '<div class="alert alert-' . $alertClas . '';
					if ( $alertDismiss ) {
						$childMsgs .= ' alert-dismissable';
					}//endif
					$childMsgs .= '">';
					if ( $alertDismiss ) $childMsgs .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

					if ( $alertCollapse ) {
						$childMsgs .= '<dl class="dl-horizontal">';
						$childMsgs .= '<dt>';
						$childMsgs .= '<i class="fa ' . $faicon . ' fa-lg messageIcons"' . $onClick . '></i>';
						$childMsgs .= '</dt>';
						$childMsgs .= '<dd>';
					}//endif


					$childMsgs .= '<div id="' . $msgid . '" class="message-contain">';
					$childMsgs .= $HMessage;
					$childMsgs .= '</div>';

					if ( $alertCollapse ) {
						if ( JOOBI_FRAMEWORK != 'netcom' ) {
							$childMsgs .= '<div id="msg'.$msgid.'" class="message-flip" style="display:none;" '.$onClick.'>';	// style="display: none;padding:.5em 0 .5em 2em;"
							$MESSAGETYPE = $type;
							$childMsgs .= WText::translate( 'Click here to view '.$MESSAGETYPE.' message.' );
							$childMsgs .= '</div>';
						}//endif

						$childMsgs .= '</dd>';
						$childMsgs .= '</dl>';
					}//endif

					$childMsgs .= '</div>';

				}//endforeach

				$html .= $childMsgs;

			}//endforeach


			$html .= $extraSound;

			$html .= '</div>';


		return $html;

  	}//endfct

}//endclass