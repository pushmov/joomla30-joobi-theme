<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/

/**
 *
 * this class is the object to create a widgetbox with all the properties possible to use
 */
class WRender_Widgetbox_classObject {

	/*
	 * The title of the widget
	 */
	public $title = '';

	/*
	 * The main content of the widget
	 */
	public $content = '';

	/*
	 * The id string
	 */
	public $id = null;

	/*
	 * An array of content to put on the right side of the title
	 */
	public $headerRightA = array();
	public $headerCenterA = array();
	public $bottomRightA = array();
	public $bottomCenterA = array();

	/*
	 * Define the style and icon
	 */
	public $faicon = null;
	public $color = null;


}//endclass


class WRender_Widgetbox_class extends Theme_Render_class {

	private static $_paneIcon = null;
	private static $_paneColor = null;


/**
 *
 *	WPage::renderBluePrint( 'panel', $data );
 *
 * @param object $data
 * -> header
 * -> body
 * -> footer
 * -> faicon
 * -> color
 * -> headerRight array() : the picklist or other things to show on the right of the header it is an array so we can add as many as we want
 */
  	public function render( $data ) {

  		if ( empty($data->content) || '<div></div>' == $data->content ) return '';

  		$container =

		$panel = WPage::newBluePrint( 'panel' );
		$panel->type = $this->value( 'catalog.container' );
//debug( 5666221, $panel->type );
		$panel->header = $data->title;
		$panel->body = $data->content;
		if ( !empty($data->id) ) $panel->id = $data->id;
		$panel->headerRightA = $data->headerRightA;
		$panel->headerCenterA = $data->headerCenterA;
		$panel->bottomRightA = $data->bottomRightA;
		$panel->bottomCenterA = $data->bottomCenterA;
		$panel->faicon = $data->faicon;
		$panel->color = $data->color;
		$panel->class = 'widgetBox';

		$widgetHTML = WPage::renderBluePrint( 'panel', $panel );
		return $widgetHTML;


  	}//endfct

}//endclass