<?php
defined('JOOBI_SECURE') or die('J....');


/**
* @version $Id: standard.php 550 2007-01-23 12:05:56Z c $
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/

/**
* <p>Translated field with ajax popup.</p>
* @author Joobi Team
*/
WLoadFile( 'form.text' , JOOBI_LIB_HTML );
class WForm_standardDatetime extends WForms_default {


/**
	* <p>Edit function return the text to print in edit multiforms</p>
	*/
	function create() {

		if ( !empty($this->element->readonly) ) return $this->show();

		/*$size = ( isset( $this->element->fieldsize ) ) ? $this->element->fieldsize : '12';
		$max = ( isset( $this->element->maxsize ) ) ? $this->element->maxsize : '10';*/
		$class = ( ( !empty( $this->element->classes ) ) ? $this->element->classes : 'inputbox' );

		//$formats is an array with all possible format for date
		$formats = array();
		$formats['dateonly'] = array( 'php' => "Y-m-d", 'js' => "yyyy-mm-dd", 'default' => "0000-00-00", 'hour' => 'false' );
		$formats['datetime'] = array( 'php' => "Y-m-d H:i", 'js' => "yyyy-mm-dd hh:ii", 'default' => "0000-00-00 00:00", 'hour' => 'true' );

		//The format is changed depending on the value of the form param
		//Only 1 or 2 is implemented yet
		//1 for standard date without time
		//2 for standard date + time
		if ( !empty($this->element->formatdate) && !empty($formats[$this->element->formatdate]) ) $format = $this->element->formatdate;
		else $format = 1;


		if ( !empty($this->value) ) {
			//if ( is_numeric($this->value) ) $this->value=date("Y-m-d H:m:s", $this->value);
			if ( is_numeric($this->value ) ) {
				//We only add the timezone if we display the time too
				//The WUser::timezone() returns the timezone compared to the server location so that date is OK
//				if ($this->dateFormat == 'datetime' ) $this->value = $this->value + WUser::timezone();
				$this->value = WApplication::date( $formats[$this->dateFormat]['php'], $this->value );
			}//endif
		} else {
			$this->value = $formats[$this->dateFormat]['default'];
		}//endif


		if ( '0000-00-00 00:00' == $this->value || '0000-00-00' == $this->value ) {
			$dateFormat = WApplication::date( $formats[$this->dateFormat]['php'], time() );
		} else {
			$dateFormat = $this->value;
		}//endif

		$readonly = empty( $this->element->readonly ) ? '' : ' readonly';
		//couponused
		$html = '<div id="' . $this->idLabel . '" class="input-append date form_datetime" data-date="' . $dateFormat . '" data-date-format="' . $formats[$this->dateFormat]['js'] . '">';
		$html .= '<input size="16" type="text" name="' . $this->map .'" value="' . $this->value . '"' . $readonly . '>';

		if ( empty($readonly) ) $html .= '<span class="add-on"><i class="fa fa-times"></i></span><span class="add-on"><i class="fa fa-calendar"></i></span></div>';

		$use24Format = WPref::load( 'PMAIN_NODE_DATEFORMAT' );
		$use24Hour = ( !empty($use24Format) ? '0' : '1' );

		//this is the minimum value possible.
		$minimumDate = '1980-01-01 00:01';

		$jsString = "jQuery('#". $this->idLabel . "').datetimepicker({
format:'". $formats[$this->dateFormat]['js'] ."',
startDate:'". $minimumDate ."',
todayBtn:1,";
if ( $this->dateFormat == 'dateonly' ) $jsString .= "minView:2,";
$jsString .= "todayHighlight:true,
weekStart:1,
autoclose:1,
startView:2,
forceParse:0,
showMeridian:" . $use24Hour . "
});";


		WPage::addJSFile( 'js/bootstrap-datetimepicker.js' );
		WPage::addCSSFile( 'css/bootstrap-datetimepicker.css' );

		WPage::addJSScript( $jsString );

		$this->content = $html;
		return true;

	}//endfct

}//endclass
