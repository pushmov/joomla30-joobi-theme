<?php
defined('JOOBI_SECURE') or die('J....');


/**
* @version $Id: standard.php 550 2007-01-23 12:05:56Z c $
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/

/**
* <p>Translated field with ajax popup.</p>
* @author Joobi Team
*/
WLoadFile( 'form.text' , JOOBI_LIB_HTML );
class WForm_standardTrans extends WForm_text {

/**
 *
 * Enter description here ...
 */
	function create() {
		return parent::create();
	}//endfct

/**
 *
 * Enter description here ...
 */
	protected function renderCreate() {

		$buttonO = WPage::newBluePrint( 'button' );
		$buttonO->type = 'infoLink';
		$buttonO->tooltips = $this->infoBubble;
		$buttonO->link = $this->url;
		$buttonO->useTitle = false;
//		$buttonO->title = $this->translationOF;
		$buttonO->text = '<i class="fa fa-language"></i>';
		$buttonO->id = 'tr_' . $this->idLabel;
		$buttonO->popUpIs = true;
//		$buttonO->popUpWidth = '80%';
//		$buttonO->popUpHeight = '50%';
		$buttonO->popUpWidth = '600';
		$buttonO->popUpHeight = '250';

		$myBotton = WPage::renderBluePrint( 'button', $buttonO );
//debug( 54222, 'the model popup break the page' );
		// <span class="input-group-addon">.00</span>
		$html = $this->content;
		$html .= '<span class="add-on">';
		$html .= $myBotton;
//		$html .= $buttonO->text;
		$html .= '</span>';

//debug( 4323435, $html );
		$this->content = $html;

	}//endfct


}//endclass
