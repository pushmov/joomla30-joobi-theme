<?php
defined('JOOBI_SECURE') or die('J....');


/**
* @version $Id: standard.php 550 2007-01-23 12:05:56Z c $
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/


/**
* <p>Upload File Field</p>
* @author Joobi Team
*/
WLoadFile( 'form.text' , JOOBI_LIB_HTML );
class WForm_standardMedia extends WForm_text {
//class WForm_media extends WForm_text {

	private static $_countUploads = 0;
	protected $inputType = 'file';

	private $_maxFileSizeShow = 0;

	private $_allowedFormatsA = array();

	private $_maxFiles = 1;


/**
	* Upload form with new feature to add a picklist of media types on external urls
	* Edit function
	*/
	function create() {
		self::$_countUploads ++;

		// Make sure that file uploads are enabled in php
		if ( !(bool) ini_get('file_uploads') ) {
			$message = WMessage::get();
			$message->adminW('You need to enable file upload in your PHP configuration to upload files.');
			$this->content = '';
			return false;
		}//endif

		static $fieldCount = array();
		static $file = null;
		$media = false;

		$onlyURL = WGlobals::get( 'mediaUploadOnlyURL_' . $this->element->namekey, false, 'global' );

		$formObject = WView::form( $this->formName );
		$formObject->enctype = true;

		//create the basic upload field
		if ( in_array( $this->element->typeName, array( 'media' ) ) ) {	// , 'image'
			 $this->element->typeName = 'file';
			 $media = true;
		}//endif

		$this->_maxFiles = ( empty($this->element->maxnbupload) || $this->element->maxnbupload < 1 ) ? 1 : ( $this->element->maxnbupload > PLIBRARY_NODE_FANCYUPLOADMAX ? PLIBRARY_NODE_FANCYUPLOADMAX : $this->element->maxnbupload );


		if (  isset($this->onlyinputClass) && $this->onlyinputClass ) return;

		//map this media file column in trucs
///////////////////////////////////////////////////////////////////////////////////
		//$mediaMapId = 'mediamap_'.$this->element->sid .'_'.$this->element->map;
		$mediaMapId = 'mediamap_'.$this->element->sid .'_'.$this->element->map.'_'.self::$_countUploads;
		$mediaMapName = 'trucs['.$this->element->sid .'][x][mdpthtp]['.$this->element->map.']';

		if ( !$onlyURL ) {


			if ( is_numeric($this->value) ) {
				//when updating a file we want to remove the old one so we do that
				$this->content .='<input type="hidden" id="'.$mediaMapId .'old_'.$this->idLabel.'" name="'.$mediaMapName .'[filid_remove]"  value="'.$this->value.'" />';
			}//endif

			//load translations
			WText::load( 'output.node' );

			$maxFileSizeShowHTML = $this->_maxFileUpload();

			if ( !empty( $this->element->standardupload) ) {
				$fancyFileUpload = false;
			} else {
				$filesFancyuploadC = WClass::get( 'files.fancyupload' );
				$fancyFileUpload = $filesFancyuploadC->check();
			}//endif


			if ( $fancyFileUpload ) {

				//$mediaMapId = 'mediaMap_'.$this->element->sid .'_'.$this->element->map;
				$mediaMapId = WGlobals::filter( 'mediaMap_'.$this->element->sid .'_'.$this->element->map."_".self::$_countUploads, 'alnum' );
	//			$fielUploadURL = WPage::completeURL( 'controller=output&task=uploadfile&wajx=1', true, true );
				$fielUploadURL = WPage::linkAjax( 'controller=output&task=uploadfile&wajx=1' );


				WText::load( 'output.node' );
	  			$mytranslation = array();
	            $mytranslation['selectfile'] = WText::translate( 'Select Files' );
	            $mytranslation['add_files'] =  WText::translate( 'Add file' );
	            $mytranslation['start_upload'] = WText::translate( 'Start upload' );
	            $mytranslation['remove_all'] = WText::translate( 'Remove all' );
	            $mytranslation['close'] = WText::translate( 'Close' );
	            $mytranslation['error_method_doesnot_exists_1'] = 'Method';
	            $mytranslation['error_method_doesnot_exists_2'] = 'does not exist on jQuery.ajaxupload';

	            $mytranslation = WGlobals::filter( $mytranslation, 'safejs' );

	            $fileID = $this->element->sid .'_'.$this->element->map;

	            $mytranslation_json = json_encode($mytranslation);
				//we put the line at the begining of the file otherwise in the HTML page we will get the indentation as well.  We try to optimized for speed.
				$JScode = "var mytranslation='". $mytranslation_json."';
jQuery(document).ready(function() {
jQuery('#".$mediaMapId."').ajaxupload({
url:'".$fielUploadURL."',
autoStart:  true,
foraxfiles: '".$fileID."',
dropArea:'#drophere_".self::$_countUploads."',";

				if ( !empty($this->element->sid) ) {
					//check on the restricted files
					$usedModelM = WModel::get( $this->element->sid );
					if ( !empty( $this->_allowedFormatsA ) ) {
						if ( !is_array($this->_allowedFormatsA) ) $this->_allowedFormatsA = explode( ',', $this->_allowedFormatsA );
						$acceptedFormat = implode( "','", $this->_allowedFormatsA );
						$acceptedFormat = strtolower($acceptedFormat);
						$JScode .= " allowExt:['" . $acceptedFormat . "'],";
					}//endif
				}//endif

				$JScode .= " maxFiles:'" . $this->_maxFiles . "',";
				$JScode .= " form:'#".$this->formName."',";
				$JScode .= " maxFileSize:'" . $this->_maxFileSizeShow . "'});});";

				WPage::addJSLibrary( 'jquery' );
				WPage::addJSFile( 'js/ajaxupload.js' );
	//			WPage::addJSFile( 'main/js/ajaxupload.js', 'inc' );
				WPage::addJSScript( $JScode, 'default', false );
				WPage::addCSSFile( 'css/ajaxupload.css' );

				$this->content = '<div id="'.$mediaMapId.'" class="clearfix"> </div>';
				$this->content .= '<div  class="drophere" id="drophere_'.self::$_countUploads.'">' . WText::translate( 'Drag files here or click "Add files" to choose' ) . '<br/><i class="fa fa-cloud-download fa-4x"></i>';
				if ( !empty( $maxFileSizeShowHTML ) ) $this->content .= '<br/><p class="text-warning">' . $maxFileSizeShowHTML . '</p>';
				$this->content .= '</div>';

			} else {
				parent::create();
				if ( !empty( $maxFileSizeShowHTML ) ) $this->content .= $maxFileSizeShowHTML;
			}//endif




			$eid = WGlobals::getEID();
			if ( !empty($eid) && $media ) {	//if media type, add edit and delete button

				//edit button

				$map = $this->element->map;
				$model = $this->element->sid;
				if ( !empty($this->value) && $this->_maxFiles < 2 ) {
					$textLink = WText::translate('Edit');
					$iconO = WPage::newBluePrint( 'icon' );
					$iconO->icon = 'edit';
					$iconO->text = WText::lib( 'Edit' );
				} else {
					$iconO = WPage::newBluePrint( 'icon' );
					$iconO->icon = 'attachment';
					$iconO->text = WText::translate( 'Attach Existing File' );
					$textLink = WText::translate('Attach Existing File');
				}//endif

	//			$attach = '<img src="'. $edit .'"/> ' . $textLink;
				$attach = WPage::renderBluePrint( 'icon', $iconO ) . $textLink;


				$objButtonO = WPage::newBluePrint( 'button' );
				$objButtonO->text = '<div style="display:block;padding-top:3px;vertical-align:middle;">'. $attach.'</div>';
				$objButtonO->type = 'standard-showAll';
				$objButtonO->wrapperDiv = 'mediaButtonBord';
	//			$attachButton = WPage::renderBluePrint( 'button', $objButtonO );
				$objButtonO->link = WPage::linkPopUp( 'controller=files-attach&task=listing&pid=' . $this->eid.'&map='.$map . '&model=' . $model );
				$objButtonO->popUpIs = true;
				$objButtonO->popUpHeight = '90%';
				$objButtonO->popUpWidth = '90%';
				$attachButton = WPage::renderBluePrint( 'button', $objButtonO );
				$this->content .= '<span class="mediaWrapButton">' . $attachButton;


	//			$editLink = WPage::linkPopUp('controller=files-attach&task=listing&pid=' . $this->eid.'&map='.$map . '&model=' .$model );
	//			$this->content .= '<span class="mediaWrapButton">' . WPage::createPopUpLink( $editLink, $attachButton, '80%', '80%', 'toolbar' );

				if ( !empty($this->value) && $this->_maxFiles < 2 ) {	//if there's a file, add delete button

					//delete button
					$iconO = WPage::newBluePrint( 'icon' );
					$iconO->icon = 'delete';
					$iconO->text = WText::translate( 'Remove' );
					$attach = WPage::renderBluePrint( 'icon', $iconO );
					$controller = WGlobals::get('controller');
					$textLink = WText::translate( 'Remove' );
	//				$attach = '<img src="'. $edit .'"/> ' . $textLink;

					$objButtonO = WPage::newBluePrint( 'button' );
					$objButtonO->text = '<div style="display:block;padding-top:3px;vertical-align:middle;">' . $attach . '</div>';
					$objButtonO->type = 'standard';
					$objButtonO->wrapperDiv = 'mediaButtonBord';
					$deleteIcon = WPage::renderBluePrint( 'button', $objButtonO );


					//main/view model
					$mainModel = WModel::get($this->element->sid, 'object' );
					$pk = $mainModel->getPK();

					//element model
					$model = WModel::get($this->element->sid, 'namekey' );

					$deleteLink = WPage::routeURL('controller=output&task=removefile&filid='.$this->value.'&map='.$map.'&pk='.$pk.'&model='.$model.'&controllerback='.$controller.'&id='.$this->eid);
					$text= WText::translate('Are you sure you want to remove?');
					$this->content .= '<a style="cursor: pointer" onclick=\'if (confirm("'.$text.'")) location.href="'.$deleteLink.'";\' >'.$deleteIcon.'</a>';

				}//endif

				$this->content .= '</span>';

			}//endif

			$this->content .='<div style="clear:both;"></div>';

		}//endif

		$myFileInfoO = new stdClass;
		//load existing file
		$filesMediaC = WClass::get( 'files.media' );
		if ( !empty($this->value) ) {
			$filesHelperC = WClass::get( 'files.helper' );
			$myFileInfoO = $filesHelperC->getInfo( $this->value );
		}//endif


//		$isMediaPick=false; //check flag  if media type is one of the external types, and use it as default value for the picklist
		//make picklist of media type
		if ( !empty($this->element->mpicklist) ) {

			$nameValue = ''; //value for existing external type
			if ( !empty($myFileInfoO->type) ) {
				//type is in the picklist
//				$mediatypes = WType::get('files.types');
				$mediatypes = WView::picklist( 'files_type' );
				if ( !$mediatypes->inValues( $myFileInfoO->type ) ) {
//					$nameValue = $myFileInfoO->name;
					$myFileInfoO->icontype = $myFileInfoO->type;
				}//endif
				if ( empty($myFileInfoO->icontype) ) {
					$mediaIconTypes = WType::get('files.icontypes');
					if ( !$mediaIconTypes->inValues( $myFileInfoO->type ) ) {
						$myFileInfoO->icontype = 'file';
					} else {
						$myFileInfoO->icontype = $mediaIconTypes->getName( $myFileInfoO->type );
					}//endif
				}//endif
			}//endif
			//create media types picklist
			$params = new stdClass;
			$params->default = !empty($myFileInfoO->type) ? $myFileInfoO->type : '';
			$params->outputType = 0; //type of display  0 dropdown
			$params->nbColumn = 1; //number of columns for the currencies
			$params->map = $mediaMapName.'[type]'; //override picklist naming
			$params->idlabel = $mediaMapId.'_type'; //override picklist idlabel

			//send the picklist to use
			$fileTypePicklist = WGlobals::get( 'mediaUploadFileType_' . $this->element->namekey, '', 'global' );
			WGlobals::set( 'mediaUploadFileTypePicklist', $fileTypePicklist, 'global' );

			$mediaPickList = WView::picklist( 'files_media_types', null, $params );
//debug( 5500, $mediaPickList );
//			$this->content .= '<br/><br/><br/>';
			$this->content .= $mediaPickList->display();
			//path input box
			$this->content .= '<input class="inputbox" type="text" size="50" id="'.$mediaMapId.'_name" name="'.$mediaMapName.'[name]" value="'.$nameValue.'">';

		}//endif - picklist content


//debug( 90001, $this->_maxFiles );
		if ( $this->_maxFiles < 2 ) {
			//make preview image
		  	if ( !empty($myFileInfoO) ) {
				//get the mime type
				if ( empty($myFileInfoO->mime) ) {
					$mimeA = array( 'unkown' );
				} else {
					$mimeA = explode( '/', $myFileInfoO->mime );
				}//endif

				if ( $mimeA[0]=='image' && !$myFileInfoO->secure ) {

					if ( $myFileInfoO->thumbnail ) {
						$myWidth = ( !empty($myFileInfoO->twidth) ) ? $myFileInfoO->twidth : ( ( $myFileInfoO->width > 48 ) ? 48 : $myFileInfoO->width);
						$myHeight = ( !empty($myFileInfoO->theight) ) ? $myFileInfoO->theight : ( ( $myFileInfoO->height > 48 ) ? 48 : $myFileInfoO->height);
					} else {
						$thumnailPath='';
						$myWidth = ( $myFileInfoO->width > 48 ) ? 48 : $myFileInfoO->width;
						$myHeight = ( $myFileInfoO->height > 48 ) ? 48 : $myFileInfoO->height;
					}//endif


					$myNewImageO = WObject::get( 'files.file' );
					$myNewImageO->name = $myFileInfoO->name;
					$myNewImageO->type = $myFileInfoO->type;
					$myNewImageO->basePath = JOOBI_URL_MEDIA;
					$myNewImageO->folder = ( empty($myFileInfoO->folder) ? 'media' : $myFileInfoO->folder );
					$myNewImageO->path = $myFileInfoO->path;
					$myNewImageO->fileID = $myFileInfoO->filid;
					$myNewImageO->thumbnail = $myFileInfoO->thumbnail;
					$myNewImageO->storage = $myFileInfoO->storage;
					$myNewImageO->secure = $myFileInfoO->secure;
					$url = $myNewImageO->fileURL();
	//				$url = JOOBI_URL_MEDIA. $filesMediaC->convertPath($myFileInfoO->path.$thumnailPath,'url').'/'.$myFileInfoO->filename;


					$startLink = '<a id="link'.$this->idLabel.'" href="#" onclick="window.open(\''.$url.'\', \'win2\', \'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width='.$myFileInfoO->width.',height='.$myFileInfoO->height.',directories=no,location=no\');return false;">';
					$this->content .=  '<div style="float:left; clear:both;"><div><strong>'.WText::translate('Current Image').$startLink.'</strong></div>';

					$data = WPage::newBluePrint( 'image' );
					$data->location = $url;
					$data->text = $myFileInfoO->filename;
					$data->width = $myWidth;
					$data->height = $myHeight;
					$this->content .= WPage::renderBluePrint( 'image', $data );

					$this->content .= '</a><div style="padding-top:10px;">' . $myFileInfoO->filename . '</div></div>';

				} else {

					if ( !empty($myFileInfoO->type) ) {

						$this->content .='<div style="clear:both;"></div>';
						$this->content .= '<span class="fileName">'. $myFileInfoO->filename .'</span>';
						$this->content .='<div style="clear:both;"></div>';
						$filesMediaC = WClass::get( 'files.media' );
						$this->content .= $filesMediaC->renderHTML( $this->value, $this->element ) .'<br />';

					}//endif

				}//endif

			}//endif

		}//endif


		//we put the field name into the load-fild array so we know which fields we need to get for files
		if ( !isset($fieldCount[$this->element->sid]) ) $fieldCount[$this->element->sid] = 0;
		$formObject = WView::form( $this->formName );
		$formObject->hidden( 'laod-fild['.$this->element->sid.']['.(int)$fieldCount[$this->element->sid].']', $this->element->map );
		$fieldCount[$this->element->sid]++;
		$listingSecurity = WGlobals::get( 'securityForm', array(), 'global' );
		$listingSecurity['sec'][] = $this->element->sid .'_' . $this->element->map;

		WGlobals::set( 'securityForm', $listingSecurity, 'global' );

		return true;

	}//endfct


}//endclass
