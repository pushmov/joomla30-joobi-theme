<?php
defined('JOOBI_SECURE') or die('J....');

//class Output_Helpermenu_class {
class WRender_Saveauto_class extends Theme_Render_class {

/**
	* <p>Load the script for the autosave of the form</p>
	* @param string $formname the formname of the view
	*/
	public function render( $formname ) {	//	autoSave( $formname ) {

			if ( defined('PLIBRARY_NODE_TIMEINTERVAL') && PLIBRARY_NODE_TIMEINTERVAL ) $timeInterval = PLIBRARY_NODE_TIMEINTERVAL;

			if (isset($timeInterval) &&  is_numeric($timeInterval)) $time = $timeInterval*1000;//convert to milliseconds
			else return;

			$controller = WGlobals::get('controller'); //controller
			$url = WPage::routeURL( 'controller='.$controller.'&task=saveauto' ); //url for the saving
//			$namekey = substr(md5($formname),0,-26).'_save';
			$namekey = 'HP_' . $formname . '_save';
//			$namekey = 'FG_' . $formname . '_save';

$script = '
(function($j){
$j(document).ready(function(){
window.WApps.helpers.autoSave("'.$formname.'" ,"'. $time.'" ,"'. $url.'" ,"'. $namekey.'" );
return false;
});
})(jQuery);';

		WPage::addJSLibrary( 'jquery' );
		WPage::addJSFile( 'main/js/jquery.autosave.js', 'inc' );
		WPage::addJSScript( $script );

	}//endfct

}//endclass
