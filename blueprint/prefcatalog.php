<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/

/**
 *
 * this class is the object to create a button with all the properties possible to use
 */
class WRender_Prefcatalog_classObject {

	/*
	 *
	 */
	public $type = 'buttonAddToCart';

	//a numeric value that we can increase to define the version
	//so we can do differnt implementation for diffrent verison
	//we can check the version avaiable
	public $version = 2;

}//endclass



class WRender_Prefcatalog_class extends Theme_Render_class {


/**
 *
 * This function is to render a button
 * @param object $data
 *
 * 			WPage::renderBluePrint( 'button', $objButtonO );
 *
 * Sample:
 		$objButtonO = WPage::newBluePrint( 'prefcatalog' );
		$objButtonO->type = 'buttonAddToCartInItemPage';
		$objButtonO->text = WText::translate( 'Make an offer' );
		$objButtonO->link = $link;
		$product->cart = WPage::renderBluePrint( 'prefcatalog', $objButtonO );
*
 *
 *
 */
  	public function render( $data ) {
//debug( 666611107, $data );
  		$html = '';
  		$objButtonO = WPage::newBluePrint( 'button' );
  		foreach( $data as $key => $value ) $objButtonO->$key = $value;

  		$type2Use = ( !empty($data->typeReplacement) ? $data->typeReplacement : $data->type );

  		switch( $type2Use ) {
                        case 'buttonViewCartInCatalogPage':
                                $objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.carticon' );	//'fa-plus';
				$objButtonO->size = $this->value( 'catalog.cartsize' );
				$objButtonO->color = $this->value( 'catalog.cartcolor' );	//'warning';
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;
                            
  			case 'buttonAddToCartInCatalogPage':
				$objButtonO->type = 'infoLink';
				$objButtonO->size = $this->value( 'catalog.cartsize' );
				$objButtonO->color = $this->value( 'catalog.cartcolor' );	//'warning';
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;
  			case 'buttonReviewInCatalogPage':
				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.reviewicon' );
				$objButtonO->size = $this->value( 'catalog.reviewsize' );
				$objButtonO->color = $this->value( 'catalog.reviewcolor' );
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;
 			case 'buttonQuestionInCatalogPage':
				$objButtonO->type = 'infoLink';
//				$objButtonO->icon = $this->value( 'catalog.reviewicon' );
				$objButtonO->size = $this->value( 'catalog.questionsize' );
				$objButtonO->color = $this->value( 'catalog.questioncolor' );
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;
  			case 'buttonAddToCartInItemPage':
				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.addcarticon' );
				$objButtonO->size = $this->value( 'catalog.addcartsize' );
				$objButtonO->color = $this->value( 'catalog.addcartcolor' );
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;
  			case 'buttonDetailsInCatalogPage':
				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.detailicon' );
				$objButtonO->size = $this->value( 'catalog.detailsize' );
				$objButtonO->color = $this->value( 'catalog.detailcolor' );
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;
  			case 'buttonViewAllInCatalogPage':
				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.viewallicon' );
				$objButtonO->size = $this->value( 'catalog.viewallsize' );
				$objButtonO->color = $this->value( 'catalog.viewallcolor' );
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;
  			case 'buttonBasketUpdateCart':
//				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.cartupdateicon' );	//'fa-plus';
				$objButtonO->size = $this->value( 'catalog.cartupdatesize' );
				$objButtonO->color = $this->value( 'catalog.cartupdatecolor' );	//'warning';
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;
  			case 'buttonBasketCheckoutPrevious':	// conitnue shopping
//				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.cartpreviousicon' );	//'fa-plus';
				$objButtonO->size = $this->value( 'catalog.cartprevioussize' );
				$objButtonO->color = $this->value( 'catalog.cartpreviouscolor' );	//'warning';
				$objButtonO->iconPosition = $this->value( 'catalog.cartpreviousiconposition' );	//'warning';
//debug( 666611107, $objButtonO );
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;
  			case 'buttonBasketCheckoutNext':	//cehckout button
//				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.cartnexticon' );	//'fa-plus';
				$objButtonO->size = $this->value( 'catalog.cartnextsize' );
				$objButtonO->color = $this->value( 'catalog.cartnextcolor' );	//'warning';
				$objButtonO->iconPosition = $this->value( 'catalog.cartnexticonposition' );	//'warning';

				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;

  			case 'buttonVendorsRegister':
				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.vendorsregistericon' );
				$objButtonO->size = $this->value( 'catalog.vendorsregistersize' );
				$objButtonO->color = $this->value( 'catalog.vendorsregistercolor' );
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;

  			case 'buttonViewMap':	//View Map
				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.viewmapicon' );	//'fa-plus';
				$objButtonO->size = $this->value( 'catalog.viewmapsize' );
				$objButtonO->color = $this->value( 'catalog.viewmapcolor' );	//'warning';
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;

  			case 'buttonEditAddress':	//Edit address
				$objButtonO->type = 'infoLink';
				$objButtonO->icon = $this->value( 'catalog.editaddressicon' );	//'fa-plus';
				$objButtonO->size = $this->value( 'catalog.editaddresssize' );
				$objButtonO->color = $this->value( 'catalog.editaddresscolor' );	//'warning';
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;

  			case 'itemAddPhoto':	//Add Photo into the Item Edit
				$objButtonO->type = 'button';
				if ( $this->value( 'toolbar.icon' ) ) $objButtonO->icon = 'fa-download';
				$objButtonO->size = $this->value( 'catalog.editaddresssize' );
				if ( $this->value( 'toolbar.color' ) ) {
					$objButtonO->color = 'info';
				} else {
					$objButtonO->color = 'default';
				}//endif
				$html = WPage::renderBluePrint( 'button', $objButtonO );
  				break;

  			case 'showAllLink':	//Add Photo into the Item Edit
  				$html = '<div class="showAll">' . $data->html . '</div>';
  				break;

  			case 'addJS4Stars':
debug( 67577112, 'THSI NEED TO BE REFACTOR BECAUSE WE DO NOT USE THE IAMGE START ANY MORE BUT WE SHOULD USE THE RESPONSIVE START HERE. AS WELL, AMOD JUST LET ME KNOW HOW ' );
/*
 *	this is an example
 * 		$myHtml .= '<a href="#" onClick="return changeStar('. $this->starRate .');">';
//		$myHtml .= '<img border=0 id="star_'.$this->starRate.'" src="'. JOOBI_URL_JOOBI_IMAGES . 'star/' .$colorStar. '">';
		$iconO = WPage::newBluePrint( 'icon' );
		$iconO->location = 'star/';
		$iconO->id = 'star_' . $this->starRate;
		$iconO->icon = $colorStar; 		// start  / star-blank
//		$iconO->size = 'large';
		$myHtml .= WPage::renderBluePrint( 'icon', $iconO );
 *
 */

//url = "' . JOOBI_URL_JOOBI_IMAGES . 'star/";
//   document.getElementById("star_"+x).src = url+name+".png";
                            //if (x <= val){name = "' . $data->color . '";}
                            //else {name = "white";}


				$changeStar = 'window.onload =function(){document.getElementById("jrate_rats").value = \'\';};';
				$changeStar .= 'function changeStar(val){
document.getElementById("jrate_rats").value = val;
for (var x = 1; x <= ' . $data->maxStar . '; x++)
{
    if (x <= val){name = "fa-star";}
    else {name = "fa-star-o";}
    	document.getElementById("star_"+x).className = "fa " + name + " text-warning fa-lg"
}
return false;
}';

				//WPage::addJSScript($changeStar,'default', false);
				WPage::addJS( $changeStar, 'text/javascript', true );
  				break;

  			default:
//debug( 666611107, 'default case' );
  				break;
  		}//endswitch

  		return $html;

  	}//endfct

}//endclass
