<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_View_class extends Theme_Render_class {

	private $_data = null;
	private static $_formIcon = null;

/**
 *
 * This function is to render a button
 * @param object $data
 *
 * 			WPage::renderBluePrint( 'view', $objButtonO );
 *
 * 				-> action: store / create
 */
  	public function render( $data ) {

  		$this->_data = $data;

  		if ( !isset(self::$_formIcon) ) self::$_formIcon = $this->value( 'view.icon' );

  		if ( $data->action == 'header' ) return  $this->_addHeader();

  	}//endfct


/**
	* <p>Function to put the title and all together in the toolbar box</p>
	* @param string $this->_data->headerMenus the menu to add to the header
	*/
	private function _addHeader() {

		$formId = isset($this->_data->view->formObj->name) ? $this->_data->view->formObj->name : $this->_data->view->firstFormName;

		$forceHeader = WGlobals::get( 'viewTitle', '', 'global' );
		if ( !empty($forceHeader) ) $this->_data->view->name = $forceHeader;

		$DIVheader='';
		if ( $this->_data->view->firstFormName == $formId ) {

			//check if we need to show the title of the header:
			$haveTitle = $this->_data->view->menu % 5;	// if type divide equally by 5 we dont show the title

			if ( $haveTitle ) {
				$text = '';
				if ( !empty($this->_data->view->name) && ( IS_ADMIN || PLIBRARY_NODE_PAGETITLE ) ) {

					$text = $this->_data->view->name;
					if ( !empty($this->_data->view->pageTitle) ) $text .= ' : ';
				}//endif

				if ( !empty($this->_data->view->pageTitle) ) {
					$text .= '<small><small>'.$this->_data->view->pageTitle.'</small></small>' ;
				}//endif
			}//endif


			//make the header
			if ( !empty($text) ) {

				$currentURL = WView::getURI();
				$text='<a title="Refresh" href="'.$currentURL.'">'.$text.'</a>';

				//create the header
				if ( self::$_formIcon && !empty($this->_data->view->faicon) ) {
					$span = '<i class="fa ' . $this->_data->view->faicon . ' text-primary"></i>';
				} else {
					$span = '';
				}//endfct

				$DIVheader = '<h1>' . $span ;
				$DIVheader .= $text .  '</h1>';

			}//endif

		}//endif


		$directEditIcon = WGlobals::get( 'directEditIcon', '', 'global' );

		$html = '<div class="clearfix">';

		$html .= '<div class="page-header pull-left">';
		$html .= $DIVheader . $directEditIcon;
		$html .= '</div>';

		if ( !empty( $this->_data->headerMenus ) ) {
			$html .= '<div class="pull-right">';
			$html .= $this->_data->headerMenus;
			$html .= '</div>';
		}//endif

		$html .= '</div>';


		return $html;


	}//endfct

}//endclass
