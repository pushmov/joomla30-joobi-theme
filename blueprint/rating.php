<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Rating_class extends Theme_Render_class {

/**
 *
 * This function is to render a button
 * @param object $data
 *
 * 			WPage::renderBluePrint( 'legend', $objButtonO );
 *
 * 				-> action: store / create
 */
  	public function render( $data ) {

  		foreach( $data as $p => $v ) $this->$p = $v;

  		return $this->_colourC();

  	}//endfct


/**
*this fct is the main fct for creating the star, it will arrange and put each star in the right place
*/
private function _colourC() {

	//here we are making the maxNbStar *10 for the use of loop
	$this->maxNbStar = $this->maxNbStar * 1;

	//this is for creating half star, if is true it will create full star
	$this->makeHalfStar = false;

	$myHtml = '';
	$this->starRate = 0;

	//this is the loop it will create star until the max star the programmer desire
	for( $i=1; $i <= $this->maxNbStar; $i++ ) {

		//this condition will stop the fct to create white star after the full and half star right away 1 meams create,0 means dont create
		$condition = 1;

		//this condition will create color star until the time the rating is greater that the loop,
		//after it will create white or half star depending on the condition below
		if ( $i < $this->rating ) {

			$this->starRate = $i;
			$myHtml .= $this->_createStar();

		} else {

			$iHalfStar	=	$i-1;
			$endFull	=	$i+0.25;
			$iStart		=	$iHalfStar+0.25;
			$iEnd		=	$iHalfStar+0.75;
			//to check if is possible to create half star or not
			if ($iStart<=$this->rating && $this->rating<=$iEnd){

				$this->makeHalfStar	= true;
//				$this->colorP = $this->_colorPreference();
				$this->starRate	= $i;

				$myHtml .= $this->_createStar();

				$this->makeHalfStar = false;
				$condition=0;

			}elseif ( $iEnd < $this->rating && $this->rating<$endFull ) {

				$this->starRate = $i;
				$myHtml .= $this->_createStar();
				$condition = 0;

			}//endif
		}//endif

		//here we will create white star
		if ($i>$this->rating && $condition==1){

			$this->starRate = $i;
			$myHtml .= $this->_createStar( 'white' );

		}//endif
	}//endfor

	return $myHtml;

 }//endfct


/**
 *
 * this fct will choice if the star are for rating system or just for display.
 * @param unknown_type $color
 */
private function _createStar( $color='' ) {

	$myHtml='';
	if ( empty($color) ) $color = $this->colorPref;

	if ( $this->makeHalfStar==true ) {
		$colorStar = 'star-half';
	} else {
		if ( $color == 'white' ) {
			$colorStar = 'star-blank';
		} else {
			$colorStar = 'star';
		}//endif
	}//endif

	if ( !defined('JOOBI_URL_THEME_JOOBI') ) WView::definePath();

	if ($this->type==1) {

		$myHtml .= '<a href="#" onClick="return changeStar('. $this->starRate .');">';
		$iconO = WPage::newBluePrint( 'icon' );
		$iconO->location = 'star/';
		$iconO->id = 'star_' . $this->starRate;
		$iconO->icon = $colorStar;
		$myHtml .= WPage::renderBluePrint( 'icon', $iconO );

		$myHtml .= '</a>';

	}elseif ( $this->type==2 ) {
		$link = WPage::routeURL( 'controller=' . $this->rateController.'&task=rate&starRate=' . $this->starRate . '&primaryId=' . $this->primaryId . '&restriction=' . $this->restriction );
		$myHtml .= '<a href="'. $link .'">';

		$iconO = WPage::newBluePrint( 'icon' );
		$iconO->location = 'star/';
		$iconO->icon = $colorStar;
//		$iconO->size = 'large';
		$myHtml .= WPage::renderBluePrint( 'icon', $iconO );

		$myHtml .= '</a>';
	} else {
		$iconO = WPage::newBluePrint( 'icon' );
		$iconO->location = 'star/';
		$iconO->icon = $colorStar;
//		$iconO->size = 'large';
		$myHtml .= WPage::renderBluePrint( 'icon', $iconO );

	}//endif

	return $myHtml;

}//endfct

}//endclass
