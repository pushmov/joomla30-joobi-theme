<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Breadcrumb_class extends Theme_Render_class {


/**
 *
 * This function is to display the path of the browsed category
 * @param array of object $branchesA
 */
  	public function render( $data ) {

  		$listA = $data->listA;
  		if ( empty($listA) ) return '';

  		if ( 'cart' == $data->type ) {
  			$navigationUsecmsBreadcrumb = $this->value( 'navigationbrdcrumbcartusecms' );
  		} else {
  			$navigationUsecmsBreadcrumb = $this->value( 'navigationbrdcrumbcatalogusecms' );
  		}//endif


  		if ( $navigationUsecmsBreadcrumb ) {
			$app = JFactory::getApplication();
			$pathway = $app->getPathWay();

			foreach( $listA as $oneItem ) {

				if ( !empty($oneItem->link) ) {
					$oneItem->link = '#';
				}//endif

				//for the basket we strech a bit the name so it is more obivous
				if ( !empty($oneItem->stretch) ) {
					$oneItem->name = '&nbsp;&nbsp;&nbsp;&nbsp;' . $oneItem->name . '&nbsp;&nbsp;&nbsp;&nbsp;';
				}//endif
				$pathway->addItem( $oneItem->name, $oneItem->link );
			}//endforeach

			return '';

  		} else {

  			if ( 'cart' == $data->type ) {

				$html = '<div class="wizard basketTrail">';
	  			//we make our own breadcrumb
	  			$count = count($listA);
	  			$width = round( 95 / $count );
				foreach( $listA as $key => $oneE ) {
	//				$html .= '<div class="step">';
					$html .= '<a';
					if ( !empty( $oneE->current ) ) $html .= ' class="current"';
					if ( !empty( $oneE->link ) ) $html .= ' href="' . $oneE->link . '"';
					$html .= ' style="width:' . $width. '%;"';
					$html .= '>';
					if ( !empty( $oneE->showNumber ) ) '<span class="badge">' . $key . '</span>';
					$html .= '<span class="trail">' . $oneE->name . '</span></a>';
	//				$html .= '</div>';
				}//endforeach
				$html .= '</div>';

  			} else {

  				$html = '<div class="breadcrumb clearfix"><ol class="breadcrumb">';
	  			//we make our own breadcrumb
	  			$count = count($listA);
	  			$width = round( 95 / $count );
				foreach( $listA as $key => $oneE ) {
					$html .= '<li';
					if ( !empty( $oneE->current ) ) $html .= ' class="active"';
					$html .= '>';
					$html .= '<a';
					if ( !empty( $oneE->link ) ) $html .= ' href="' . $oneE->link . '"';
					$html .= '>';
					if ( !empty( $oneE->showNumber ) ) '<span class="badge">' . $key . '</span>';
					$html .= '<span class="trail">' . $oneE->name . '</span></a>';
					$html .= '</li>';
				}//endforeach
				$html .= '</ol></div>';

  			}//endif

			return $html;
  		}//endif

  	}//endfct

}//endclass
