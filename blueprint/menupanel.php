<?php
defined('JOOBI_SECURE') or die('J....');

/**
* <p>Library</p>
* @link joobi.co
* @copyright Copyright (c) 2007-2015 Joobi Limited All rights reserved.
* @link joobi.co/r.php\?l=license
* @author Joobi Team
*/
class WRender_Menupanel_class extends Theme_Render_class {

/**
 *
 * This function is to render a menu type of cpanel
 * @param object $data
 */
  	public function render( $data ) {

  		//we need to prepare all the variables
		WPage::addCSSFile( 'css/menudashboard.css' );
		$commonCSS = array();

		$html = '<div class="clearfix">';
		foreach( $data->elements as $oneIcon ) {
			if ( !empty($oneIcon->parent) ) continue;


		if ( 102 == $oneIcon->type ) {
			//processing of the custom buttons
			$buttonObject = Output_Mlinks_class::loadButtonFile( $oneIcon );
			if ( null === $buttonObject ) {
				continue;
//				$obj = null;
//				return $obj;
			}//endif

			$buttonObject->initialiseMenu( $data );
			$status = $buttonObject->make();
			if ( false === $status ) continue;

		}//endif

			WTools::getParams( $oneIcon );
			if ( !empty($oneIcon->requirednode) ) {
				$nodeExist = WExtension::exist( $oneIcon->requirednode );
				if ( empty($nodeExist) ) continue;
			}//endif

			if ( empty($oneIcon->faicon) ) $oneIcon->faicon = 'fa-question';


			if ( !empty($data->nestedView) ) {

				//check what type of link we have
				if ( substr( $oneIcon->action, 0, 1 ) == '/'
				|| substr( $oneIcon->action, 0, 4 ) == 'http'
				|| ( JOOBI_INDEX != '' && substr( $oneIcon->action, 0, strlen( JOOBI_INDEX ) ) ==  JOOBI_INDEX )
				) {
					$link = WPage::link( $oneIcon->action );
				} else {
					$link = WPage::link( 'controller=' . $oneIcon->action );
				}//endif

			} else {
				$link = WPage::routeURL( 'controller=' . $oneIcon->icon, '', false, false, false, $oneIcon->icon );
			}//endif

			$html .= '<div style="float:left;">
<div class="icon">
<a href="' . $link . '">
<i class="fa ' . $oneIcon->faicon . ' fa-4x"></i>
<span>' . $oneIcon->name . '</span>
</a>
</div>
</div>';

		}//endforeach

		$html .= '</div>';

		return $html;

  	}//endfct

}//endclass
